;;; -*- lexical-binding: t; -*-
(custom-set-variables
 '(safe-local-variable-values
   '((eval conda-env-activate "py39")
     (magit-todos-exclude-globs ".android/*" ".authinfo/*" ".cache/*" ".conda/*" ".config/*" ".dir-locals.el/*" ".gitmodules/*" ".gnupg/*" ".local/*" ".mbsyncrc/*" ".mozilla/*" ".pki/*" ".profile/*" ".R/*" ".slime/*" ".ssh/*" ".ssr/*" ".stremio-server/*" ".TinyTeX/*" ".tor-browser/*" ".visidata/*" ".visidatarc/*" ".Xauthority/*" ".xinitrc/*" ".xprofile/*" ".zotero/*" ".zprofile/*" "bin/*" "dotfiles-private/*" "dotfiles-public/*" "downloads/*" "google-drive/*" "LICENSE/*" "README/*" "todo.txt/*" "Zotero/*" ".emacs.d/*" ".emacs.dx/*" ".emacs.d.centaur/*" ".m2/*")
     (magit-todos-exclude-globs ".config/*" ".local/*" ".TinyTeX/*" ".cache/*" "Zotero/*")
     (magit-todos-exclude-globs ".config/*" ".local/*" ".TinyTeX/*" ".cache/*")
     (magit-todos-exclude-globs "elpa/*" "lisp/github/*" "var/*" "elpy/*" "quelpa/*")
     (magit-todos-exclude-globs "elpa/*" "lisp/github/*" "quelpa/*")
     (magit-todos-exclude-globs "*.csv")
     (magit-todos-exclude-globs "*")
     (magit-todos-exclude-globs "elpa/*")
     (magit-todos-exclude-globs "/data/raw/*" "/data/temp/*")
     (magit-todos-exclude-globs "./elpa/*")
     (magit-todos-exclude-globs "./.config/*" "./.local/*" "./.TinyTeX/*" "./.cache/*")
     (magit-todos-exclude-globs "./.config/*" "./.local/*" "./.TinyTeX/*")
     (projectile-project-compilation-cmd . "make all")
     (dired-omit-mode . 1)
     (dired-omit-files . "^\\~$*")
     (projectile-project-compilation-cmd . "make all")
     (projectile-project-compilation-cmd . "make")))
 '(warning-suppress-log-types
   '(((yasnippet backquote-change))
     ((yasnippet backquote-change))))
 '(warning-suppress-types '(((yasnippet backquote-change))))
 '(warning-suppress-log-types
   '((use-package)
     (use-package)
     ((yasnippet backquote-change))))
 '(warning-suppress-types '((use-package) ((yasnippet backquote-change))))
 )

(provide 'init-custom-variables)
