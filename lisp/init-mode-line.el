;;; init-mode-line.el --- mode-line configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; doom-modeline
;; (use-package doom-modeline
;;   :init (doom-modeline-mode -1)
;;   :custom ((doom-modeline-height 1))
;;   :config
;;   (set-face-attribute 'mode-line nil :family "Fira Code Nerd Font" :height 100)
;;   (set-face-attribute 'mode-line-inactive nil :family "Fira Code Nerd Font" :height 100)
;;   (setq doom-modeline-buffer-encoding t
;;         doom-modeline-buffer-file-name-style 'truncate-upto-project
;;         doom-modeline-buffer-modification-icon nil
;;         doom-modeline-buffer-state-icon nil
;;         doom-modeline-continuous-word-count-modes '(markdown-mode gfm-mode org-mode)
;;         doom-modeline-display-default-persp-name t
;;         doom-modeline-icon nil
;;         doom-modeline-indent-info t
;;         doom-modeline-lsp t
;;         doom-modeline-major-mode-color-icon nil
;;         doom-modeline-major-mode-icon nil
;;         doom-modeline-minor-modes t
;;         doom-modeline-modal-icon nil
;;         doom-modeline-persp-icon t
;;         doom-modeline-persp-name t
;;         doom-modeline-project-detection 'projectile
;;         doom-modeline-vcs-max-length 30
;;         doom-modeline-workspace-name t)
;;   (doom-modeline-def-modeline 'funk-simple-line
;;     '(workspace-name modals matches buffer-info remote-host parrot selection-info misc-info vcs)
;;     '(input-method buffer-encoding buffer-position minor-modes major-mode process checker))
;;   (doom-modeline-set-modeline 'funk-simple-line t)
;;   )

;;; keycast
;; (use-package keycast)

;;; hide-mode-line
(use-package! hide-mode-line
  ;; :defer 5
  )

;;; diminish
(use-package! diminish
  ;; :defer 5
  )

;;; minions
(use-package! minions
  :config
  (minions-mode 1)
  (setq minions-mode-line-lighter "+")
  (setq minions-direct '(perspective-mode eglot  flycheck-mode flymake-mode lsp-mode flyspell-mode))
  )

;;; nyan
;; (use-package nyan-mode
;;   :config
;;   (nyan-mode +1))

;;; set a custom buffer name in modeline

;;;; previous approach to change buffer name in the mode-line
;; (setq display-time-format " %a %e %b, %H:%M")
;; (setq display-time-interval 60)
;; (setq display-time-default-load-average nil)
;; (add-hook 'after-init-hook #'display-time-mode)

(load-file "~/.doom.d/lisp/lisp-local/funk-modeline.el")
;; testing out
;; (funk/modeline--buffer-file-name buffer-file-name buffer-file-truename 'shrink)
;; (funk/modeline--buffer-file-name buffer-file-name buffer-file-truename 'shrink 'shrink)

(setq-default  mode-line-buffer-identification '((:eval (funk/set-funk-modeline))))

;;; modify vc-git indicator on mode-line
(advice-add #'vc-git-mode-line-string :filter-return #'funk/replace-git-status)
(defun funk/replace-git-status (tstr)
  (if (not (in-slow-ssh))
      (progn
        (let* ((tstr (replace-regexp-in-string "Git" "" tstr))
               (first-char (substring tstr 0 1))
               (rest-chars (substring tstr 1)))
          (cond
           ((string= ":" first-char) ;;; Modified
            (setq vc-state (replace-regexp-in-string "^:" "\uF126 " tstr))
            (propertize vc-state 'face '((:inherit vc-edited-state))))
           ((string= "-" first-char) ;; No change
            (setq vc-state (replace-regexp-in-string "^-" "\uF126 " tstr))
            (propertize vc-state 'face '((:inherit vc-up-to-date-state))))
           (t tstr))))))

;;; show file size in modeline
;; (size-indication-mode -1)


;;; change modeline color based on buffer status
;; ;; FROM: https://stackoverflow.com/a/17009340/5974372
;; (require 'cl)
;; (lexical-let ((default-color (cons (face-background 'mode-line)
;;                                    (face-foreground 'mode-line))))
;;   (add-hook 'post-command-hook
;;             (lambda ()
;;               (let ((color (cond ((minibufferp) default-color)
;;                                  ;; ((evil-insert-state-p) '("#edc809" . "#ffffff"))
;;                                  ((evil-emacs-state-p)  '("#000000" . "#ffffff"))
;;                                  ((buffer-modified-p)   '("#FF6E67" . "#ffffff"))
;;                                  (t default-color))))
;;                 (set-face-background 'mode-line (car color))
;;                 (set-face-foreground 'mode-line (cdr color))))))

;;; integration with eyebrowse

(defun funk/eyebrowse-current-slot-mode-line-indicator ()
  (let* ((number-slot
          (number-to-string (car (assoc (eyebrowse--get 'current-slot) (eyebrowse--get 'window-configs)))))
         (name-slot
          (nth 2 (assoc (eyebrowse--get 'current-slot) (eyebrowse--get 'window-configs)))))
    (concat
     "["
     (propertize (concat number-slot (if (equal name-slot "" ) "" ":") name-slot) 'face 'eyebrowse-mode-line-active)
     "]"
     )
    ))

;;; modify the line counter
;; Based on: https://stackoverflow.com/a/8191130/5974372
(defvar funk/mode-line-buffer-line-count nil)
(make-variable-buffer-local 'funk/mode-line-buffer-line-count)

;; (defvar line-mess nil)
;; (make-variable-buffer-local 'line-mess)

;; (defmacro with-suppressed-message (&rest body)
;;   "Suppress new messages temporarily in the echo area and the `*Messages*' buffer while BODY is evaluated."
;;   (declare (indent 0))
;;   (let ((message-log-max nil))
;;     `(with-temp-message (or (current-message) "") ,@body)))

;; (defun funk/mode-line-show-count-in-region ()
;;   "WIP."
;;   (interactive)
;;   (if (region-active-p)
;;       (progn
;;         (let* ((beg (region-beginning))
;;                (end (region-end))
;;                (count (count-lines beg end t)))
;;           (setq line-mess (with-suppressed-message (count-words--message " vai" beg end)))))
;;     (setq line-mess nil))
;;   line-mess)


(setq-default mode-line-format
              '("%e" mode-line-front-space
                (:eval (eyebrowse-mode-line-indicator))
                ;; (:eval (funk/eyebrowse-current-slot-mode-line-indicator))
                " "
                (:propertize
                 ("" mode-line-mule-info mode-line-client mode-line-modified mode-line-remote)
                 display
                 (min-width
                  (1.0)))
                ;; mode-line-remote
                mode-line-frame-identification
                mode-line-buffer-identification
                " "
                (vc-mode vc-mode)
                "  "
                minions-mode-line-modes
                mode-line-misc-info
                ;; '(:eval (list (nyan-create)))
                ;; mode-line-position
                (list 'line-number-mode " [")
                (:eval (when line-number-mode
                         (let ((str "%l"))
                           (when (and (not (buffer-modified-p)) funk/mode-line-buffer-line-count)
                             (setq str (concat str "/" funk/mode-line-buffer-line-count)))
                           str)))
                (list 'column-number-mode ",%c]")
                ;; (:eval (funk/mode-line-show-count-in-region))
                mode-line-end-spaces))


(defun funk/mode-line-count-lines ()
  (setq funk/mode-line-buffer-line-count (int-to-string (count-lines (point-min) (point-max)))))

(add-hook 'find-file-hook 'funk/mode-line-count-lines)
(add-hook 'after-save-hook 'funk/mode-line-count-lines)
(add-hook 'after-revert-hook 'funk/mode-line-count-lines)
(add-hook 'dired-after-readin-hook 'funk/mode-line-count-lines)

;; NOTE: use this if you want to remove eyebrowse slot
;; list from the right side of the mode-line
(setq-default mode-line-misc-info
              '((which-function-mode
                 (which-func-mode
                  ("" which-func-format " ")))
                (eglot--managed-mode
                 (" [" eglot--mode-line-format "] "))
                (global-mode-string
                 ("" global-mode-string))))

(provide 'init-mode-line)
;;; init-mode-line.el ends here
