;;; init-git.el --- Magit configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; magit
(use-package! magit
  :after projectile
  :init
  (setq magit-define-global-key-bindings nil)
  (setq magit-repolist-columns
        '(("Name"        30 magit-repolist-column-ident ())
          ("Status"      10 magit-repolist-column-flag)
          ("Version"     25 magit-repolist-column-version ())
          ("⇣"            3 magit-repolist-column-unpulled-from-upstream
           ((:right-align t)
            (:help-echo "Upstream changes not in branch")))
          ("⇡"            3 magit-repolist-column-unpushed-to-upstream
           ((:right-align t)
            (:help-echo "Local changes not in upstream")))
          ("Path"         99 magit-repolist-column-path ())))

  (setq magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)

  :bind
  ("C-x g g" . magit-status)
  ("C-x g d" . magit-dispatch)
  ("C-x g f" . magit-file-dispatch)
  ("C-x g l" . magit-list-repositories)

  :config
  ;; manage dotfiles ($HOME) in bare repos
  ;;;###autoload
  (defun ~/magit-process-environment (env)
    "Add GIT_DIR and GIT_WORK_TREE to ENV when in a special directory.
https://github.com/magit/magit/issues/460 (@cpitclaudel)."
    (let ((default (file-name-as-directory (expand-file-name default-directory)))
          (home (expand-file-name "~/")))
      (when (string= default home)
        (let ((gitdir (expand-file-name "~/dotfiles-public")))
          (push (format "GIT_WORK_TREE=%s" home) env)
          (push (format "GIT_DIR=%s" gitdir) env))))
    env)

  (progn
    (let ((project-dirs (bound-and-true-p projectile-known-projects)))
      (setq magit-repository-directories
            (mapcar (lambda (path) `(,path . 1)) project-dirs))))

  ;;;###autoload
  (defun funk/magit-repolist-sort-columns ()
    "Sort 'Status' column in `magit'-tabulated-list."
    (setq-local tabulated-list-sort-key (cons "Status" t)))

  (general-define-key
   :keymaps 'transient-base-map
   "<escape>" 'transient-quit-one)

  (setq magit-ediff-dwim-show-on-hunks t)

  (setq ediff-split-window-function 'split-window-horizontally)

  (setq magit-after-save-refresh-buffers t)

  (advice-add 'magit-process-environment :filter-return #'~/magit-process-environment)

  (add-hook 'projectile-after-switch-project-hook #'(lambda () (progn
                                                                 (let ((project-dirs (bound-and-true-p projectile-known-projects)))
                                                                   (setq magit-repository-directories
                                                                         (mapcar (lambda (path) `(,path . 1)) project-dirs))))))

  (add-hook 'magit-repolist-mode-hook 'funk/magit-repolist-sort-columns)

  ;; (defun funk/refresh-gitgutter+-mode ()
  ;;   (interactive)
  ;;   (progn
  ;;     (global-git-gutter+-mode -1)
  ;;     (global-git-gutter+-mode +1)))

  ;; (add-hook 'magit-post-stage-hook 'funk/refresh-gitgutter+-mode)
  ;; (add-hook 'magit-post-unstage-hook 'funk/refresh-gitgutter+-mode)
  ;; (add-hook 'magit-post-refresh-hook 'funk/refresh-gitgutter+-mode)
  )

;;; forge
(use-package! forge
  ;; :defer 5
  ;; NOTE: Make sure to configure a GitHub token before using this package!
  ;; - https://magit.vc/manual/forge/Token-Creation.html#Token-Creation
  ;; - https://magit.vc/manual/ghub/Getting-Started.html#Getting-Started
  :after magit)

(use-package! git-gutter
  :config
  (global-git-gutter-mode)
  (setq git-gutter:update-interval 0.02))

(use-package! git-gutter-fringe
  ;; based on:
  ;; https://www.reddit.com/r/emacs/comments/suxc9b/modern_gitgutter_in_emacs/
  :config
  ;; (setq-default left-fringe-width  20)
  ;; (setq-default right-fringe-width 20)
  (define-fringe-bitmap 'git-gutter-fr:added
    [#b11000000] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:modified
    [#b11000000] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:deleted
    [
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     #b11000000
     #b11000000
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     ]))

;;; magit-todos
(use-package! magit-todos
  ;; :defer 10
  :config
  (setq magit-todos-keyword-suffix "\\(?:([^)]+)\\)?:?") ; make colon optional
  (setq magit-todos-exclude-globs '("*.map" "*.html" "var/*" "elpa/*" "elpy" "dump.Rmd" "manuscript/" "*TinyTeX*" "*.csv"))
  ;; (setq magit-todos-group-by '(magit-todos-item-filename magit-todos-item-keyword magit-todos-item-first-path-component))
  (setq magit-todos-group-by '(magit-todos-item-keyword))
  (magit-todos-mode +1))

;;; git-commit
(use-package! git-commit
  :config
  ;;;###autoload
  (defun funk/check-git-spelling (force)
    "Check spelling of commit message.
When FORCE is truthy, continue commit unconditionally."
    (let ((tick (buffer-chars-modified-tick))
          (result
           (let ((ispell-skip-region-alist ; Dynamic variable
                  (cons (list (rx line-start "#") #'forward-line) ; Comment
                        ispell-skip-region-alist)))
             (ispell-buffer))))
      (cond
       (force
        t)
       ;; When spell check was completed, result is truthy
       (result
        ;; When nothing was corrected, character tick counter is
        ;; unchanged
        (or (= (buffer-chars-modified-tick) tick)
            (y-or-n-p "Spelling checked.  Commit? "))))))
  (global-git-commit-mode)
  (add-hook 'git-commit-finish-query-functions #'funk/check-git-spelling))

;;; browse-at-remote
(use-package! browse-at-remote
  ;; :defer 20
  :bind
  ("C-x g b" . browse-at-remote))

;;; vc-msg
(use-package! vc-msg
  ;; :defer 20
  :bind
  ("C-x g o" . vc-msg-show))

;;; git-modes

(use-package! git-modes
  ;; :defer 10
  )

;;; blamer

(use-package! blamer
  ;; :defer 10
  :ensure nil
  :quelpa (blamer :fetcher github :repo "artawower/blamer.el")
  (blamer-idle-time 0.7)
  (blamer-min-offset 80)
  :config
  ;; (global-blamer-mode 1)
  (setq blamer-author-formatter " ✎ %s ")
  (setq blamer-datetime-formatter "[%s]")
  (setq blamer-commit-formatter " ● %s")
  (setq blamer-prettify-time-p t)
  (setq blamer-uncommitted-changes-message "NO COMMITTED"))

;;; git-timemachine

(use-package! git-timemachine
  ;; NOTE: bindigins are defined by evil-collection
  ;; see: https://github.com/emacs-evil/evil-collection/blob/master/modes/git-timemachine/evil-collection-git-timemachine.el
  :defer 15
  )

;;; diff-hl
(use-package! diff-hl
  ;; :defer 15
  :init
  :defines diff-hl-margin-symbols-alist
  :config
  (custom-set-faces
   '(diff-hl-change          ((t (:background unspecified))))
   '(diff-hl-delete          ((t (:background unspecified))))
   '(diff-hl-margin-change   ((t (:background unspecified))))
   '(diff-hl-margin-delete   ((t (:background unspecified))))
   '(diff-hl-margin-ignored  ((t (:background unspecified))))
   '(diff-hl-margin-insert   ((t (:background unspecified))))
   '(diff-hl-margin-unknown  ((t (:background unspecified))))
   '(diff-hl-margin-change   ((t (:background unspecified))))
   '(diff-hl-margin-delete   ((t (:background unspecified))))
   '(diff-hl-margin-ignored  ((t (:background unspecified))))
   '(diff-hl-margin-insert   ((t (:background unspecified))))
   '(diff-hl-margin-unknown  ((t (:background unspecified))))
   )
  (setq diff-hl-margin-symbols-alist
        '((insert . "+")
          (delete . "-")
          (change . "~")
          (unknown . "*")
          (ignored . "i")))
  (setq diff-hl-dired-extra-indicators nil)
  (diff-hl-margin-mode)
  (add-hook 'dired-mode-hook 'diff-hl-dired-mode)
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh))

;;; code-review

;; REVIEW 2021-12-04 19:14:17 -0300: maybe in the future
(use-package code-review :disabled)

;;; custom functions
(defun funk/git-clone-clipboard-url ()
  "Clone git URL in clipboard asynchronously and open in dired when finished."
  (interactive)
  (cl-assert (string-match-p (regexp-opt-group '("git@" "https")) (current-kill 0)) nil "No URL in clipboard")
  (let* ((url (current-kill 0))
         (download-dir (expand-file-name "~/downloads/quick-clone-repos"))
         (project-dir (concat (file-name-as-directory download-dir)
                              (file-name-base url)))
         (default-directory download-dir)
         (command (format "git clone %s" url))
         (buffer (generate-new-buffer (format "*%s*" command)))
         (proc))
    (make-directory download-dir :parents)
    (when (file-exists-p project-dir)
      (if (y-or-n-p (format "%s exists. delete?" (file-name-base url)))
          (delete-directory project-dir t)
        (user-error "Bailed")))
    (switch-to-buffer buffer)
    (setq proc (start-process-shell-command (nth 0 (split-string command)) buffer command))
    (with-current-buffer buffer
      (setq default-directory download-dir)
      (shell-command-save-pos-or-erase)
      (require 'shell)
      (shell-mode)
      (view-mode +1))
    (set-process-sentinel proc (lambda (process state)
                                 (let ((output (with-current-buffer (process-buffer process)
                                                 (buffer-string))))
                                   (kill-buffer (process-buffer process))
                                   (if (= (process-exit-status process) 0)
                                       (progn
                                         (message "finished: %s" command)
                                         (dired project-dir))
                                     (user-error (format "%s\n%s" command output))))))
    (set-process-filter proc #'comint-output-filter)))

(provide 'init-git)
;;; init-git.el ends here
