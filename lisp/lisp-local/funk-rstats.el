;;; funk-rstats.el --- Miscellaneous functions for ESS mode -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; Launch new R instances
(defun funk/ess-start-R ()
  (interactive)
  (if (not (member "*R*" (mapcar (function buffer-name) (buffer-list))))
      (progn
        (delete-other-windows)
        (setq w1 (selected-window))
        (setq w1name (buffer-name))
        (setq w2 (split-window-left w1 nil t))
        (R)
        (set-window-buffer w2 "*R*")
        (set-window-buffer w1 w1name))))

(defun funk/ess-eval ()
  (interactive)
  (funk/ess-start-R)
  (if (and transient-mark-mode mark-active)
      (call-interactively 'ess-eval-region)
    (call-interactively 'ess-eval-region-or-function-or-paragraph)))

;; cause "Shift+Enter" to send the current line to *R*
(defun funk/ess-eval ()
  (interactive)
  (if (and transient-mark-mode mark-active)
      (call-interactively 'ess-eval-region)
    (call-interactively 'ess-eval-region-or-function-or-paragraph)))

;; default ESS to use R
(defun ess-set-language ()
  (setq-default ess-language "R")
  (setq ess-language "R"))

;;; Explicitly show busy processes on the mode-line of the inferior-ess-mode.
(defvar ess--busy-custom '("        " " *BUSY* " "        "))
(setq ess-busy-strings ess--busy-custom)

;;; Format the mode-line of the inferior-ess-mode
(defun funk/ess-display-host-name ()
  "Get the hostname to display on the mode-line of the inferior-ess-mode."
  (interactive)
  (let ((host (if (file-remote-p default-directory)
                  (tramp-file-name-host (tramp-dissect-file-name default-directory))
                (system-name))))
    (concat " @" host)))

(defun funk/ess-inferior-mode-line ()
  "Format the mode-line of the inferior-ess-mode."
  (setq-local mode-line-format '("%e"
                                 " "
                                 (:eval (eyebrowse-mode-line-indicator))
                                 " "
                                 mode-line-buffer-identification
                                 minions-mode-line-modes
                                 mode-line-misc-info
                                 (:eval (funk/ess-display-host-name))
                                 mode-line-end-spaces))
  (setq-local mode-line-process ""))
(add-hook 'inferior-ess-mode-hook 'funk/ess-inferior-mode-line)

;;; Custom actions during/after the code evaluation
(defun funk/ess-eval-and-recenter ()
  (interactive)
  (progn
    (ess-eval-region-or-function-or-paragraph-and-step)
    (recenter)))

(advice-add 'ess-eval-region-or-function-or-paragraph-and-step :before #'funk/try-expand-outline)

;;; Quick launch R for test
(defun funk/quick-r ()
  "Launch a new R instance with few opinated sample code."
  (interactive)
  (eyebrowse-create-window-config)
  (eyebrowse-rename-window-config (eyebrowse--get 'current-slot) "quick-r")
  (projectile-switch-project-by-name "~/google-drive/kguidonimartins/git-repos/quick-r")
  (progn
    (find-file (shell-command-to-string "echo -n quick-r_$(date '+%Y%m%d-%H%M%S').R"))
    (insert "if (!require(\"tidyverse\")) install.packages(\"tidyverse\")\nif (!require(\"here\")) install.packages(\"here\")\nif (!require(\"fs\")) install.packages(\"fs\")"))
  (evil-insert 0)
  (newline)
  (newline)
  (ess-eval-buffer))

;;; Smart code evaluation through a pipe chain
(defun funk/ess-eval-pipe-through-line (vis &optional assign)
  "Like `ess-eval-paragraph' but only evaluates up to the pipe on this line.

If no pipe, evaluate paragraph through the end of current line.
Prefix arg VIS toggles visibility of ess-code as for `ess-eval-region'.
The optional ASSIGN argument is useful in some cases. At the code examples
below, the | is the current cursor position and * is the position
up to which the code is evaluated.

    If ASSIGN is nil, the code is evaluated is this way:

        carb2 <-
        * mtcars %>%
        | select(carb, mpg) %>%
          filter(carb == 2)

    Which is the same as:

        mtcars %>%
          select(carb, mpg)

    Another example:

        carb2 <-
        * mtcars %>%
          select(carb, mpg) %>%
        | filter(carb == 2)

    Which is the same as:

        mtcars %>%
          select(carb, mpg) %>%
          filter(carb == 2)

    If ASSIGN is non-nil, the code is evaluated is this way:

      * carb2 <-
          mtcars %>%
        | select(carb, mpg) %>%
          filter(carb == 2)

    Which is the same as:

        carb2 <-
          mtcars %>%
          select(carb, mpg)

You can define smart keys for these commands, for example:
C-\\ for `funk/ess-eval-pipe-through-line' and
C-S-\\ or C-| for `funk/ess-eval-pipe-through-line-and-assign'.

Refer to `funk/ess-eval-pipe-through-line-and-assign'."
  (interactive "P")
  (save-excursion
    (let ((end (progn
                 (funk/ess-beginning-of-pipe-or-end-of-line)
                 (point)))
          (beg (progn (backward-paragraph)
                      (ess-skip-blanks-forward 'multiline)
                      (if (null assign)
                          (funk/ess-skip-assign-arrow))
                      (point))))
      (ess-eval-region beg end vis))))

(defun funk/add-pipe ()
  "Add a pipe operator %>% at the end of the current line.
Don't add one if the end of line already has one.  Ensure one
space to the left and start a newline with indentation."
  (interactive)
  (end-of-line)
  (unless (looking-back "%>%" nil)
    (just-one-space 1)
    (insert "%>%"))
  (newline-and-indent))

(defun funk/ess-eval-pipe-through-line-and-assign ()
  "When the ASSIGN argument of `funk/ess-eval-pipe-through-line' is non-nil."
  (interactive)
  (funk/ess-eval-pipe-through-line t t))

(defun funk/ess-skip-assign-arrow ()
  "Find the `<-' character and forward one character."
  (interactive)
  (progn
    (if (search-forward "<-" nil t)
        (forward-char 1))))

(defun funk/ess-beginning-of-pipe-or-end-of-line ()
  "Find point position of end of line or beginning of pipe %>%."
  (if (search-forward "%>%" (line-end-position) t)
      (goto-char (match-beginning 0))
    (end-of-line)))

(defun funk/add-pipe-inline ()
  "Add a pipe operator %>% at the end of the current line.
Don't add one if the end of line already has one."
  (interactive)
  (end-of-line)
  (unless (looking-back "%>%" nil)
    (just-one-space 1)
    (insert "%>%")))

(defun funk/add-pipe-here ()
  "Add a pipe operator %>% at the current position."
  (interactive)
  (just-one-space 1)
  (insert "%>%"))

(defun funk/add-in-here ()
  "Add a pipe operator %in% at the current position."
  (interactive)
  (just-one-space 1)
  (insert "%in%"))

;;; Custom code wrapper
(defun funk/ess-baseplot-by-wrap-object ()
  (interactive)
  (beginning-of-line)
  (insert "plot(")
  (end-of-line)
  (insert ", colNA = \"blue\")"))

(defun funk/ess-select-bracket-block ()
  (interactive)
  (progn
    (beginning-of-line)
    (evil-visual-char)
    (evil-find-char 1 ?\{)
    (evilmi-jump-items)))

(defun funk/insert-fig-ref ()
  (interactive)
  ;; (just-one-space 1)
  (insert "`r figure_ref(FIG_)`")
  (backward-char 2))

(defun funk/insert-tab-ref ()
  (interactive)
  ;; (just-one-space 1)
  (insert "`r table_ref(TAB_)`")
  (backward-char 2))

;; compile rmarkdown to HTML or PDF with M-n s
;; use YAML in Rmd doc to specify the usual options
;; which can be seen at http://rmarkdown.rstudio.com/
;; thanks http://roughtheory.com/posts/ess-rmarkdown.html
(defun funk/render-rmarkdown ()
  "Compile R markdown (.Rmd). Should work for any output type."
  (interactive)
  ;; Check if attached R-session
  (condition-case nil
      (ess-get-process)
    (error
     (ess-switch-process)))
  (let* ((rmd-buf (current-buffer)))
    (save-excursion
      (let* ((sprocess (ess-get-process ess-current-process-name))
             (sbuffer (process-buffer sprocess))
             (buf-coding (symbol-name buffer-file-coding-system))
             (R-cmd
              (format "library(rmarkdown); rmarkdown::render(\"%s\")"
                      buffer-file-name)))
        (message "Running rmarkdown on %s" buffer-file-name)
        (ess-execute R-cmd 'buffer nil nil)
        (switch-to-buffer rmd-buf)
        (ess-show-buffer (buffer-name sbuffer) nil)))))

;;; get the magrittr pipe with CTRL-shift-m
(defun funk/then_R_operator ()
  "R - %>% operator or 'then' pipe operator"
  (interactive)
  (just-one-space 1)
  (insert "%>%")
  (reindent-then-newline-and-indent))

;; ;; FROM: https://github.com/walmes/emacs/blob/master/funcs.el#L381
;; ;;----------------------------------------------------------------------
;; ;; Functions related do Rmd files.

;; Goes to next chunk.
(defun funk/polymode-next-chunk ()
  "Go to next chunk. This function is not general because is
   assumes all chunks are of R language."
  (interactive)
  (search-forward-regexp "^```{.*}$" nil t)
  (forward-line 1))

;; Goes to previous chunk.
(defun funk/polymode-previous-chunk ()
  "Go to previous chunk. This function is not general because is
   assumes all chunks are of R language."
  (interactive)
  (search-backward-regexp "^```$" nil t)
  (search-backward-regexp "^```{.*}$" nil t)
  (forward-line 1))

;; Evals current R chunk.
(defun funk/polymode-eval-R-chunk ()
  "Evals all code in R chunks in a polymode document (Rmd files)."
  (interactive)
  (if (derived-mode-p 'ess-mode)
      (let ((ptn (point))
            (beg (progn
                   (search-backward-regexp "^```{r.*}$" nil t)
                   (forward-line 1)
                   (line-beginning-position)))
            (end (progn
                   (search-forward-regexp "^```$" nil t)
                   (forward-line -1)
                   (line-end-position))))
        (ess-eval-region beg end nil)
        (goto-char ptn))
    (message "ess-mode weren't detected.")))

;; Evals R chunk and goes to next chunk.
(defun funk/polymode-eval-R-chunk-and-next ()
  "Evals a R chunk and move point to next chunk."
  (interactive)
  (funk/polymode-eval-R-chunk)
  (funk/polymode-next-chunk))

;; Mark a word at a point.
;; http://www.emacswiki.org/emacs/ess-edit.el
(defun ess-edit-word-at-point ()
  (save-excursion
    (buffer-substring
     (+ (point) (skip-chars-backward "a-zA-Z0-9._"))
     (+ (point) (skip-chars-forward "a-zA-Z0-9._")))))

;; Eval any word where the cursor is (objects, functions, etc).
(defun funk/ess-eval-word ()
  (interactive)
  (let ((x (ess-edit-word-at-point)))
    (ess-eval-linewise (concat x))))

;; folding rmd text and code-------------------------------------------------------
;; FROM: https://github.com/polymode/polymode/issues/128

(defun funk/rmd-fold-region (beg end &optional msg)
  "Eval all spans within region defined by BEG and END.
MSG is a message to be passed to `polymode-eval-region-function';
defaults to \"Eval region\"."
  (interactive "r")
  (save-excursion
    (let* ((base (pm-base-buffer))
                                        ; (host-fun (buffer-local-value 'polymode-eval-region-function (pm-base-buffer)))
           (host-fun (buffer-local-value 'polymode-eval-region-function base))
           (msg (or msg "Eval region"))
           )
      (if host-fun
          (pm-map-over-spans
           (lambda (span)
             (when (eq (car span) 'body)
               (with-current-buffer base
                 (ignore-errors (vimish-fold (max beg (nth 1 span)) (min end (nth 2 span)))))))
           beg end)
        (pm-map-over-spans
         (lambda (span)
           (when (eq (car span) 'body)
             (setq mapped t)
             (when polymode-eval-region-function
               (setq evalled t)
               (ignore-errors (vimish-fold
                               (max beg (nth 1 span))
                               (min end (nth 2 span))
                               )))))
         beg end)
        ))))

(defun funk/rmd-fold-codes ()
  "Eval all inner chunks in the buffer. "
  (interactive)
  (vimish-fold-delete-all)
  (funk/rmd-fold-region (point-min) (point-max) "Eval buffer")
  (polymode-previous-chunk 1)
  (next-line 2)
  )

(defun re-seq (regexp string n)
  "Get a list of all regexp matches in a string"
  (save-match-data
    (let ((pos 0)
          matches)
      (while (string-match regexp string pos)
        (add-to-list 'matches (+ n (string-match regexp string pos)) t)
        (setq pos (match-end 0)))
      matches)))


(defun funk/rmd-fold-text ()
  (interactive)
  (vimish-fold-delete-all)
  (let ((begs (re-seq "```{" (buffer-string) 0))
        (ends (re-seq "```$" (buffer-string) 0)))
    (add-to-list 'begs (point-max) t)
    (while ends  (progn
                   (ignore-errors (vimish-fold (nth 1 begs) (nth 0 ends)))
                   (pop begs)
                   (pop ends))))
  (beginning-of-buffer)
  (polymode-next-chunk 2))

;; https://emacs.stackexchange.com/questions/63308/clear-console-output-on-r-buffers-when-using-ess
(defun funk/r-clear-buffer ()
  (interactive)
  (let ((r-repl-buffer (seq-find (lambda (buf)
                                   (string-prefix-p "*R" (buffer-name buf)))
                                 (buffer-list))))
    (if r-repl-buffer
        (with-current-buffer r-repl-buffer
          (comint-clear-buffer))
      (user-error "No R REPL buffers found"))))


(defun funk/ess-quit-kill-inferior ()
  (interactive)
  (let ((r-repl-buffer (seq-find (lambda (buf)
                                   (string-prefix-p "*R:" (buffer-name buf)))
                                 (buffer-list))))
    (if r-repl-buffer
        (with-current-buffer r-repl-buffer
          (kill-buffer r-repl-buffer))
      (user-error "No R REPL buffers found"))))

(defun ess-abort ()
  (interactive)
  (interrupt-process (ess-get-process)))
(define-key ess-mode-map (kbd "C-c C-a") 'ess-abort)
(define-key inferior-ess-mode-map (kbd "C-c C-a") 'ess-abort)


(define-key ess-mode-map (kbd "C-c l") #'funk/r-clear-buffer)
(advice-add #'funk/ess-quit-kill-inferior :around #'auto-yes)

;; FROM: https://github.com/emacs-ess/ESS/issues/686#issuecomment-475331573
(defun funk/ess-xref-find-etags ()
  (interactive)
  (let ((xref-backend-functions '(etags--xref-backend))
        (thing (xref--read-identifier "Find definitions of: ")))
    (xref--find-definitions (replace-regexp-in-string "^\\.\\|\\.$\\|^[^/]+/" "" thing) nil)))
;; meta get tag
(define-key ess-mode-map (kbd "M-g M-t") 'funk/ess-xref-find-etags)

(defun funk/ess-styler ()
  "Emacsy way of ~/.local/bin/tools/r/rstylerfile.
Adaptated from: https://emacs.stackexchange.com/a/50455/31478"
  (interactive)
  (let ((tempfile (make-temp-file nil nil ".R"))
        (reg-beg (region-beginning))
        (reg-end (region-end)))
    (if (region-active-p)
        (progn
          ;; save user selected region to a tempfile
          (write-region reg-beg reg-end tempfile)
          ;; Run the command synchronously (i.e., blocking Emacs) and
          ;; do not popup the *Shell Command Output* buffer
          (save-window-excursion
            (shell-command (format "Rscript -e 'styler::style_file(\"%s\")'" tempfile)))
          ;; kill the user selected region and replace the original
          ;; text using the formated text in the tempfile
          (kill-region reg-beg reg-end)
          (insert-file-contents tempfile)
          (message "INFO from `funk/ess-styler': Check results in the *Shell Command Output* buffer."))
      (message "Error: Please, select a region first!"))))


(provide 'funk-rstats)
;;; funk-rstats.el ends here
