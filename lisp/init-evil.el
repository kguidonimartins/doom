;;; init-evil.el --- Evil configs -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; evil

(use-package! evil
  :init
  (setq evil-undo-system 'undo-fu)
  (setq sentence-end "[\\.\\?\\!\\;\\:\\-] +") ;; . or ? or ! followed by spaces.
  (setq evil-ex-search-direction 'forward)
  (setq evil-want-keybinding nil)
  (setq evil-want-integration t)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump t)
  (setq evil-delete-backward-char-and-join nil)
  (setq evil-insert-digraph nil)
  (setq evil-want-Y-yank-to-eol t)
  ;; edit by visual lines
  (setq evil-respect-visual-line-mode nil)

  :config
  (evil-mode 1)
  ;; evil-normal-state is preferred, so revert when idle
  (run-with-idle-timer 60 t 'evil-normal-state)
  ;; don't move cursor back when exiting insert state
  (setq evil-move-cursor-back nil)
  ;; allow end of line movement
  (setq evil-move-beyond-eol nil)
  ;; highlight closing bracket like vim not emacs
  (setq evil-highlight-closing-paren-at-point-states '(not emacs insert replace))
  ;; whether to allow evil-char move across lines
  (setq evil-cross-lines nil)
  ;; fine-grained undo
  (setq evil-want-fine-undo t)
  ;; hide mode-line state indicator
  (setq evil-mode-line-format nil)

  (setq evil-emacs-state-cursor  '("#000000" box)
        evil-normal-state-cursor '("#f0cc09" box)
        evil-visual-state-cursor '("#ff6e67" box)
        evil-insert-state-cursor '("#f0cc09" bar))

  (setq evil-insert-state-message nil)
  (setq evil-visual-state-message nil)

  ;; disable some keys because company-ui bindings
  (define-key evil-insert-state-map (kbd "C-h") nil)
  (define-key evil-insert-state-map (kbd "C-j") nil)
  (define-key evil-insert-state-map (kbd "C-k") nil)

  ;; simulate emacs bindings
  (define-key evil-insert-state-map (kbd "C-a") 'beginning-of-line)
  (define-key evil-insert-state-map (kbd "C-e") 'end-of-line)

  ;; ;; navigating outline headings: just use `]]' or `[['.
  ;; (define-key evil-normal-state-map (kbd "gn") 'outline-next-heading)
  ;; (define-key evil-normal-state-map (kbd "gp") 'outline-previous-heading)

  ;; my precious
  (define-key evil-normal-state-map (kbd "C-p") 'funk/counsel-file-jump)

  ;; disable for outline
  (define-key evil-normal-state-map (kbd "C-.") 'counsel-outline)

  ;; new-line also in normal mode
  ;; (define-key evil-normal-state-map (kbd "C-m") 'newline)

  ;; insert project filename here
  ;; (define-key evil-insert-state-map (kbd "C-l") 'funk/insert-file-name-here)

  ;; move lines
  (define-key evil-visual-state-map (kbd "C-j") (concat ":m '>+1" (kbd "RET") "gv=gv"))
  (define-key evil-visual-state-map (kbd "C-k") (concat ":m '<-2" (kbd "RET") "gv=gv"))
  (define-key evil-normal-state-map (kbd "C-j") (concat ":m +1" (kbd "RET") "=="))
  (define-key evil-normal-state-map (kbd "C-k") (concat ":m -2" (kbd "RET") "=="))

  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-normal-state-map (kbd "C-w \\") 'evil-window-prev)

  ;; because of the weird behavior of the swiper, which I cannot understand yet.
  (define-key evil-normal-state-map (kbd "/") 'swiper)
  (define-key evil-normal-state-map (kbd "n") 'evil-search-previous)
  (define-key evil-normal-state-map (kbd "N") 'evil-search-next)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  ;; Like find file
  (define-key evil-insert-state-map (kbd "C-x C-f") 'company-files)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'async-shell-command-buffer 'normal)
  (evil-set-initial-state 'comint-mode 'normal)
  (evil-set-initial-state 'ess-r-help-mode 'normal)
  (evil-set-initial-state 'inferior-ess-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal)
  (evil-set-initial-state 'git-commit-mode 'insert)
  (evil-set-initial-state 'git-gutter+-commit-mode 'insert)

  ;; don't quit emacs when using `:q'
  (evil-ex-define-cmd "q" #'kill-this-buffer)
  ;; save and kill buffers without quiting emacs...
  ;; but works as usual when editing commits in magit
  (evil-ex-define-cmd "wq" #'funk/save-and-kill-this-buffer)
  ;; prevents `qa' to quit emacs
  (evil-ex-define-cmd "qa" #'projectile-kill-buffers)
  (evil-ex-define-cmd "wqa" #'funk/save-and-kill-project-buffers)

  ;; permanent marks
  (add-to-list 'desktop-locals-to-save 'evil-markers-alist)

  ;;   (defun evil-paste-after-from-0 ()
  ;;     "Paste multiple times after yank.
  ;; From: https://github.com/syl20bnr/spacemacs/issues/5070#issuecomment-184464650
  ;; See also:
  ;; https://vim.fandom.com/wiki/Remembering_previous_deletes/yanks
  ;; https://vim.fandom.com/wiki/Copy,_cut_and_paste"
  ;;     (interactive)
  ;;     (let ((evil-this-register ?0))
  ;;       (call-interactively 'evil-paste-after)))
  ;;   (setq-default evil-kill-on-visual-paste nil)

  ;;   (define-key evil-visual-state-map "p" 'evil-paste-after-from-0)

  ;;;###autoload
  (defun funk/save-and-kill-this-buffer ()
    "Force write and close buffers. Works on Magit commit message too."
    (interactive)
    (if (string-equal (buffer-name) "COMMIT_EDITMSG")
        (with-editor-finish +1)
      (save-buffer)
      (kill-this-buffer)))

  ;;;###autoload
  (defun funk/save-and-kill-project-buffers ()
    "Kill all project buffers."
    (interactive)
    (projectile-save-project-buffers)
    (projectile-kill-buffers)
    (eyebrowse-close-window-config))
  (advice-add #'funk/save-and-kill-project-buffers :around #'auto-yes)

  ;;;###autoload
  (defun funk/evil-center-line (&rest _)
    "Used after `evil-search-next'.
From: https://github.com/noctuid/evil-guide#whats-the-equivalent-of-nnoremap-n-nzz"
    (evil-scroll-line-to-center nil))

  (advice-add 'evil-backward-paragraph :after #'funk/evil-center-line)
  (advice-add 'evil-forward-paragraph  :after #'funk/evil-center-line)
  (advice-add 'evil-jump-backward      :after #'funk/evil-center-line)
  (advice-add 'evil-jump-forward       :after #'funk/evil-center-line)
  (advice-add 'evil-scroll-down        :after #'funk/evil-center-line)
  (advice-add 'evil-scroll-page-down   :after #'funk/evil-center-line)
  (advice-add 'evil-scroll-page-up     :after #'funk/evil-center-line)
  (advice-add 'evil-search-next        :after #'funk/evil-center-line)
  (advice-add 'evil-scroll-up          :after #'funk/evil-center-line)
  (advice-add 'evil-search-previous    :after #'funk/evil-center-line)

  (advice-add 'evil-goto-definition :after #'funk/try-expand-outline)

  (advice-add 'evil-beginning-of-line :after #'pulse-momentary-highlight-one-line)

  ;;;###autoload
  (defun funk/insert-file-name-here(filename &optional args)
    "Insert name of file FILENAME into buffer after point."
    (interactive "*fInsert file name: \nP")
    (cond ((equal args '-)
           ;; M-- insert /home/user/.emacs.d/lisp/init-keybinding.el
           (insert (expand-file-name filename)))
          ((equal args '(4))
           ;; C-u insert ~/.emacs.d/lisp/init-keybinding.el
           (insert (abbreviate-file-name filename)))
          ((equal args '(16))
           ;; C-u C-u insert init-keybinding.el
           (insert (file-relative-name filename)))
          (t
           ;; insert lisp/init-keybinding.el
           (insert (string-replace (abbreviate-file-name (projectile-project-root)) "" filename))))
    (message "Inserting: %s" filename))

  ;; define new text objects
  (defmacro define-and-bind-text-object (key start-regex end-regex)
    "Define new text objects for evil-mode.
For example: change inner commas or change inner $.
from: https://stackoverflow.com/a/22418983/5974372"
    (let ((inner-name (make-symbol "inner-name"))
          (outer-name (make-symbol "outer-name")))
      `(progn
         (evil-define-text-object ,inner-name (count &optional beg end type)
           (evil-select-paren ,start-regex ,end-regex beg end type count nil))
         (evil-define-text-object ,outer-name (count &optional beg end type)
           (evil-select-paren ,start-regex ,end-regex beg end type count t))
         (define-key evil-inner-text-objects-map ,key (quote ,inner-name))
         (define-key evil-outer-text-objects-map ,key (quote ,outer-name)))))

  (define-and-bind-text-object "$" "\\$" "\\$")
  (define-and-bind-text-object "," "," ",")

  (add-hook 'git-commit-mode-hook 'evil-insert-state)
  (add-hook 'git-gutter+-commit-mode-hook 'evil-insert-state)
  (add-hook 'compilation-mode-hook 'evil-normal-state)
  (add-hook 'shell-mode-hook 'evil-normal-state)
  (add-hook 'org-capture-mode-hook 'evil-insert-state)
  (add-hook 'org-log-buffer-setup-hook 'evil-insert-state)

  ;; NOTE: workaround for `ciW'. Without this advice,
  ;; `ciW' change `...thi|s)' to `...|'.
  (defalias #'forward-evil-WORD #'forward-evil-symbol)

  ;; NOTE: workaround to treat `_' as part of a word in the
  ;; languages listed below. Thus, `ciw' correctly capture
  ;; word (or symbols) as `test_test_test'.
  (add-hook 'ess-mode-hook #'(lambda () (modify-syntax-entry ?_ "w")))
  (add-hook 'emacs-lisp-mode-hook #'(lambda () (modify-syntax-entry ?- "w")))
  (add-hook 'python-mode-hook #'(lambda () (modify-syntax-entry ?_ "w")))

  ;;;###autoload
  (defun funk/indent-region ()
    "Works like `vip=' in evil normal mode, but it is binded to `,I'"
    (interactive)
    (save-excursion
      (progn
        (call-interactively 'evil-visual-char)
        (call-interactively 'evil-inner-paragraph)
        (indent-region (region-beginning) (region-end))
        ))
    )

  ;;;###autoload
  (defun q ()
    "Simulates `:q' or `kill-this-buffer'."
    (interactive)
    (kill-this-buffer))

  ;;;###autoload
  (defun qa ()
    "Simulates `:qa' or `projectile-kill-buffers'."
    (interactive)
    (projectile-kill-buffers))

  ;;;###autoload
  (defun w ()
    "Simulates `:w' or `save-buffer'."
    (interactive)
    (save-buffer))

  ;;;###autoload
  (defun wq ()
    "Simulates `:wq' or `funk/save-and-kill-this-buffer'."
    (interactive)
    (funk/save-and-kill-this-buffer))

  ;;;###autoload
  (defun wqa ()
    "Simulates `:wqa' or `funk/save-and-kill-project-buffers'."
    (interactive)
    (funk/save-and-kill-project-buffers))

  ;; https://github.com/noctuid/evil-guide#jump
  ;; https://github.com/gilbertw1/better-jumper
  (evil-add-command-properties #'counsel-outline                     :jump t)
  (evil-add-command-properties #'counsel-projectile-switch-to-buffer :jump t)
  (evil-add-command-properties #'counsel-switch-buffer               :jump t)
  (evil-add-command-properties #'evil-next-line                      :jump t)
  (evil-add-command-properties #'evil-previous-line                  :jump t)
  (evil-add-command-properties #'git-gutter+-next-hunk               :jump t)
  (evil-add-command-properties #'git-gutter+-previous-hunk           :jump t)
  (evil-add-command-properties #'helpful-visit-reference             :jump t)
  (evil-add-command-properties #'ivy-switch-buffer                   :jump t)
  (evil-add-command-properties #'scroll-buffer-down                  :jump t)
  (evil-add-command-properties #'scroll-buffer-up                    :jump t)

  ;; https://github.com/noctuid/evil-guide#ex-command-definition
  (evil-ex-define-cmd "mu[4e]" 'mu4e)
  (evil-ex-define-cmd "t" "mu4e")

  ;; https://github.com/noctuid/evil-guide#binding-keys-to-keys-keyboard-macros
  (evil-define-key 'normal 'global
    ;; select the previously pasted text and indent
    ;; use `gv' to get the latest selected text
    "gp" "`[v`]="
    ;; run the macro in the q register
    "Q" "@q")

  (defun funk/evil-easy-find-replace ()
    (interactive)
    (let ((from (read-from-minibuffer "replace from: "))
          (to (read-from-minibuffer "replace to: ")))
      (setq-local evil-string-search (concat "%s#" from "#" to "#g"))
      (evil-ex evil-string-search)))

  (define-key evil-normal-state-map (kbd "S") 'funk/evil-easy-find-replace)

  (evil-define-key 'visual 'global
    ;; run macro in the q register on all selected lines
    "Q" (kbd ":norm @q RET")
    ;; repeat on all selected lines
    "." (kbd ":norm . RET"))

  (defun funk/evil-visual-tab ()
    "Indent region if in visual-line-mode, otherwise select contains inside a pair of tags via `evil-jump-item'"
    (interactive)
    (if (eq evil-visual-selection 'line)
        (indent-region (region-beginning) (region-end))
      (evil-jump-item)))

  (general-define-key :keymaps 'visual
                      "<tab>" 'funk/evil-visual-tab)

  (defun funk/evil--recenter-after-goto-point-max (count)
    "Thin wrapper around `evil-scroll-line-to-center' so center the end-of-buffer after a G motion."
    (unless count
      (recenter nil)))

  (advice-add #'evil-goto-line :after #'funk/evil--recenter-after-goto-point-max))

;;; undo-tree
(use-package! undo-tree
  ;; :defer 3
  :init
  (setq undo-limit 78643200)
  (setq undo-outer-limit 104857600)
  (setq undo-strong-limit 157286400)
  (setq undo-tree-mode-lighter " UN")
  (setq undo-tree-auto-save-history t)
  (setq undo-tree-enable-undo-in-region nil)
  ;; Persistent undo-tree history across emacs sessions
  (setq funk/undo-tree-history-dir (let ((dir (concat user-emacs-directory
                                                      "var/undo/")))
                                     (make-directory dir :parents)
                                     dir))
  (setq undo-tree-history-directory-alist `(("." . ,funk/undo-tree-history-dir)))
  (add-hook 'undo-tree-visualizer-mode-hook (lambda ()
                                              (undo-tree-visualizer-selection-mode)
                                              (setq display-line-numbers nil)))
  :config
  (evil-set-undo-system 'undo-tree))

(after! evil
  (global-undo-tree-mode 1))

;;; evil-collection
(use-package! evil-collection
  :after evil
  :config
  (setq evil-collection-outline-bind-tab-p t)
  (evil-collection-init))

;;; evil-commentary
(use-package! evil-commentary
  :after evil
  :config
  (evil-commentary-mode +1))

;;; evil-surround
(use-package! evil-surround
  ;; :defer 5
  :after evil
  :config
  (global-evil-surround-mode t))

;;; evil-matchit
(use-package! evil-matchit
  ;; :defer 5
  :after evil
  :config
  (global-evil-matchit-mode 1))

;;; evil-lion
(use-package! evil-lion
  ;; :defer 5
  ;; vimish EasyAlign
  ;; Press `glip=' or `vipgl='.
  ;; Where:
  ;; `gl' is the operator
  ;; `ip' is the text object
  ;; `='  is the separator aligned by
  :after evil
  :config
  (evil-lion-mode))

;;; evil-string-inflection
(use-package! evil-string-inflection
  :disabled
  ;; :defer 5
  ;; Change between:
  ;; CamelCaseWord
  ;; camelCaseWord
  ;; camel-case-word
  ;; camel_case_word
  ;; CAMEL_CASE_WORD
  ;; CamelCaseWord
  :after evil
  :config
  (define-key evil-normal-state-map "gR" 'evil-operator-string-inflection))

;;; evil-googles
(use-package! evil-goggles
  ;; :defer 2
  ;; blink like `TextYankPost'.
  :after evil
  :config
  (custom-set-faces
   '(evil-goggles-yank-face
     ((t (:weight ultra-bold))))
   )
  (evil-goggles-mode)
  (evil-goggles-use-diff-faces)
  (setq evil-goggles-pulse t)
  (setq evil-goggles-enable-paste t)
  (setq evil-goggles-duration 0.5))

;;; evil-visual-mark-mode
(use-package! evil-visual-mark-mode
  ;; :defer 5
  :init
  (evil-visual-mark-mode)
  :config
  (advice-add 'evil-delete-marks :after
              (lambda (&rest args)
                (evil-visual-mark-cleanup))))

;;; evil-multiedit
(use-package! evil-multiedit
  ;; :defer 5
  :after evil
  :config
  (setq evil-multiedit-follow-matches t)

  (general-define-key
   :states '(normal)
   (kbd "C-S-<return>") 'evil-multiedit-toggle-marker-here
   (kbd "C-<backspace>") 'evil-multiedit-toggle-or-restrict-region
   "C-S-k" 'evil-multiedit-prev
   "C-S-j" 'evil-multiedit-next
   "C-S-n" 'evil-multiedit-match-and-next
   "C-S-p" 'evil-multiedit-match-and-prev
   "C-S-a" 'evil-multiedit-match-all)

  (general-define-key
   :states '(visual)
   (kbd "C-S-<return>") 'evil-multiedit-toggle-marker-here
   "C-S-k" 'evil-multiedit-prev
   "C-S-j" 'evil-multiedit-next
   "C-S-n" 'evil-multiedit-match-symbol-and-next
   "C-S-p" 'evil-multiedit-match-symbol-and-prev
   "C-S-a" 'evil-multiedit-match-all)

  (general-define-key
   :keymaps '(evil-multiedit-state-map)
   (kbd "C-<backspace>") 'evil-multiedit-toggle-or-restrict-region
   "C-S-n" 'evil-multiedit-match-symbol-and-next
   "C-S-p" 'evil-multiedit-match-symbol-and-prev
   "C-S-k" 'evil-multiedit-prev
   "C-S-j" 'evil-multiedit-next))

;;; evil-quickscope
(use-package! evil-quickscope
  :disabled
  ;; :defer 5
  :after evil
  :config
  :hook ((prog-mode . turn-on-evil-quickscope-mode)
         (LaTeX-mode . turn-on-evil-quickscope-mode)
         (ess-mode . turn-on-evil-quickscope-mode)
         (emacs-lisp-mode . turn-on-evil-quickscope-mode)
         (org-mode . turn-on-evil-quickscope-mode)))

;;; evil-textobj-anyblock
(use-package! evil-textobj-anyblock
  :after evil
  :config
  (define-key evil-inner-text-objects-map "b" 'evil-textobj-anyblock-inner-block)
  (define-key evil-outer-text-objects-map "b" 'evil-textobj-anyblock-a-block)
  (add-hook 'ess-mode-hook
            (lambda ()
              (setq-local evil-textobj-anyblock-blocks
                          '(("(" . ")")
                            ("{" . "}")
                            ("\\[" . "\\]")
                            ("\"" . "\"")))))
  )



(provide 'init-evil)
;;; init-evil.el ends here
