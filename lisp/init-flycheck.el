;;; init-flycheck.el --- flycheck configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; flycheck
(use-package flycheck)

;;; flycheck-color-mode-line
(use-package flycheck-color-mode-line
  :after (flycheck)
  :config
  (eval-after-load "flycheck"
    '(add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode)))

;;; flycheck-inline
(use-package flycheck-inline
  :after (flycheck)
  :config
  (with-eval-after-load 'flycheck
    (add-hook 'flycheck-mode-hook #'flycheck-inline-mode)))

;;; flycheck-languagetool
(use-package flycheck-languagetool
  :after (flycheck langtool)
  :hook
  (text-mode . (lambda ()
                 (require 'flycheck-languagetool)))
  (markdown-mode . (lambda ()
                     (require 'flycheck-languagetool)))
  (poly-markdown+r-mode . (lambda ()
                            (require 'flycheck-languagetool)))
  :init
  (setq flycheck-languagetool-commandline-jar "/usr/bin/languagetool"))

(provide 'init-flycheck)
;;; init-flycheck.el ends here
