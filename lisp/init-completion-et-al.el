;; -*- lexical-binding: t; -*-

;; REVIEW: https://www.reddit.com/r/emacs/comments/ol2luk/from_ivy_counsel_to_vertico_consult/

;; (use-package vertico
;;   :bind (:map vertico-map
;;               ("C-j" . vertico-next)
;;               ("C-k" . vertico-previous)
;;               :map minibuffer-local-map
;;               ("C-w" . backward-kill-word))
;;   :config
;;   (setq vertico-cycle t)
;;   :init
;;   (vertico-mode))

(use-package savehist
  ;; :defer 2
  :init
  (savehist-mode +1))

(use-package marginalia
  ;; :defer 2
  :config
  (setq marginalia-annotators '(marginalia-annotators-heavy))
  :init
  (marginalia-mode))

(use-package prescient
  ;; :defer 2
  )
(use-package ivy-prescient
  ;; :defer 2
  )
(use-package selectrum-prescient
  ;; :defer 2
  )

(use-package selectrum
  ;; :defer 2
  :bind
  (:map selectrum-minibuffer-map
        ("TAB" . selectrum-insert-current-candidate)
        ("C-l" . selectrum-insert-current-candidate)
        ("C-j" . selectrum-next-candidate)
        ("C-k" . selectrum-previous-candidate))
  :config
  (selectrum-mode +1)
  ;; to make sorting and filtering more intelligent
  (selectrum-prescient-mode +1)

  ;; to save your command history on disk, so the sorting gets more
  ;; intelligent over time
  (prescient-persist-mode +1)
  (setq selectrum-fix-vertical-window-height t)
  (setq selectrum-max-window-height 20)

  ;; complete filenames at poiny
  (autoload 'ffap-file-at-point "ffap")
  (add-hook 'completion-at-point-functions
            (defun complete-path-at-point+ ()
              (let ((fn (ffap-file-at-point))
                    (fap (thing-at-point 'filename)))
                (when (and (or fn
                               (equal "/" fap))
                           (save-excursion
                             (search-backward fap (line-beginning-position) t)))
                  (list (match-beginning 0)
                        (match-end 0)
                        #'completion-file-name-table)))) 'append)

  (defun funk/selectrum-select-current-candidate-if-not-dir ()
    "Select the current candidate. If, however, the current selection is a directory, enter the directory instead of opening it using `dired'."
    (interactive)
    (let*  ((input (minibuffer-contents-no-properties))
            (index selectrum--current-candidate-index)
            (candidate (selectrum--get-candidate index))
            (dir (if (directory-name-p input) input (file-name-directory input))))
      (if (and dir (directory-name-p candidate))
          (progn
            (delete-minibuffer-contents)
            (insert (format "%s%s" dir candidate)))
        (selectrum-select-current-candidate))))

  (defun funk/selectrum-unified-tab ()
    "<tab> does the following things
1. if there is a common part among candidates, complete the common part;
2. if there is only one candidate, select the candidate
3. if the last command is `funk/selectrum-unified-tab', or `selectrum--current-candidate-index' is not 0/-1 (the top candidate), then select the current candidate"
    (interactive)
    (when selectrum--current-candidate-index
      (let* ((common (try-completion "" selectrum--refined-candidates)))
        (cond
         ;; case 3
         ((or (eq last-command this-command)
              (not (memq selectrum--current-candidate-index '(0 -1))))
          (funk/selectrum-select-current-candidate-if-not-dir))
         ;; case 2
         ((= 1 (length selectrum--refined-candidates))
          (funk/selectrum-select-current-candidate-if-not-dir))
         ;; case 1
         ((not (string= common ""))
          (let*  ((input (minibuffer-contents-no-properties))
                  (dir (if (directory-name-p input) input (file-name-directory input))))
            (if dir
                (progn
                  (delete-minibuffer-contents)
                  (insert (format "%s%s" dir common)))
              (insert common))))))))

  (general-define-key :keymaps 'selectrum-minibuffer-map
                      "<tab>" #'funk/selectrum-unified-tab))

(use-package embark
  ;; :defer 5
  ;; REVIEW: https://karthinks.com/software/fifteen-ways-to-use-embark/

  :bind
  (("C-:" . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))

  ;; (defun embark-which-key-indicator ()
  ;;     "An embark indicator that displays keymaps using which-key.
  ;; The which-key help message will show the type and value of the
  ;; current target followed by an ellipsis if there are further
  ;; targets."
  ;;     (lambda (&optional keymap targets prefix)
  ;;       (if (null keymap)
  ;;           (which-key--hide-popup-ignore-command)
  ;;         (which-key--show-keymap
  ;;          (if (eq (caar targets) 'embark-become)
  ;;              "Become"
  ;;            (format "Act on %s '%s'%s"
  ;;                    (plist-get (car targets) :type)
  ;;                    (embark--truncate-target (plist-get (car targets) :target))
  ;;                    (if (cdr targets) "…" "")))
  ;;          (if prefix
  ;;              (pcase (lookup-key keymap prefix 'accept-default)
  ;;                ((and (pred keymapp) km) km)
  ;;                (_ (key-binding prefix 'accept-default)))
  ;;            keymap)
  ;;          nil nil t))))

  ;; (setq embark-indicators
  ;;       '(embark-which-key-indicator
  ;;         embark-highlight-indicator
  ;;         embark-isearch-highlight-indicator))
  )


(use-package consult
  ;; :defer 5
  )
(use-package consult-dir
  ;; :defer 5
  )

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  ;; :defer 5
  :after (embark consult)
  ;; if you want to have consult previews as you move around an
  ;; auto-updating embark collect buffer
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(provide 'init-completion-et-al)
