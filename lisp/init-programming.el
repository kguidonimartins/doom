;;; -*- lexical-binding: t; -*-

;; Check (on save) whether the file edited contains a shebang, if yes,
;; make it executable from
;; http://mbork.pl/2015-01-10_A_few_random_Emacs_tips
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

(setq require-final-newline nil)

;;; editorconfig
(use-package editorconfig
  :config
  (editorconfig-mode 1))

;;; yaml-mode
(use-package yaml-mode
  ;; :defer 5
  )

;;; restclient (like postman)
;; https://github.com/pashky/restclient.el

(use-package restclient
  :config
  (add-to-list 'auto-mode-alist '("\\.http" . restclient-mode))
  (defun funk/restclient-eval-request-and-stay ()
    (interactive)
    (restclient-http-send-current nil t))
  (general-define-key
   :keymaps '(restclient-mode-map)
   :states '(normal)
   ",,"      #'funk/restclient-eval-request-and-stay
   "SPC SPC" #'funk/restclient-eval-request-and-stay
   "C-c C-c" #'funk/restclient-eval-request-and-stay))

;; https://github.com/iquiw/company-restclient
(use-package  company-restclient)

;; https://github.com/alf/ob-restclient.el

;;; format-all
;; like vim's ale
(use-package format-all)

;;; electric

(use-package electric
  :init
  (progn
    (electric-pair-mode 1))
  :config
  (setq-default electric-pair-inhibit-predicate 'electric-pair-conservative-inhibit)
  ;; (setq-default electric-pair-inhibit-predicate
  ;;               (lambda (c)
  ;;                 (if (looking-at "[ \n\t]")
  ;;                     (electric-pair-default-inhibit c)
  ;;                   t)))
  )

;;; debug
;; (use-package dap-mode
;;   ;; Uncomment the config below if you want all UI panes to be hidden by default!
;;   ;; :custom
;;   ;; (lsp-enable-dap-auto-configure nil)
;;   ;; :config
;;   ;; (dap-ui-mode 1)
;;   :config
;;   ;; Set up Node debugging
;;   (require 'dap-node)
;;   (dap-node-setup) ;; Automatically installs Node debug adapter if needed
;;   ;; Bind `C-c l d` to `dap-hydra` for easy access
;;   (general-define-key
;;    :keymaps 'lsp-mode-map
;;    :prefix lsp-keymap-prefix
;;    "d" '(dap-hydra t :wk "debugger")))

;;; wakatime-mode
(use-package wakatime-mode
  :ensure-system-package
  ((wakatime . "yay -S --noconfirm wakatime-cli-bin"))
  :diminish wakatime-mode
  :if (executable-find "wakatime")
  :init
  (setq wakatime-api-key (funk-auth 'wakatimekey))
  (setq wakatime-cli-path (executable-find "wakatime"))
  :config
  (global-wakatime-mode +1)
  ;; global-wakatime-mode breaks recovering autosaves for some reason
  (advice-add 'recover-this-file :around
              (lambda (oldfn &rest args)
                (let ((wakatime-was-enabled global-wakatime-mode))
                  (when wakatime-was-enabled
                    (global-wakatime-mode -1))
                  (apply oldfn args)
                  (when wakatime-was-enabled
                    (global-wakatime-mode))))))

;;; copy-as-format
(use-package copy-as-format
  ;; :defer 5
  :bind (("C-c w m" . copy-as-format-markdown)
         ("C-c w g" . copy-as-format-slack)
         ("C-c w o" . copy-as-format-org-mode)
         ("C-c w r" . copy-as-format-rst)
         ("C-c w s" . copy-as-format-github)
         ("C-c w w" . copy-as-format))
  :init
  (setq copy-as-format-default "github"))

;;; xref
(use-package xref
  ;; :defer 5
  :config
  (add-to-list 'xref-prompt-for-identifier #'xref-find-references 'append))

;;; flymake
(use-package flymake
  ;; :defer 5
  )

(use-package flymake-shellcheck
  ;; :defer 5
  :commands flymake-shellcheck-load
  :init
  (add-hook 'sh-mode-hook 'flymake-shellcheck-load))

;;; lsp-mode
(use-package lsp-mode
  ;; :defer 5
  :commands (lsp lsp-deferred)
  :config
  (setq lsp-keymap-prefix "C-c C-l")  ;; Or 'C-l', 's-l'
  (setq lsp-diagnostic-package :none)
  (setq lsp-enable-symbol-highlighting nil)
  (setq lsp-ui-doc-show-with-cursor nil)
  (setq lsp-headerline-breadcrumb-enable nil)
  (lsp-enable-which-key-integration t))

;; lsp-ui
(use-package lsp-ui
  ;; :defer 5
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-position 'top))

;; (require 'keytar)
;; (use-package lsp-grammarly
;;   :hook ((text-mode markdown-mode) . (lambda ()
;;                                        (require 'lsp-grammarly)
;;                                        (lsp))))  ; or lsp-deferred

;; ;; lsp-ivy
;; (use-package lsp-ivy)

;;; eglot
(use-package eglot
  ;; :defer 5
  :config
  (setq eglot-ignored-server-capabilites '(:documentHighlightProvider
                                           :hoverProvider))
  ;; (define-key evil-normal-state-map (kbd "K") 'eldoc-doc-buffer)
  (define-key eglot-mode-map (kbd "C-c o") 'eglot-code-action-organize-imports)
  (define-key eglot-mode-map (kbd "C-c h") 'eldoc)
  (define-key eglot-mode-map (kbd "<f6>") 'xref-find-definitions)
  (add-to-list 'eglot-server-programs
               `(python-mode . ("pyls" "-v" "--tcp" "--host"
                                "localhost" "--port" :autoport))))

(use-package counsel-etags
  :disabled
  :after counsel
  ;; :defer 5
  )


;; (use-package eldoc-box
;;   :hook ((text-mode prog-mode ess-r-mode) . eldoc-box-hover-mode))

;;; makefile-executor
(use-package makefile-executor
  :disabled
  ;; :defer t
  :config
  (add-hook 'makefile-mode-hook 'makefile-executor-mode))

;;; dumb-jump

;;   :config
;;   (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

;; ;;; expand-region
;; (use-package expand-region
;;   :disabled
;;   :defer 5
;;   :bind ("C-+" . er/expand-region))

;;; citre (ctags)
;; (use-package citre
;;   :config
;;   (require 'citre)
;;   (require 'citre-config)
;;   (setq
;;    citre-use-project-root-when-creating-tags t
;;    citre-prompt-language-for-ctags-command t
;;    citre-auto-enable-citre-mode-modes '(prog-mode ess-mode)))

;;; tree-sitter
(use-package tree-sitter
  :ensure tree-sitter-langs
  :diminish
  :hook ((after-init . global-tree-sitter-mode)
         (tree-sitter-after-on . tree-sitter-hl-mode)))

;;; misc
(advice-add 'compile-goto-error :after #'funk/try-expand-outline)

(provide 'init-programming)
;;; activity-watch-mode

(use-package activity-watch-mode
  :config
  (global-activity-watch-mode))

;;; init-programming.el ends here
