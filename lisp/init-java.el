;;; init-java.el --- java config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package lsp-java
  :config (add-hook 'java-mode-hook 'lsp))


(provide 'init-java)
;;; init-java.el ends here
