;;; init-ui.el --- Configuration for Emacs UI -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; dashboard

;;; help
(setq help-window-select t)


;;; goto-address-mode
(global-goto-address-mode +1)

;;; link-hint
(use-package! link-hint
  :after evil
  :config
  (define-key evil-normal-state-map (kbd "SPC f") 'link-hint-open-link))

;;; frame transparency
;; Make frame transparency overridable
(defvar funk/frame-transparency '(100 . 97)
  "Alist defining the transparency values for active and inactive frames.")

;; Set frame transparency
(set-frame-parameter (selected-frame) 'alpha funk/frame-transparency)
(add-to-list 'default-frame-alist `(alpha . ,funk/frame-transparency))

;; Set maximized frame
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;;; window divider

;; (setq window-divider-default-bottom-width 1
;;       window-divider-default-right-width 1)

;; (window-divider-mode)

;;; miscellaneous UI configurations

(setq max-mini-window-height 10)
(setq use-dialog-box nil)

(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(menu-bar-mode -1)

(context-menu-mode)

(setq-default cursor-in-non-selected-windows nil)

(blink-cursor-mode +1)

(setq use-short-answers t) ; emacs 28

(setq inhibit-startup-message t)

(setq visible-bell nil)

(column-number-mode)
(global-visual-line-mode t) ; word wrap; dont show arrow

(setq global-display-fill-column-indicator-mode nil)
(setq fill-column 79)

;; Disable line numbers
(global-display-line-numbers-mode 0)
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                treemacs-mode-hook
                eshell-mode-hook
                inferior-ess-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(setq confirm-kill-emacs 'y-or-n-p)

;; nice scrolling
(setq scroll-step 1)
(setq scroll-margin 0
      scroll-conservatively 100000
      scroll-preserve-screen-position 1
      scroll-preserve-screen-position 'always
      next-screen-context-lines 5)

;; Echo keystrokes a little faster
(setq echo-keystrokes 0.1)


;; turn on highlight matching brackets
(show-paren-mode 1)

;; highlight brackets
;; can be: parenthesis, expression, or mixed
(setq show-paren-style 'parenthesis)

;; ;; maintain a header in all buffers
;; (defun funk/custom-header ()
;;   (setq header-line-format " "))
;; (add-hook 'buffer-list-update-hook 'funk/custom-header)

;; ;; set a internal border
;; (add-to-list 'default-frame-alist '(internal-border-width . 15))
(set-fringe-mode 10)

;; Remove trailing white spaces
(add-hook 'before-save-hook 'whitespace-cleanup)


;; remove truncate line indicator
(setq-default fringe-indicator-alist (assq-delete-all 'truncation fringe-indicator-alist))

;; (setq-default fringe-indicator-alist
;;               '((continuation nil nil)
;;                 (truncation nil nil)
;;                 (overlay-arrow . nil)
;;                 (up . nil)
;;                 (down . nil)
;;                 (top nil nil)
;;                 (bottom nil nil nil nil)
;;                 (top-bottom nil nil nil nil)
;;                 (empty-line . nil)
;;                 (unknown . nil)))

;; don't truncate line in occur-mode
(add-hook 'occur-mode-hook (lambda ()
                             (visual-line-mode -1)
                             (toggle-truncate-lines 1)
                             ))
;; title bar
;; ;; Emacs title bar to reflect file name
(defun frame-title-string ()
  "Return the file name of current buffer, using ~ if under home directory"
  (let
      ((fname (or
               (buffer-file-name (current-buffer))
               (buffer-name)))

       (host (if (file-remote-p default-directory)
                 (tramp-file-name-host
                  (tramp-dissect-file-name default-directory))
               (system-name)))
       )

    ;; ;;let body
    ;; (when (string-match (getenv "HOME") fname)
    ;;   (setq fname (replace-match "~" t t fname)))
    ;; fname))
    ;;let body
    (setq fname (concat "[emacs@" host "] " fname))
    fname))

;; ;;; Title = '~/file/path [major mode] - current emacs version'
;; (if (file-remote-p default-directory)
;;     nil
;;   (setq frame-title-format '("" (:eval (frame-title-string)) " [%m] - Emacs " emacs-version)))

(setq frame-title-format '((:eval (frame-title-string))))


;;; hl-block-mode

;; (use-package! hl-block-mode
;;   :disabled
;;   :commands (hl-block-mode)
;;   :config
;;   (setq hl-block-bracket nil)    ;; Match all brackets.
;;   (setq hl-block-single-level t) ;; Only one pair of brackets.
;;   (setq hl-block-style 'bracket) ;; or color-tint
;;   :hook ((prog-mode ess-mode ess-r-mode emacs-lisp-mode) . hl-block-mode))

;;; highlight-parentheses

(use-package! highlight-parentheses
  :config
  (highlight-parentheses-mode +1))

;;; beacon
;; (use-package! beacon
;;   :disabled
;;   :defer 2
;;   ;; cursor visual clues
;;   :config
;;   (beacon-mode -1))

;;; pulse (pulse region like beacon)
(defun pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))

(dolist (command '(
                   scroll-up-command
                   scroll-down-command
                   recenter-top-bottom
                   other-window
                   evil-window-up
                   evil-window-down
                   evil-window-left
                   evil-window-right
                   switch-to-buffer
                   ))
  (advice-add command :after #'pulse-line))

;;; all-the-icons
(use-package! all-the-icons
  ;; :defer 2
  :init
  (when (and (not (member "all-the-icons" (font-family-list)))
             (window-system))
    (all-the-icons-install-fonts t))
  :config
  (setq all-the-icons-mode-icon-alist (remove '(messages-buffer-mode all-the-icons-faicon "file-o" :v-adjust 0.0 :face all-the-icons-dsilver) all-the-icons-mode-icon-alist))
  (add-to-list 'all-the-icons-mode-icon-alist '(messages-buffer-mode all-the-icons-faicon "stack-overflow" :v-adjust 0.0))
  (add-to-list 'all-the-icons-mode-icon-alist '(inferior-ess-mode all-the-icons-faicon "flask" :v-adjust 0.0))
  (add-to-list 'all-the-icons-mode-icon-alist '(inferior-ess-r-mode all-the-icons-faicon "flask" :v-adjust 0.0))
  (add-to-list 'all-the-icons-mode-icon-alist '(ess-r-mode all-the-icons-fileicon "R" :height 0.8))
  (add-to-list 'all-the-icons-mode-icon-alist '(ess-r-help-mode all-the-icons-material "help_outline"))
  ;; (add-to-list 'all-the-icons-mode-icon-alist '(jupyter-repl-mode  all-the-icons-faicon "leaf"))
  (add-to-list 'all-the-icons-mode-icon-alist '(debugger-mode  all-the-icons-material "error_outline"))
  (add-to-list 'all-the-icons-extension-icon-alist '("rproj" all-the-icons-material "fingerprint" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-extension-icon-alist '("shp" all-the-icons-faicon "map" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-extension-icon-alist '("rmd" all-the-icons-faicon "leaf" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-regexp-icon-alist '("^renv\\.lock" all-the-icons-fileicon "R" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-regexp-icon-alist '("^\\.Rprofile$" all-the-icons-fileicon "R" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-regexp-icon-alist '("^tags$" all-the-icons-faicon "tags" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-regexp-icon-alist '("^TAGS$" all-the-icons-faicon "tags" :face all-the-icons-lblue))
  )

;;; rainbow-mode
(use-package! rainbow-mode
  ;; :defer 2
  :init
  (add-hook 'lisp-mode-hook #'rainbow-mode)
  (add-hook 'markdown-mode-hook #'rainbow-mode)
  (add-hook 'org-mode-hook #'rainbow-mode)
  (add-hook 'csv-mode-hook #'rainbow-mode)
  (add-hook 'ess-mode-hook #'rainbow-mode)
  (add-hook 'help-mode-hook #'rainbow-mode)
  (add-hook 'inferior-ess-mode-hook #'rainbow-mode)
  :hook prog-mode
  :config (setq-default rainbow-x-colors-major-mode-list '()))

;;; rainbow-delimiters
(use-package! rainbow-delimiters
  ;; :defer 2
  :hook
  (prog-mode . rainbow-delimiters-mode)
  (ess-r-mode . rainbow-delimiters-mode)
  (ess-mode . rainbow-delimiters-mode))

;;; hl-line-mode
(use-package! hl-line
  ;; :defer 2
  :ensure nil
  :config
  (add-hook 'evil-insert-state-exit-hook (lambda() (hl-line-mode -1)))
  (add-hook 'evil-insert-state-entry-hook (lambda() (hl-line-mode +1)))
  (add-hook 'org-agenda-mode-hook (lambda() (hl-line-mode +1)))
  (add-hook 'csv-mode-hook (lambda() (hl-line-mode +1))))

;;; dimmer
;; (use-package! dimmer
;;   ;; indicates which buffer is currently active
;;   ;; by dimming the faces in the other buffers
;;   ;; :defer 2
;;   :init
;;   (dimmer-mode t)
;;   :config
;;   (setq-default dimmer-fraction 0.4)
;;   (dimmer-configure-hydra)
;;   (dimmer-configure-magit)
;;   (dimmer-configure-which-key)
;;   (dimmer-configure-company-box)
;;   (dimmer-configure-helm)
;;   (dimmer-configure-org)
;;   (setq dimmer-adjustment-mode :foreground)
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "magit-diff:")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*R:")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*rg*")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*Org Agenda*")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*help\\[R\\:")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*helpful")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*Calendar*"))

;;; tab-bar
;; (defun funk/name-tab-by-project-or-default ()
;;   "Return project name if in a project, or default tab-bar name if not.
;; The default tab-bar name uses the buffer name."
;;   (let ((project-name (projectile-project-name)))
;;     (if (string= "-" project-name)
;;         (tab-bar-tab-name-current)
;;       (concat (projectile-project-name) ":::" (buffer-name) " "))))

;; (setq tab-bar-mode t)
;; (setq tab-bar-show t)
;; (setq tab-bar-new-tab-choice "*new-tab*")
;; (setq tab-bar-tab-name-function #'funk/name-tab-by-project-or-default)

;;; centaur-tabs

;; (use-package! centaur-tabs
;;   :init
;;   (centaur-tabs-mode t)
;;   :config
;;   (centaur-tabs-group-by-projectile-project)
;;   (defun centaur-tabs-hide-tab (x)
;;     "Do no to show buffer X in tabs."
;;     (let ((name (format "%s" x)))
;;       (or
;;        ;; Current window is not dedicated window.
;;        (window-dedicated-p (selected-window))
;;        ;; Buffer name not match below blacklist.
;;        (string-prefix-p "*epc" name)
;;        (string-prefix-p "*helm" name)
;;        (string-prefix-p "*Helm" name)
;;        (string-prefix-p "*Compile-Log*" name)
;;        (string-prefix-p "*lsp" name)
;;        (string-prefix-p "*company" name)
;;        (string-prefix-p "*Flycheck" name)
;;        (string-prefix-p "*tramp" name)
;;        (string-prefix-p " *Mini" name)
;;        (string-prefix-p "*help" name)
;;        (string-prefix-p "*straight" name)
;;        (string-prefix-p " *temp" name)
;;        (string-prefix-p "*Help" name)
;;        (string-prefix-p "*mybuf" name)
;;        (string-prefix-p "*which-key*" name)
;;        ;; ;; Is not magit buffer.
;;        ;; (and (string-prefix-p "magit" name)
;;        ;;      (not (file-name-extension name)))
;;        )))
;;   (setq centaur-tabs-style "bar"
;;         centaur-tabs-height 10
;;         centaur-tabs-plain-icons nil
;;         centaur-tabs-set-icons t
;;         centaur-tabs-set-modified-marker nil
;;         centaur-tabs-show-new-tab-button nil
;;         centaur-tabs-show-navigation-buttons nil
;;         centaur-tabs-set-bar 'under
;;         x-underline-at-descent-line t)
;;   (centaur-tabs-headline-match)
;;   (setq centaur-tabs-label-fixed-length 10)
;;   ;; (setq centaur-tabs-gray-out-icons 'buffer)
;;   (centaur-tabs-enable-buffer-reordering)
;;   (setq centaur-tabs-adjust-buffer-order t)
;;   (setq uniquify-separator "/")
;;   (setq uniquify-buffer-name-style 'forward)
;;
;;   (defun centaur-tabs-projectile-buffer-groups ()
;;     "Return the list of group names BUFFER belongs to."
;;     (if centaur-tabs-projectile-buffer-group-calc
;;         (symbol-value 'centaur-tabs-projectile-buffer-group-calc)
;;       (set (make-local-variable 'centaur-tabs-projectile-buffer-group-calc)
;;
;;            (cond
;;             ((string-equal "*Messages*" (buffer-name)) '("Misc"))
;;             ((string-equal "*Backtrace*" (buffer-name)) '("Misc"))
;;             ((string-match-p "*mu4e" (buffer-name)) '("Mail"))
;;             ((condition-case _err
;;                  (projectile-project-root)
;;                (error nil)) (list (projectile-project-name)))
;;             ((memq major-mode '(emacs-lisp-mode python-mode emacs-lisp-mode c-mode
;;                                                 c++-mode javascript-mode js-mode
;;                                                 js2-mode makefile-mode
;;                                                 lua-mode vala-mode)) '("Coding"))
;;             ((memq major-mode '(nxhtml-mode html-mode
;;                                             mhtml-mode css-mode)) '("HTML"))
;;             ((memq major-mode '(org-mode calendar-mode diary-mode)) '("Org"))
;;             ((memq major-mode '(dired-mode)) '("Dir"))
;;             (t '("Other"))))
;;       (symbol-value 'centaur-tabs-projectile-buffer-group-calc)))
;;
;;   (general-define-key
;;    :keymaps '(centaur-tabs-mode-map)
;;    :states '(normal)
;;    "gt" #'centaur-tabs-forward
;;    "gT" #'centaur-tabs-backward
;;    )
;;
;;   (add-hook 'dired-mode-hook 'centaur-tabs-local-mode)
;;   (add-hook 'which-key-mode-hook 'centaur-tabs-local-mode)
;;   :hook
;;   ((which-key-mode . centaur-tabs-local-mode)
;;    (dashboard-mode . centaur-tabs-local-mode)
;;    ;; (term-mode . centaur-tabs-local-mode)
;;    ;; (vterm-mode . centaur-tabs-local-mode)
;;    (calendar-mode . centaur-tabs-local-mode)
;;    (org-agenda-mode . centaur-tabs-local-mode)
;;    (helpful-mode . centaur-tabs-local-mode))
;;   ;; (magit-status-mode . centaur-tabs-local-mode)
;;   ;; :bind
;;   ;; ("C-<prior>" . centaur-tabs-backward)
;;   ;; ("C-<next>" . centaur-tabs-forward)
;;   ;; ("C-c t s" . centaur-tabs-counsel-switch-group)
;;   ;; ("C-c t p" . centaur-tabs-group-by-projectile-project)
;;   ;; ("C-c t g" . centaur-tabs-group-buffer-groups)
;;   ;; (:map evil-normal-state-map
;;   ;;       ("g t" . centaur-tabs-forward)
;;   ;;       ("g T" . centaur-tabs-backward))
;;   )



;;; helpful

(use-package! helpful
  ;; :defer 2
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :config
  ;; HACK: https://github.com/Wilfred/helpful/issues/282#issuecomment-1040299356
  (defvar read-symbol-positions-list nil)
  (defun helpful--autoloaded-p (sym buf)
    "Return non-nil if function SYM is autoloaded."
    (-when-let (file-name (buffer-file-name buf))
      (setq file-name (s-chop-suffix ".gz" file-name))
      (help-fns--autoloaded-p sym))))

;;; elisp-demos
(use-package!  elisp-demos
  :config
  (advice-add 'describe-function-1 :after #'elisp-demos-advice-describe-function-1)
  (advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update)
  )

;;; whitespace-mode

(global-whitespace-mode)

;; Display whitespace characters globally
(setq whitespace-line-column 120)

;; Customize Whitespace Characters
;;  - Newline: \u00AC = ¬
;;  - Tab:     \u2192 = →
;;             \u00BB = »
;;             \u25B6 = ▶
(setq-default whitespace-display-mappings
              (quote ((newline-mark ?\n [?\u00AC ?\n] [?\u00AC ?\n]))))

(setq-default whitespace-style
              (quote (face tabs trailing space-before-tab newline
                           indentation space-after-tab tab-mark newline-mark
                           empty)))

(setq pixel-scroll-precision-mode t)

(provide 'init-ui)
;;; init-ui.el ends here
