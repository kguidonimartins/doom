;;; -*- lexical-binding: t; -*-

;;; dired

;; Dired is a built-in file manager for Emacs that does some pretty amazing things!  Here are some key bindings you should try out:
;; *** Key Bindings
;; **** Navigation
;; *Emacs* / *Evil*
;; - =n= / =j= - next line
;; - =p= / =k= - previous line
;; - =j= / =J= - jump to file in buffer
;; - =RET= - select file or directory
;; - =^= - go to parent directory
;; - =S-RET= / =g O= - Open file in "other" window
;; - =M-RET= - Show file in other window without focusing (previewing files)
;; - =g o= (=dired-view-file=) - Open file but in a "preview" mode, close with =q=
;; - =g= / =g r= Refresh the buffer with =revert-buffer= after changing configuration (and after filesystem changes!)

;; **** Marking files
;; - =m= - Marks a file
;; - =u= - Unmarks a file
;; - =U= - Unmarks all files in buffer
;; - =* t= / =t= - Inverts marked files in buffer
;; - =% m= - Mark files in buffer using regular expression
;; - =*= - Lots of other auto-marking functions
;; - =k= / =K= - "Kill" marked items (refresh buffer with =g= / =g r= to get them back)
;; - Many operations can be done on a single file if there are no active marks!

;; **** Copying and Renaming files
;; - =C= - Copy marked files (or if no files are marked, the current file)
;; - Copying single and multiple files
;; - =U= - Unmark all files in buffer
;; - =R= - Rename marked files, renaming multiple is a move!
;; - =% R= - Rename based on regular expression: =^test= , =old-\&=

;; *Power command*: =C-x C-q= (=dired-toggle-read-only=) - Makes all file names in the buffer editable directly to rename them!  Press =Z Z= to confirm renaming or =Z Q= to abort.

;; **** Deleting files
;; - =D= - Delete marked file
;; - =d= - Mark file for deletion
;; - =x= - Execute deletion for marks
;; - =delete-by-moving-to-trash= - Move to trash instead of deleting permanently

;; **** Creating and extracting archives
;; - =Z= - Compress or uncompress a file or folder to (=.tar.gz=)
;; - =c= - Compress selection to a specific file
;; - =dired-compress-files-alist= - Bind compression commands to file extension

;; **** Other common operations
;; - =T= - Touch (change timestamp)
;; - =M= - Change file mode
;; - =O= - Change file owner
;; - =G= - Change file group
;; - =S= - Create a symbolic link to this file
;; - =L= - Load an Emacs Lisp file into Emacs

;; Thumbnails images
;; https://www.youtube.com/watch?v=NrY3t3W0_cM
;; Mark files with =m= and open thumbnails with =C-t d=

(use-package! dired
  :commands (dired dired-jump)
  :custom ((dired-listing-switches "-AhFlv --group-directories-first"))
  :config

  (defun funk/dired-copy-filename-at-point ()
    "When in a dired buffer, copy only the filename to the clipboard.
For example: a path like `/home/user/.config/emacs/lisp/functions/funk.el'
will be copied as `funk.el' only."
    (interactive)
    (dired-copy-filename-as-kill))

  (defun funk/dired-copy-full-path-at-point ()
    "When in a dired buffer, copy only the full path to the clipboard.
For example: a path like `/home/user/.config/emacs/lisp/functions/funk.el'
will be copied exactly as it is."
    (interactive)
    (dired-copy-filename-as-kill 0))

  (defun funk/dired-copy-tilda-path-at-point ()
    "When in a dired buffer, copy only the path with ~/ to the clipboard.
For example: a path like `/home/user/.emacs.d/lisp/functions/funk.el'
will be copied as `~/.emacs.d/lisp/functions/funk.el'."
    (interactive)
    (dired-copy-filename-as-kill 0)
    (setq filename-from-kill-ring (current-kill 0))
    (when (string-match (getenv "HOME") filename-from-kill-ring)
      (setq filename-home-tilda (replace-match "~" t t filename-from-kill-ring))
      (let ((x-select-enable-clipboard t)) (kill-new filename-home-tilda))
      (message filename-home-tilda)))

  (defun funk/dired-copy-from-project-path-at-point ()
    "When in a dired buffer, copy only the path from the project root to the clipboard.
For example: a path like `/home/user/.config/emacs/lisp/functions/funk.el'
will be copied as `lisp/functions/funk.el'."
    (interactive)
    (dired-copy-filename-as-kill 0)
    (setq filename-from-kill-ring (current-kill 0))
    (when (string-match (projectile-project-root) filename-from-kill-ring)
      (setq filename-without-project-root (replace-match "" t t filename-from-kill-ring))
      (let ((x-select-enable-clipboard t)) (kill-new filename-without-project-root))
      (message filename-without-project-root)))

  (defun funk/open-with (arg)
    "Open visited file in default external program.
When in dired mode, open file under the cursor.
With a prefix ARG always prompt for command to use.
FROM: https://github.com/bbatsov/crux/blob/20c07848049716a0e1aa2560e23b5f4149f2a74f/crux.el#L161"
    (interactive "P")
    (let* ((current-file-name
            (if (or (eq major-mode 'dired-mode) (eq major-mode 'dirvish-mode))
                (dired-get-file-for-visit)
              buffer-file-name))
           (open (pcase system-type
                   (`darwin "open")
                   ((or `gnu `gnu/linux `gnu/kfreebsd) "xdg-open")))
           (program (if (or arg (not open))
                        (read-shell-command "Open current file with: ")
                      open)))
      (call-process program nil 0 nil current-file-name)))

  (defun funk/git-add-files(files)
    "Run git add with the input file.
@see: https://gist.github.com/justinhj/5945047"
    (interactive)
    (shell-command (format "git add -f %s" files)))

  (defun funk/dired-git-add-marked-files()
    "For each marked file in a dired buffer add it to the index.
@see: https://gist.github.com/justinhj/5945047"
    (interactive)
    (if (eq major-mode 'dired-mode)
        (let ((filenames (dired-get-marked-files))
              (files ""))
          (dolist (fn filenames)
            (setq fn (shell-quote-argument fn))
            (setq files (concat files " " fn)))
          (funk/git-add-files files))
      (error (format "Not a Dired buffer \(%s\)" major-mode))))

  (defun funk/dired-drag-and-drop ()
    "Open dragon with the marked files on dired.
Depends on: https://github.com/mwh/dragon"
    (interactive)
    ;; xdragon rename is a nix thing, pretty sure.
    (make-process
     :name "dragon"
     :command (append '("dragon") (dired-get-marked-files))
     :noquery t))

  ;; Delete my files by moving them to the trash
  ;; https://github.com/nivekuil/rip#emacs
  (setq delete-by-moving-to-trash t)
  (defun system-move-file-to-trash (filename)
    (shell-command (concat (executable-find "rip") " " filename)))
  (evil-collection-define-key 'normal 'dired-mode-map
    "h"  'dired-single-up-directory
    "l"  'dired-single-buffer
    "yf" 'funk/dired-copy-filename-at-point
    "yh" 'funk/dired-copy-full-path-at-point
    "yy" 'funk/dired-copy-tilda-path-at-point
    "yp" 'funk/dired-copy-from-project-path-at-point
    "Y"  'funk/dired-copy-tilda-path-at-point
    (kbd "C-<return>") 'funk/open-with
    (kbd "C-SPC") 'funk/hydra-top-menu/body
    ")" 'dired-git-info-mode
    )
  (which-key-add-key-based-replacements
    "yf" "copy basename path"
    "yh" "copy hard full path"
    "yy" "copy tilda full path"
    "yp" "copy project path"
    "Y" "copy basename"
    )
  (general-define-key
   :keymaps '(dired-mode-map)
   :states '(normal)
   ;; With big powers come big responsibilities.
   ;; These delete commands sometimes drive me crazy.
   ;; Thanks to `rip' (github.com/nivekuil/rip) I haven't lost entire projects.
   ;; So, just open a terminal and run the command `rip' (aliased to `rm').
   ;; In terminal, if you prefer, you can use the select and delete commands
   ;; inside `lf', which are also aliased to `rip'.
   "x" nil
   "d" nil
   "D" nil
   "SPC" nil
   )
  ;; (setq dired-dwim-target t). Then, go to dired, split your window,
  ;; split-window-vertically & go to another dired directory. When you will
  ;; press C to copy, the other dir in the split pane will be default
  ;; destination.
  (setq dired-dwim-target t)
  ;; stop opening so many dired buffers
  (setq dired-kill-when-opening-new-dired-buffer t)
  :init
  (add-hook 'dired-mode-hook 'dired-hide-details-mode)
  (add-hook 'dired-mode-hook 'auto-revert-mode)
  (add-hook 'dired-mode-hook 'hl-line-mode)
  (add-hook 'dired-load-hook
            (function (lambda () (load "dired-x")))))

;;; all-the-icons-dired
(use-package! all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

;;; dired-single
(use-package! dired-single
  ;; :defer 2
  :after dired)

;;; dired-git-info
(use-package! dired-git-info
  :disabled
  ;; :defer 2
  :after dired
  :config
  (setq dgi-auto-hide-details-p nil)
  (add-hook 'dired-after-readin-hook 'dired-git-info-auto-enable))

;;; diredfl
(use-package! diredfl
  ;; :defer 2
  :init (diredfl-global-mode))


;;; dired-subtree
(use-package! dired-subtree
  ;; :defer 2
  :config
  (custom-set-faces
   '(dired-subtree-depth-1-face ((t (:background unspecified))))
   '(dired-subtree-depth-2-face ((t (:background unspecified))))
   '(dired-subtree-depth-3-face ((t (:background unspecified))))
   '(dired-subtree-depth-4-face ((t (:background unspecified))))
   '(dired-subtree-depth-5-face ((t (:background unspecified))))
   '(dired-subtree-depth-6-face ((t (:background unspecified))))
   )
  (advice-add 'dired-subtree-toggle :after (lambda ()
                                             (interactive)
                                             (when all-the-icons-dired-mode
                                               (revert-buffer)))))
;;; dired-filter
(use-package! dired-filter
  ;; :defer 2
  )


;;; dired-hide-dotfiles
(use-package! dired-hide-dotfiles
  ;; :defer 2
  ;; :hook (dired-mode . dired-hide-dotfiles-mode)
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "H" 'dired-hide-dotfiles-mode))

;;; dired-narrow
(use-package! dired-narrow
  ;; :defer 2
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "?" 'dired-narrow))

;;; dired-open
;; (use-package dired-open
;;   :commands (dired dired-jump)
;;   :config
;;   (setq dired-open-extensions '(("png" . "feh")
;;                                 ("mkv" . "mpv"))))

;; (use-package dired-rainbow
;;   :after dired
;;   :config
;;   (dired-rainbow-define html "#eb5286" ("css" "less" "sass" "scss" "htm" "html" "jhtm" "mht" "eml" "mustache" "xhtml"))
;;   (dired-rainbow-define xml "#f2d024" ("xml" "xsd" "xsl" "xslt" "wsdl" "bib" "json" "msg" "pgn" "rss" "yaml" "yml" "rdata"))
;;   (dired-rainbow-define document "#9561e2" ("docm" "doc" "docx" "odb" "odt" "pdb" "pdf" "ps" "rtf" "djvu" "epub" "odp" "ppt" "pptx"))
;;   (dired-rainbow-define markdown "#ffed4a" ("org" "etx" "info" "markdown" "md" "mkd" "nfo" "pod" "rst" "tex" "textfile" "txt"))
;;   (dired-rainbow-define database "#6574cd" ("xlsx" "xls" "csv" "accdb" "db" "mdb" "sqlite" "nc"))
;;   (dired-rainbow-define media "#de751f" ("mp3" "mp4" "MP3" "MP4" "avi" "mpeg" "mpg" "flv" "ogg" "mov" "mid" "midi" "wav" "aiff" "flac"))
;;   (dired-rainbow-define image "#f66d9b" ("tiff" "tif" "cdr" "gif" "ico" "jpeg" "jpg" "png" "psd" "eps" "svg"))
;;   (dired-rainbow-define log "#c17d11" ("log"))
;;   (dired-rainbow-define shell "#f6993f" ("awk" "bash" "bat" "sed" "sh" "zsh" "vim"))
;;   (dired-rainbow-define interpreted "#38c172" ("py" "ipynb" "rb" "pl" "t" "msql" "mysql" "pgsql" "sql" "r" "clj" "cljs" "scala" "js"))
;;   (dired-rainbow-define compiled "#4dc0b5" ("asm" "cl" "lisp" "el" "c" "h" "c++" "h++" "hpp" "hxx" "m" "cc" "cs" "cp" "cpp" "go" "f" "for" "ftn" "f90" "f95" "f03" "f08" "s" "rs" "hi" "hs" "pyc" ".java"))
;;   (dired-rainbow-define executable "#8cc4ff" ("exe" "msi"))
;;   (dired-rainbow-define compressed "#51d88a" ("7z" "zip" "bz2" "tgz" "txz" "gz" "xz" "z" "Z" "jar" "war" "ear" "rar" "sar" "xpi" "apk" "xz" "tar"))
;;   (dired-rainbow-define packaged "#faad63" ("deb" "rpm" "apk" "jad" "jar" "cab" "pak" "pk3" "vdf" "vpk" "bsp"))
;;   (dired-rainbow-define encrypted "#ffed4a" ("gpg" "pgp" "asc" "bfe" "enc" "signature" "sig" "p12" "pem"))
;;   (dired-rainbow-define fonts "#6cb2eb" ("afm" "fon" "fnt" "pfb" "pfm" "ttf" "otf"))
;;   (dired-rainbow-define partition "#e3342f" ("dmg" "iso" "bin" "nrg" "qcow" "toast" "vcd" "vmdk" "bak"))
;;   (dired-rainbow-define vc "#0074d9" ("git" "gitignore" "gitattributes" "gitmodules"))
;;   (dired-rainbow-define-chmod executable-unix "#38c172" "-.*x.*")
;;   (dired-rainbow-define-chmod directory "#6cb2eb" "d.*"))
;; (use-package dired-collapse :hook (dired-mode . dired-collapse-mode))


;;; dired-collapse
(use-package! dired-collapse
  :bind
  (:map dired-mode-map
        ("M-c" . dired-collapse-mode))
  :config
  (dired-collapse-mode +1)
  (add-hook 'dired-mode-hook 'dired-collapse-mode))

;;; recentf
(use-package! recentf
  ;; :defer 2
  :config
  (recentf-mode)
  (setq  recentf-max-saved-items 1000
         recentf-exclude '("^/var/"
                           "COMMIT_EDITMSG\\'"
                           ".*-autoloads\\.el\\'"
                           "^/elpa/")))

;;; vlf: open very large files
(use-package! vlf
  ;; :defer 2
  :config
  (require 'vlf-setup))

;;; openwith
(use-package! openwith
  ;; :defer 2
  :init
  (openwith-mode t)
  :config
  (setq openwith-associations '(
                                ;; ("\\.pdf\\'" "zathura" (file))
                                ("\\.docx\\'" "libreoffice" (file))
                                ("\\.xls\\'" "libreoffice" (file))
                                ("\\.xlsx\\'" "libreoffice" (file))
                                ("\\.html\\'" "$BROWSER" (file))
                                ))
  (add-hook 'dired-mode-hook #'(lambda () (openwith-mode +1))))

;;; rg
(use-package! rg
  ;; :defer 2
  :config
  (defun calibre-search (query)
    (interactive)
    (rg query "*" "~/google-drive/kguidonimartins/calibre-library"))
  (setq rg-executable (executable-find "rga")))

;;; dirvish

(use-package! dirvish
  ;; :defer 2
  :config
  (general-define-key
   :keymaps '(dirvish-mode-map)
   :states '(normal)
   "h"  #'dirvish-up-directory
   "l"  #'dirvish-find-file
   "gg" #'dirvish-go-top
   "G"  #'dirvish-go-bottom)
  (defun dirvish-preview-directory-exa-dispatcher (file _dv)
    "A dispatcher function for `dirvish-preview-dispatchers'.
If FILE can be matched by this dispatcher, Dirvish uses `exa' to
generate directory preview."
    (when (file-directory-p file)
      `(shell . ("exa" "--color=always" "--long" "--header" "--git" "--classify" "--all" "--group" "--modified" "--time-style=long-iso" "--group-directories-first" ,file))))
  (defun funk/dirvish-disable-some-modes ()
    (when (derived-mode-p 'dirvish-mode)
      (progn
        (all-the-icons-dired-mode -1)
        (visual-line-mode -1)
        (openwith-mode +1)
        (toggle-truncate-lines +1))))
  (add-hook 'dirvish-activation-hook #'funk/dirvish-disable-some-modes)
  (add-hook 'dirvish-mode-hook #'funk/dirvish-disable-some-modes)
  (add-hook 'dirvish-preview-setup-hook #'funk/dirvish-disable-some-modes)
  (setq dirvish-header-style 'normal)
  (setq dirvish-body-fontsize-increment 0)
  ;; (dirvish-override-dired-mode +1)
  ;; (advice-add 'dirvish-dired :around 'dirvish)
  ;; (dirvish-minibuf-preview-mode) ; only selectrum or consult
  (global-set-key (kbd "<f7>") 'dirvish)
  (setq dirvish-attributes '(all-the-icons file-size))
  (dirvish-override-dired-mode -1)
  (dirvish-peek-mode)
  ;; (custom-set-faces
  ;;  '(ansi-color-blue
  ;;    ((t
  ;;      (
  ;;       :foreground  "#FFFFFF"
  ;;       :background  unspecified
  ;;       )
  ;;      )))
  ;;  '(ansi-color-bold
  ;;    ((t
  ;;      (
  ;;       :weight bold
  ;;       :inherit default
  ;;       )
  ;;      )))
  ;;  )
  )



;;; saveplace
;; open files in the last edited position
(use-package! saveplace
  :config
  (setq save-place-ignore-files-regexp "\\(?:COMMIT_EDITMSG\\|hg-editor-[[:alnum:]]+\\.txt\\|svn-commit\\.tmp\\|bzr_log\\.[[:alnum:]]+\\|[[:alnum:]]\\.csv\\)$")
  (save-place-mode)
  (add-hook 'save-place-find-file-hook 'recenter)
  (advice-add 'save-place-find-file-hook :after #'(lambda() (when (not (eq (buffer-name) "COMMIT_EDITMSG")) (outline-cycle)))))

;; CHECK: https://github.com/hidaqa/dired-copy-paste
;; CHECK: https://github.com/amno1/.emacs.d/blob/main/lisp/dired-extras.el

;;; open file with sudo
  (defun funk/sudo-find-file (file-name)
    "Like find file, but opens the file as root."
    (interactive "FSudo Find File: ")
    (let ((root-file-name (concat "/sudo::" (expand-file-name file-name))))
      (counsel-find-file root-file-name)))

;;; custom functions
(defun funk/create-empty-file-if-no-exists (file-path &optional arg)
  "Create a file with FILEPATH parameter."
  (interactive)
  (if (file-exists-p file-path)
      (message (concat  "File " (concat file-path " already exists")))
    (with-temp-buffer (write-file file-path))))

(defun funk/create-readme-here (&optional arg)
  (interactive)
  (funk/create-empty-file-if-no-exists "./README.txt"))

(defun funk/insert-filename-here ()
  (interactive)
  (insert (buffer-name)))

(provide 'init-file-management)
;;; init-file-management.el ends here
