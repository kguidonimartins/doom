;;; init-keychord.el --- key-chords configs -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; key-chord

(after! key-chord
  ;; FROM: https://www.reddit.com/r/emacs/comments/6p1xku/evil_moving_around_in_insert_mode/
  ;; CHECK: https://www.johndcook.com/blog/2015/02/01/rare-bigrams/
  ;; CHECK: https://www.reddit.com/r/emacs/comments/22hzx7/what_are_your_keychord_abbreviations/
  (key-chord-mode 1)

  ;;;###autoload
  (defun key-chord-define (keymap keys command)
    ;; NOTE 2021-09-07: This is a redefinition of the original `key-chord-define',
    ;; in which avoid the definition of keymaps in the inverse order. For example,
    ;; when defining `:r', the original function also defines `r:'.
    ;; In sum, this current definition just remove the last line of the
    ;; original function.
    "Define in KEYMAP, a key-chord of the two keys in KEYS starting a COMMAND.
KEYS can be a string or a vector of two elements. Currently only
elements that corresponds to ascii codes in the range 32 to 126
can be used.
COMMAND can be an interactive function, a string, or nil.
If COMMAND is nil, the key-chord is removed."
    (if (/= 2 (length keys))
        (error "Key-chord keys must have two elements"))
    ;; Exotic chars in a string are >255 but define-key wants 128..255
    ;; for those.
    (let ((key1 (logand 255 (aref keys 0)))
          (key2 (logand 255 (aref keys 1))))
      (if (eq key1 key2)
          (define-key keymap (vector 'key-chord key1 key2) command)
        (define-key keymap (vector 'key-chord key1 key2) command))))

  ;;;###autoload
  (defun funk/save-buffer-and-exit-insert-state()
    (interactive)
    (save-buffer)
    (evil-normal-state))

  ;;;###autoload
  (defun funk/counsel-dired-jump ()
    "Open minibuffer from the project root."
    (interactive)
    (with-eval-after-load 'projectile
      (with-eval-after-load 'counsel-projectile
        (with-eval-after-load 'counsel
          (counsel-dired-jump nil (projectile-project-root))))))

  (defun funk/counsel-projectile-switch-to-buffer-from-minibuffer ()
    "When in ivy-switch-buffer minibuffer, this command narrow only candidates
 of the current project."
    (interactive)
    (ivy-quit-and-run (counsel-projectile-switch-to-buffer)))

  (setq key-chord-one-key-delay 0.2)
  (setq key-chord-two-keys-delay 0.2)

  ;; confirm/accept/eval with `jj'
  (key-chord-define minibuffer-mode-map    "jj" 'exit-minibuffer)
  (with-eval-after-load 'helm
    (key-chord-define helm-map             "jj" 'helm-confirm-and-exit-minibuffer))
  (with-eval-after-load 'ivy
    (key-chord-define ivy-minibuffer-map   "jj" 'ivy-done))
  ;; (key-chord-define minibuffer-mode-map    "jj" 'exit-minibuffer)
  (key-chord-define evil-ex-completion-map "jj" 'exit-minibuffer)

  ;; abort/quit/cancel/exit with `kj'
  (with-eval-after-load 'ivy
    (key-chord-define ivy-minibuffer-map   "kj" 'minibuffer-keyboard-quit))
  (with-eval-after-load 'helpful
    (key-chord-define helpful-mode-map     "kj" 'quit-window))
  ;; (key-chord-define minibuffer-mode-map    "kj" 'minibuffer-keyboard-quit)
  (key-chord-define help-mode-map          "kj" 'quit-window)
  (key-chord-define dired-mode-map         "kj" 'quit-window)
  (key-chord-define evil-ex-completion-map "kj" 'abort-recursive-edit)
  (key-chord-define evil-visual-state-map  "kj" 'evil-exit-visual-state)
  (key-chord-define evil-insert-state-map  "kj" 'funk/save-buffer-and-exit-insert-state)

  ;; most used commands: give some rest to the left pinkie
  (with-eval-after-load 'ivy
    (key-chord-define-global               ":l" 'switch-to-buffer)
    (key-chord-define ivy-switch-buffer-map ":l" 'funk/counsel-projectile-switch-to-buffer-from-minibuffer)
    (key-chord-define-global               ":k" 'counsel-projectile-switch-to-buffer)
    (key-chord-define-global               ":s" 'counsel-projectile-switch-project))
  (with-eval-after-load 'dired
    (key-chord-define-global               ":d" 'funk/counsel-dired-jump))
  (with-eval-after-load 'rg
    (key-chord-define-global               ":r" 'rg))
  (key-chord-define-global                 ":j" 'delete-window)
  (key-chord-define-global                 ":o" 'other-window)
  (key-chord-define-global                 ":f" 'find-file)
  (key-chord-define-global                 ":g" 'magit-status)
  (key-chord-define evil-normal-state-map  "::" 'execute-extended-command)
  (key-chord-define evil-insert-state-map  "jj" 'funk/save-buffer-and-exit-insert-state)
  (key-chord-define evil-insert-state-map  "kk" 'funk/save-buffer-and-exit-insert-state)
  (key-chord-define evil-normal-state-map  "kj" 'funk/save-buffer-and-exit-insert-state)
  ;; (with-eval-after-load 'org
  ;;   (key-chord-define-global               ":a" 'org-agenda-list))

  )

(provide 'init-keychord)
;;; init-keychord.el ends here
