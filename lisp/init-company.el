;;; init-company.el --- Configurations for company-mode  -*- lexical-binding: t; -*-

;; Copyright (c) 2020-2021 Karlo Guidoni <kguidonimartins@gmail.com>

;; Author: Karlo Guidoni <kguidonimartins@gmail.com>
;; URL: https://gitlab/kguidonimartins/emacs
;; Package-Requires: ((emacs "28.1"))

;; This file is NOT part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Configurations for company-mode.

;;; Code:

;;; company
(use-package company
  :config
  (global-company-mode +1)

  (setq company-auto-complete nil)
  (setq company-clang-insert-arguments nil)
  (setq company-dabbrev-downcase nil)
  (setq company-dabbrev-ignore-case nil)
  (setq company-idle-delay 0.01)
  (setq company-echo-delay (if (display-graphic-p) nil 0))
  (setq company-minimum-prefix-length 2)
  (setq company-require-match nil)
  (setq company-selection-wrap-around t)
  (setq company-show-numbers t)
  (setq company-tooltip-align-annotations t)
  (setq company-tooltip-limit 10)
  (setq company-dabbrev-other-buffers t)
  (setq company-files-exclusions '(".git/" ".DS_Store"))
  ;; (setq company-format-margin-function #'company-detect-icons-margin)
  (setq evil-collection-company-use-tng t)

  (setq company-backends '((company-capf :with company-yasnippet)
                           (company-dabbrev-code company-keywords company-files)
                           company-dabbrev
                           hippie-expand
                           ))

  (global-set-key (kbd "C-c y") 'company-yasnippet)
  (global-set-key (kbd "C-c C-/") #'company-other-backend)

  (defun funk/set-company-tng-mode ()
    (company-tng-mode +1))
  (add-hook 'company-mode-hook 'funk/set-company-tng-mode)
  (define-key company-active-map (kbd "C-k") 'company-select-previous)
  (define-key company-active-map (kbd "<ESC>") 'company-abort)
  (define-key company-active-map (kbd "C-j") 'company-select-next)
  (define-key company-active-map (kbd "C-l") 'company-complete-selection)
  ;; (define-key company-active-map (kbd "SPC") 'company-complete-selection)
  (define-key company-active-map (kbd "TAB") 'company-select-next)
  (define-key company-active-map (kbd "<backtab>") 'company-select-previous)
  (define-key company-active-map (kbd "RET") 'company-complete-selection)
  (define-key company-active-map (kbd ">") 'company-search-candidates))

;;; company-prescient
(use-package company-prescient
  :after (company prescient)
  :config
  (company-prescient-mode))

;;; company-quickhelp
(use-package company-quickhelp
  ;; :defer 5
  :init
  (company-quickhelp-mode +1)
  (setq company-quickhelp-max-lines 30)
  :config
  (eval-after-load 'company
    '(define-key company-active-map (kbd "C-c h") #'company-quickhelp-manual-begin)))

;;; company-box
;; (use-package company-box
;;   :hook (company-mode . company-box-mode))

;;; company-suggest (disabled)
(use-package company-suggest
  ;; :defer 5
  :disabled
  :config
  (add-to-list 'company-backends 'company-suggest-google)
  (setq company-suggest-complete-sentence t))

;;; company-anaconda
(use-package company-anaconda
  ;; :defer 5
  :after (company python jupyter))
;; (lambda ()
;;   (set (make-local-variable 'company-backends)
;;        '((elpy-company-backend :with company-tabnine company-jedi)))))

;;; company-bibtex

(use-package company-bibtex
  :config
  (add-to-list 'company-backends 'company-bibtex)
  (setq company-bibtex-bibliography
        '(
          "~/google-drive/kguidonimartins/UFG/tese-karlo/fire-fd-data/bibliography/bibliography-paper.bib"
          "~/google-drive/kguidonimartins/UFG/tese-karlo/fire-fd-data/bibliography/manual_entry.bib"
          "~/google-drive/kguidonimartins/UFG/tese-karlo/fire-fd-data/bibliography/bibliography-used.bib"
          "~/google-drive/kguidonimartins/UFG/tese-karlo/intro-concl/bibliography/bigbib.bib"
          "~/google-drive/kguidonimartins/UFG/tese-karlo/intro-concl/bibliography/manual_entry.bib"
          )
        )
  )

;;; Setup company for specific major modes
;;;; company for python
(defun funk/python-mode-company ()
  "Setup `company-mode' for `python-mode'"
  (company-mode +1)
  (setq company-idle-delay 0.01)
  (setq company-minimum-prefix-length 1)
  (setq company-backends '((company-anaconda
                            :with
                            company-capf
                            ))))
(add-hook 'python-mode-hook #'funk/python-mode-company)

;;;; company for emacs-lisp
(defun funk/emacs-lisp-mode-company ()
  "Setup `company-mode' for `emacs-lisp-mode'"
  (company-mode +1)
  (setq company-idle-delay 0.01)
  (setq company-minimum-prefix-length 1)
  (setq-local company-backends '((
                                  company-yasnippet
                                  :with
                                  company-capf
                                  company-dabbrev-code
                                  ))))
(add-hook 'emacs-lisp-mode-hook #'funk/emacs-lisp-mode-company)

;;;; company for org
(defun funk/org-mode-company ()
  "Setup `company-mode' for `org-mode'"
  (setq-local company-minimum-prefix-length 2)
  (company-mode +1)
  (setq-local company-backends '((
                                  company-yasnippet
                                  :with
                                  company-files
                                  company-capf
                                  ))))

(defun funk/org--complete-keywords ()
  "Allow company to complete org keywords after ^#+"
  (add-hook 'completion-at-point-functions
            'pcomplete-completions-at-point nil t))

(add-hook 'org-mode-hook #'company-mode)
(add-hook 'org-mode-hook #'funk/org--complete-keywords)
(add-hook 'org-mode-hook #'funk/org-mode-company)

;;;; company for shell
(defun funk/sh-mode-company ()
  "Setup `company-mode' for `sh-mode'"
  (company-mode +1)
  (setq-local company-backends '((company-files
                                  company-keywords
                                  company-capf
                                  company-dabbrev-code
                                  company-etags
                                  company-dabbrev
                                  company-yasnippet
                                  hippie-expand))))
(add-hook 'sh-mode-hook #'funk/sh-mode-company)

;;;; company for markdown
(defun funk/markdown-mode-company ()
  "Setup `company-mode' for `markdown-mode'"
  (company-mode +1)
  (setq-local company-backends '((company-dabbrev
                                  ;; company-suggest-google
                                  :with
                                  company-bibtex
                                  company-files
                                  company-keywords
                                  company-capf
                                  company-dabbrev-code
                                  company-etags
                                  company-yasnippet
                                  hippie-expand))))
(add-hook 'markdown-mode-hook #'funk/markdown-mode-company)

;;;; company for ess
(defun funk/ess-mode-company ()
  "Setup `company-mode' for `ess-mode'"
  (if (not (in-slow-ssh))
      (progn
        (company-mode +1)
        (setq-local company-minimum-prefix-length 2)
        (setq-local company-backends '((company-R-args
                                        company-R-objects
                                        company-dabbrev-code
                                        :with
                                        company-R-library
                                        company-yasnippet
                                        company-keywords
                                        complete-path-at-point+
                                        hippie-expand
                                        ;; hippie-expand
                                        ;; company-files
                                        ;; company-capf
                                        ;; company-etags
                                        ;; company-dabbrev
                                        ))))))
(add-hook 'ess-mode-hook #'funk/ess-mode-company)

;;;; company for csv
;; (defun funk/csv-mode-company ()
;;   "Setup `company-mode' for `emacs-lisp-mode'"
;;   (company-mode +1)
;;   (setq company-idle-delay 0.01)
;;   (setq company-minimum-prefix-length 1)
;;   (setq-local company-backends '((company-keywords
;;                                   hippie-expand
;;                                   company-files
;;                                   ))))
;; (add-hook 'csv-mode-hook #'funk/csv-mode-company)

;;; hippie
(setq hippie-expand-try-functions-list '(try-expand-dabbrev
                                         try-expand-dabbrev-all-buffers
                                         try-expand-dabbrev-from-kill
                                         try-complete-file-name-partially
                                         try-complete-file-name
                                         try-expand-all-abbrevs
                                         try-expand-list
                                         try-expand-line
                                         try-complete-lisp-symbol-partially
                                         try-complete-lisp-symbol))

(provide 'init-company)
;;; init-company.el ends here
