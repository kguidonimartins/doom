;;; init-comment.el --- comment functions -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; Excerpt from prot-comment.el: Thanks Prot!
;; helper code from prot-common.el, which is `require'd by
;; prot-comment.el
(defun funk/comment-header()
  (interactive)
  (let ((comment-string (read-string "Comment: ")))
    (insert
     (concat comment-start
             (make-string
              (comment-add nil)
              (string-to-char comment-start))
             comment-padding
             (upcase comment-string)
             (concat " ")
             comment-end)))
  (fill-to-end)
  )

(defvar funk/common--line-regexp-alist
  '((empty . "[\s\t]*$")
    (indent . "^[\s\t]+")
    (non-empty . "^.+$")
    (list . "^\\([\s\t#*+]+\\|[0-9]+[^\s]?[).]+\\)")
    (heading . "^[=-]+"))
  "Alist of regexp types used by `funk/common-line-regexp-p'.")

(defun funk/common-line-regexp-p (type &optional n)
  "Test for TYPE on line.
TYPE is the car of a cons cell in
`funk/common--line-regexp-alist'.  It matches a regular
expression.

With optional N, search in the Nth line from point."
  (save-excursion
    (goto-char (point-at-bol))
    (and (not (bobp))
         (or (beginning-of-line n) t)
         (save-match-data
           (looking-at
            (alist-get type funk/common--line-regexp-alist))))))

;; and what follows is from funk/comment.el
(defcustom funk/comment-comment-keywords
  '("TODO" "FIXME" "IDEIA" "HACK" "REVIEW" "NOTE" "CHECK" "REFS" "DUMP" "DEPRECATED" "DONE" "TEMP")
  "List of strings with comment keywords."
  :type '(repeat string)
  :group 'funk/comment)

(defcustom funk/comment-timestamp-format-concise "%F"
  "Specifier for date in `funk/comment-timestamp-keyword'.
Refer to the doc string of `format-time-string' for the available
options."
  :type 'string
  :group 'funk/comment)

(defcustom funk/comment-timestamp-format-verbose "%F %T %z"
  "Like `funk/comment-timestamp-format-concise', but longer."
  :type 'string
  :group 'funk/comment)
;; NOTE 2021-08-28 10:17:44 -0300: this variable is saved by `psession'.
;; So, having `/' in their name broken things. Be careful.
(defvar funk-comment--keyword-hist '()
  "Input history of selected comment keywords.")

(defun funk/comment--keyword-prompt (keywords)
  "Prompt for candidate among KEYWORDS."
  (let ((def (car funk-comment--keyword-hist)))
    (completing-read
     (format "Select keyword [%s]: " def)
     keywords nil nil nil 'funk-comment--keyword-hist def)))

;;;###autoload
(defun funk/comment-timestamp-keyword (keyword &optional verbose)
  "Add timestamped comment with KEYWORD.

When called interactively, the list of possible keywords is that
of `funk/comment-comment-keywords', though it is possible to
input arbitrary text.

If point is at the beginning of the line or if line is empty (no
characters at all or just indentation), the comment is started
there in accordance with `comment-style'.  Any existing text
after the point will be pushed to a new line and will not be
turned into a comment.

If point is anywhere else on the line, the comment is indented
with `comment-indent'.

The comment is always formatted as 'DELIMITER KEYWORD DATE:',
with the date format being controlled by the variable
`funk/comment-timestamp-format-concise'.

With optional VERBOSE argument (such as a prefix argument
`\\[universal-argument]'), use an alternative date format, as
specified by `funk/comment-timestamp-format-verbose'."
  (interactive
   (list
    (funk/comment--keyword-prompt funk/comment-comment-keywords)
    current-prefix-arg))
  (let* ((date (if verbose
                   funk/comment-timestamp-format-verbose
                 funk/comment-timestamp-format-concise))
         (string (format "%s %s: " keyword (format-time-string date)))
         (beg (point)))
    (cond
     ((or (eq beg (point-at-bol))
          (funk/common-line-regexp-p 'empty))
      (let* ((maybe-newline (unless (funk/common-line-regexp-p 'empty 1) "\n")))
        ;; NOTE 2021-07-24: we use this `insert' instead of
        ;; `comment-region' because of a yet-to-be-determined bug that
        ;; traps `undo' to the two states between the insertion of the
        ;; string and its transformation into a comment.
        (insert
         (concat comment-start
                 ;; NOTE 2021-07-24: See function `comment-add' for
                 ;; why we need this.
                 (make-string
                  (comment-add nil)
                  (string-to-char comment-start))
                 comment-padding
                 string
                 comment-end))
        (indent-region beg (point))
        (when maybe-newline
          (save-excursion (insert maybe-newline)))))
     (t
      (comment-indent t)
      (insert (concat " " string))))))


;;; Insert date at point
(defcustom funk/simple-date-specifier "%F"
  "Date specifier for `format-time-string'.
Used by `funk/simple-inset-date'."
  :type 'string
  :group 'funk/simple)

(defcustom funk/simple-time-specifier "%R %z"
  "Time specifier for `format-time-string'.
Used by `funk/simple-inset-date'."
  :type 'string
  :group 'funk/simple)

;;;###autoload
(defun funk/simple-insert-date (&optional arg)
  "Insert the current date as `funk/simple-date-specifier'.

With optional prefix ARG (\\[universal-argument]) also append the
current time understood as `funk/simple-time-specifier'.

When region is active, delete the highlighted text and replace it
with the specified date."
  (interactive "P")
  (let* ((date funk/simple-date-specifier)
         (time funk/simple-time-specifier)
         (format (if arg (format "%s %s" date time) date)))
    (when (use-region-p)
      (delete-region (region-beginning) (region-end)))
    (insert (format-time-string format))))

;;; insert a custom header
(defun fill-to-end ()
  ;; ——— Minor mode ——————————————————————————————————————————————————————————————
  ;; --- Minor mode --------------------------------------------------------------
  (interactive)
  (save-excursion
    (end-of-line)
    (while (< (current-column) 80)
      (insert-char ?—))))

;;; virtual-comment

;; (use-package virtual-comment
;;   :config
;;   (add-hook 'find-file-hook 'virtual-comment-mode))

(provide 'init-comment)
;;; init-comment.el ends here
