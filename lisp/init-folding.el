;;; init-folding.el --- folding configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Check the list and clean up!
;; - folding
;; - hideshow
;; - vimish-fold
;; - evil-vimish-fold
;; - origami
;; - https://wikemacs.org/wiki/Folding
;; - https://wikemacs.org/wiki/Outline
;; - https://www.reddit.com/r/emacs/comments/e2u5n9/code_folding_with_outlineminormode/
;; - https://github.com/gregsexton/origami.el
;; - https://github.com/jwiegley/dot-emacs/blob/master/init.el#L3172
;; - https://www.emacswiki.org/emacs/ESSOutlineMode
;; - https://www.modernemacs.com/post/outline-ivy/
;; - https://www.emacswiki.org/emacs/OutlineMode
;; - https://emacs.stackexchange.com/a/8065/31478
;; - https://www.emacswiki.org/emacs/HideShow
;; - https://stackoverflow.com/questions/2399612/why-is-there-no-code-folding-in-emacs
;; - https://gitlab.com/protesilaos/dotfiles/-/blob/master/emacs/.emacs.d/prot-emacs.el#L2912
;; - https://gitlab.com/protesilaos/dotfiles/-/blob/master/emacs/.emacs.d/prot-lisp/prot-outline.el
;; - https://github.com/sonofjon/config-emacs/blob/bdda67e06879ecfbce4ccd38d7c36bd86eab1f89/init.el#L977-L993
;; - https://github.com/sonofjon/config-emacs/blob/bdda67e06879ecfbce4ccd38d7c36bd86eab1f89/init.el#L1047
;; - https://github.com/sonofjon/config-emacs/blob/bdda67e06879ecfbce4ccd38d7c36bd86eab1f89/init.el#L1204-L1238
;; - https://github.com/sonofjon/config-emacs/blob/bdda67e06879ecfbce4ccd38d7c36bd86eab1f89/init.el#L1330-L1423
;; - https://github.com/peleusj/emacs.d/blob/e233da063196b73578fab4d8e8c9e533d4a79532/lisp/init-outline.el

;;; outline-minor-faces

;; (use-package outline-minor-faces
;;   :after outline
;;   :config (add-hook 'outline-minor-mode-hook
;;                     'outline-minor-faces-add-font-lock-keywords))

;; (use-package backline
;;   :after outline
;;   :config (advice-add 'outline-flag-region :after 'backline-update))


;;; origami (disabled)
;; (use-package origami)

;;; folding (disabled)
;; (use-package folding
;;   :config
;;   (folding-mode-add-find-file-hook)
;;   (load "folding" 'nomessage 'noerror)
;;   (folding-mode-add-find-file-hook)
;;   (add-hook 'LaTeX-mode-hook 'folding-mode)
;;   (add-hook 'ess-mode-hook 'folding-mode)
;;   (folding-add-to-marks-list 'ess-mode "#{{{ " "#}}}" " ")
;;   (folding-add-to-marks-list 'ess-mode "--{{{ " "--}}}" " "))
;;

;;; hideshow
(use-package! hideshow
  :config
  (dolist (hook (list
                 'c-mode-hook
                 'java-mode-hook
                 'perl-mode-hook
                 'php-mode-hook
                 'emacs-lisp-mode-hook
                 'lisp-mode-hook
                 'markdown-mode-hook
                 'ess-mode-hook
                 'python-mode-hook
                 ))
    (add-hook hook 'hs-minor-mode)))
;;
;; (defun toggle-fold ()
;;   (interactive)
;;   (save-excursion
;;     (end-of-line)
;;     (hs-toggle-hiding)))
;;
;; (defvar hs-special-modes-alist
;;   (mapcar 'purecopy
;;           '((c-mode "{" "}" "/[*/]" nil nil)
;;             (c++-mode "{" "}" "/[*/]" nil nil)
;;             (ess-mode "{" "}" "/[*/]" nil nil)
;;             (markdown-mode "```{" "```" "/[*/]" nil nil)
;;             (bibtex-mode ("@\\S(*\\(\\s(\\)" 1))
;;             (java-mode "{" "}" "/[*/]" nil nil)
;;             (js-mode "{" "}" "/[*/]" nil))))

;;; vimish-fold (disabled)
;; (use-package vimish-fold
;;   ;; evil-vimish-fold provides vim-like code folding for a large variety of
;;   ;; code types.
;;   ;; Quick usage tips:
;;   ;; `zf' create a fold
;;   ;; `zd' delete a fold
;;   ;; `za' toggle
;;   ;; `zo' open
;;   ;; `zc' close
;;   ;; `zj' navigate down a fold
;;   ;; `zk' navigate up a fold
;;   :after evil)

;;; evil-vimish-fold (disabled)
;; (use-package evil-vimish-fold
;;   :after vimish-fold
;;   :init
;;   (setq evil-vimish-fold-target-modes '(prog-mode conf-mode text-mode ess-mode))
;;   :config
;;   (global-evil-vimish-fold-mode))

;;; bicycle
(use-package! bicycle
  :after outline)

;;; outline-(minor)-mode
(setq outline-minor-mode-highlight 'override) ; emacs28
(setq outline-minor-mode-cycle t)

(defun funk/toggle-outline-minor-mode ()
  "Toggle the `outline-minor-mode' for the current major-mode."
  (interactive)
  (if (null outline-minor-mode)
      (progn
        (outline-minor-mode 1)
        (outline-hide-body)
        (message "Enabled `outline-minor-mode'"))
    (outline-minor-mode -1)
    (message "Disabled `outline-minor-mode'")))

(defun funk/try-expand-outline ()
  (interactive)
  (progn
    (when (outline-on-heading-p)
      (outline-show-entry))))

;;; setup ellipsis!
;; (set-display-table-slot
;;  standard-display-table
;;  'selective-display
;;  (let ((face-offset (* (face-id 'shadow) (lsh 1 22))))
;;    (vconcat (mapcar (lambda (c) (+ face-offset c)) " ▼ "))))

;;; setup navigation

(defun funk/outline-next-heading ()
  (interactive)
  (outline-next-visible-heading +1)
  (call-interactively 'evil-scroll-line-to-center))

(defun funk/outline-next-heading-and-top ()
  (interactive)
  (outline-next-visible-heading +1)
  (call-interactively 'evil-scroll-line-to-top))

(defun funk/outline-previous-heading ()
  (interactive)
  (outline-next-visible-heading -1)
  (call-interactively 'evil-scroll-line-to-center))

(defun funk/outline-previous-heading-and-top ()
  (interactive)
  (outline-next-visible-heading -1)
  (call-interactively 'evil-scroll-line-to-top))

(defun funk/set-outline-mode-keys()
  (interactive)
  (general-define-key
   :keymaps '(outline-mode-map)
   :states '(normal)
   "C-j" nil
   "C-k" nil
   "C-M-j" #'outline-move-subtree-down
   "C-M-k" #'outline-move-subtree-up
   "]]"    #'funk/outline-next-heading
   "[["    #'funk/outline-previous-heading
   "SPC ]" #'funk/outline-next-heading-and-top
   "SPC [" #'funk/outline-previous-heading-and-top)
  (general-define-key
   :keymaps '(outline-minor-mode-map)
   :states '(normal)
   "C-j" nil
   "C-k" nil
   "C-M-j" #'outline-move-subtree-down
   "C-M-k" #'outline-move-subtree-up
   "]]"    #'funk/outline-next-heading
   "[["    #'funk/outline-previous-heading
   "SPC ]" #'funk/outline-next-heading-and-top
   "SPC [" #'funk/outline-previous-heading-and-top))

(add-hook 'outline-mode-hook 'funk/set-outline-mode-keys)
(add-hook 'outline-minor-mode-hook 'funk/set-outline-mode-keys)


;;; outiline for prog-mode
;; (add-hook 'prog-mode-hook
;;           #'(lambda ()
;;              (outline-minor-mode)
;;              (funk/set-outline-mode-keys)
;;              (outline-hide-body)
;;              ))

;;; outline for ess-mode
(add-hook 'ess-mode-hook
          #'(lambda ()
              (outline-minor-mode)
              (setq-local outline-regexp "\\(^#\\{3,7\\} \\)\\|\\(^[a-zA-Z0-9_\.]+ ?<- ?function(.*{\\)")
              (setq-local outline-regexp "\\(^#\\{3,7\\} \\)")
              (defun outline-level ()
                (cond ((looking-at "^### ") 1)
                      ((looking-at "^#### ") 2)
                      ((looking-at "^##### ") 3)
                      ((looking-at "^###### ") 4)
                      ((looking-at "^####### ") 5)
                      ((looking-at "^######## ") 6)
                      ((looking-at "^[a-zA-Z0-9_\.]+ ?<- ?function(.*{") 3)
                      (t 1000)))
              (funk/set-outline-mode-keys)
              (outline-hide-body)
              ))

(defun funk/selective-display-hide ()
  (interactive)
  (set-selective-display 2))

(defun funk/selective-display-show ()
  (interactive)
  (set-selective-display 0))

;;; outline for emacs-lisp-mode
(add-hook 'emacs-lisp-mode-hook
          #'(lambda ()
              (outline-minor-mode)
              (setq-local outline-regexp "\\(;;;+ \\)\\([^( ]\\)")
              ;; (setq outline-heading-alist
              ;;       '((";;;;; " . 1)
              ;;         (";;;; " . 2)))
              ;; (defun outline-level ()
              ;;   (cond ((looking-at "^;;; ") 1)
              ;;         ((looking-at "^;;;; ") 2)
              ;;         ((looking-at "^;;;;; ") 3)
              ;;         ((looking-at "^;;;;;; ") 4)
              ;;         ((looking-at "^;;;;;;; ") 5)
              ;;         ((looking-at "^;;;;;;;; ") 6)
              ;;         (t 1000)))
              (funk/set-outline-mode-keys)
              (outline-hide-body)
              ))

;; (add-hook 'markdown-mode-hook
;;           #'(lambda ()
;;               (outline-minor-mode)
;;               (outline-hide-body)
;;               ))

;;; force outline height
(custom-set-faces
 '(outline-1 ((t (:height 1.100 :weight bold))))
 '(outline-2 ((t (:height 1.075 :weight bold))))
 '(outline-3 ((t (:height 1.050 :weight bold))))
 '(outline-4 ((t (:height 1.025 :weight bold))))
 '(outline-5 ((t (:height 1.000 :weight bold))))
 '(outline-6 ((t (:height 1.000 :weight bold))))
 '(outline-7 ((t (:height 1.000 :weight bold))))
 '(outline-8 ((t (:height 1.000 :weight bold))))
 )

(provide 'init-folding)
;;; init-folding.el ends here
