;;; -*- lexical-binding: t; -*-

;; spell------------------------------------------------------------------------
(use-package emacs
  ;; :defer 5
  :config
  (setq ispell-program-name "hunspell")

  (defun spell-buffer-pt-BR ()
    "Function to spell check inside the buffer."
    (interactive)
    (ispell-change-dictionary "pt_BR")
    (flyspell-buffer))

  (defun spell-buffer-en ()
    "Function to spell check inside the buffer."
    (interactive)
    (ispell-change-dictionary "en_US")
    (flyspell-buffer))

  (with-eval-after-load "ispell"
    ;; Configure `LANG`, otherwise ispell.el cannot find a 'default
    ;; dictionary' even though multiple dictionaries will be configured
    ;; in next line.
    (setenv "LANG" "en_US")
    (setq ispell-program-name "hunspell")
    ;; Configure languages
    (setq ispell-dictionary "pt_BR,en_US")
    (setq ispell-local-dictionary "pt_BR,en_US")
    ;; ispell-set-spellchecker-params has to be called
    ;; before ispell-hunspell-add-multi-dic will work
    (ispell-set-spellchecker-params)
    (ispell-hunspell-add-multi-dic "pt_BR,en_US")
    ;; For saving words to the personal dictionary, don't infer it from
    ;; the locale, otherwise it would save to ~/.hunspell_de_DE.
    (setq ispell-personal-dictionary (expand-file-name "etc/spell/hunspell_personal_dictionary" user-emacs-directory))))

;; The personal dictionary file has to exist, otherwise hunspell will
;; silently not use it.
;;(unless (file-exists-p ispell-personal-dictionary)
;;(write-region "" nil ispell-personal-dictionary nil 0))


(use-package langtool
  ;; :defer 15
  ;; LanguageTool: https://github.com/mhayashi1120/Emacs-langtool
  ;; Install server with: `yay -S languagetool'
  ;; It works out of the box!
  :init (require 'langtool)
  :config
  (setq langtool-java-classpath
        "/usr/share/languagetool:/usr/share/java/languagetool/*")
  (setq langtool-default-language "en-US")
  (setq langtool-mother-tongue "pt")

  ;; Show LanguageTool report automatically by popup
  (defun langtool-autoshow-detail-popup (overlays)
    (when (require 'popup nil t)
      ;; Do not interrupt current popup
      (unless (or popup-instances
                  ;; suppress popup after type `C-g` .
                  (memq last-command '(keyboard-quit)))
        (let ((msg (langtool-details-error-message overlays)))
          (popup-tip msg)))))

  (setq langtool-autoshow-message-function 'langtool-autoshow-detail-popup))


(use-package flyspell-correct
  :after flyspell
  :bind (:map flyspell-mode-map ("C-M-i" . flyspell-correct-wrapper)))

(use-package flyspell-correct-popup
  :after flyspell-correct)

(use-package emacs
  :if (executable-find "proselint")
  :after flycheck
  ;; :defer 5
  :config
  (flycheck-define-checker proselint
    "A linter for prose."
    :command ("proselint" source-inplace)
    :error-patterns
    ((warning line-start (file-name) ":" line ":" column ": "
              (id (one-or-more (not (any " "))))
              (message) line-end))
    :modes (text-mode markdown-mode gfm-mode org-mode poly-markdown+r-mode-hook))
  (add-to-list 'flycheck-checkers 'proselint))

;; REVIEW: https://github.com/mclear-tools/dotemacs/blob/master/setup-config/setup-citation.el

;; TIL 2021-10-16: `doi-insert-bibtex' do the same as ~/.local/bin/tools/pdf/doi2bib

(use-package academic-phrases
  :disabled
  ;; :defer 10
  )

(use-package ebib
  :disabled
  ;; :defer 10
  :config
  (setq ebib-preload-bib-files '(
                                 "~/google-drive/kguidonimartins/UFG/tese-karlo/cap_02/bibliography/bibliography.bib"
                                 "~/google-drive/kguidonimartins/UFG/tese-karlo/cap_02/bibliography/manual_entry.bib"
                                 ))
  )

(use-package emacs-everywhere
  :disabled
  ;; :defer 10
  )

(use-package darkroom
  ;; :defer 10
  )

(use-package writegood-mode
  :disabled
  ;; :defer 10
  :config
  (global-set-key "\C-c\C-gw" 'writegood-mode)
  (global-set-key "\C-c\C-gg" 'writegood-grade-level)
  (global-set-key "\C-c\C-ge" 'writegood-reading-ease))

(use-package flycheck-grammarly
  ;; :defer 5
  :after flycheck
  :config
  (require 'flycheck-grammarly)
  (setq flycheck-grammarly-check-time 0.8)
  (add-to-list 'flycheck-checkers 'grammarly))

(provide 'init-spell)

;;; init-spell.el ends here
