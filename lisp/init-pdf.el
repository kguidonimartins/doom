;;; init-pdf.el --- pdf and docx configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; pdf-tools
(use-package pdf-tools
  ;; :defer 10
  ;; :pin manual ;;manually update
  :config
  ;; wrapper for save-buffer ignoring arguments
  (defun funk/save-buffer-no-args ()
    "Save buffer ignoring arguments"
    (save-buffer))
  ;; initialise
  (pdf-tools-install :no-query)
  (setq-default pdf-view-display-size 'fit-width)
  ;; automatically annotate highlights
  (setq pdf-annot-activate-created-annotations t)
  ;; use isearch instead of swiper
  (define-key pdf-view-mode-map (kbd "C-s") 'isearch-forward)
  ;; turn off cua so copy works
  (add-hook 'pdf-view-mode-hook (lambda () (cua-mode 0)))
  ;; more fine-grained zooming
  (setq pdf-view-resize-factor 1.1)
  ;; keyboard shortcuts
  (define-key pdf-view-mode-map (kbd "a") 'pdf-annot-add-highlight-markup-annotation)
  (define-key pdf-view-mode-map (kbd "t") 'pdf-annot-add-text-annotation)
  (define-key pdf-view-mode-map (kbd "D") 'pdf-annot-delete)
  (define-key pdf-view-mode-map (kbd "C-f") 'pdf-view-next-page-command)
  (define-key pdf-view-mode-map (kbd "C-b") 'pdf-view-previous-page-command)
  ;; set midnight colors
  ;; wait until map is available
  (with-eval-after-load "pdf-annot"
    (define-key pdf-annot-edit-contents-minor-mode-map (kbd "<return>") 'pdf-annot-edit-contents-commit)
    (define-key pdf-annot-edit-contents-minor-mode-map (kbd "<S-return>") 'newline)
    ;; save after adding comment
    (advice-add 'pdf-annot-edit-contents-commit :after 'funk/save-buffer-no-args))

  (require 'pdf-continuous-scroll-mode)
  (setq pdf-continuous-suppress-introduction t)
  (add-hook 'pdf-view-mode-hook 'pdf-continuous-scroll-mode)
  (add-hook 'pdf-view-mode-hook #'hide-mode-line-mode)

  (evil-make-overriding-map pdf-view-mode-map 'normal)
  (evil-set-initial-state 'pdf-view-mode 'normal)

  (evil-define-key 'normal pdf-view-mode-map
    "j" 'pdf-continuous-scroll-forward
    "k" 'pdf-continuous-scroll-backward
    ;; alternatively
    (kbd "C-o") 'pdf-history-backward
    (kbd "C-i") 'pdf-history-forward
    "m" 'pdf-view-position-to-register
    "'" 'pdf-view-jump-to-register
    "/" 'pdf-occur
    "o" 'pdf-outline
    "f" 'pdf-links-action-perform
    "i" 'pdf-view-midnight-minor-mode
    "e" 'pdf-cscroll-toggle-mode-line
    "a" 'pdf-annot-add-highlight-markup-annotation
    "y" 'pdf-annot-add
    "t" 'pdf-annot-add-text-annotation
    "D" 'pdf-annot-delete)

  ;; disable cursor in pdf-view
  (add-hook 'pdf-view-mode-hook
            (lambda ()
              (set (make-local-variable 'evil-normal-state-cursor) (list nil))))

  ;; disable cursor in image-view
  (add-hook 'image-mode-hook
            (lambda ()
              (set (make-local-variable 'evil-normal-state-cursor) (list nil)))))

;;; pdf-view-restore
(use-package pdf-view-restore
  ;; :defer 15
  :after pdf-tools
  :config
  (add-hook 'pdf-view-mode-hook 'pdf-view-restore-mode)
  (setq pdf-view-restore-filename (expand-file-name "var/pdf-view-restore-save.el" user-emacs-directory)))

;;; doc-view
(use-package doc-view
  ;; :defer 15
  :config

  ;; set continuous mode for doc view
  (setq doc-view-continuous t)

  ;; set midnight colors for doc-view---------------------------------------------
  ;; https://emacs.stackexchange.com/a/17366/31478
  (defun define-doc-view-current-cache-dir ()
    ;; doc-view-current-cache-dir was renamed to doc-view--current-cache-dir in Emacs 24.5
    (or (fboundp 'doc-view-current-cache-dir)
        (defalias 'doc-view-current-cache-dir 'doc-view--current-cache-dir)))
  (eval-after-load "doc-view" '(define-doc-view-current-cache-dir))

  (defun doc-view-reverse-colors ()
    "Inverts document colors.\n
Requires an installation of ImageMagick (\"convert\")."
    (interactive)
    ;; error out when ImageMagick is not installed
    (if (/= 0 (call-process-shell-command "convert -version"))
        (error "Reverse colors requires ImageMagick (convert)")
      (when (eq major-mode 'doc-view-mode)
        ;; assume current doc-view internals about cache-names
        (let ((file-name (expand-file-name (format "page-%d.png"
                                                   (doc-view-current-page))
                                           (doc-view-current-cache-dir))))
          (call-process-shell-command
           "convert" nil nil nil "-negate" file-name file-name)
          (clear-image-cache)
          (doc-view-goto-page (doc-view-current-page))))))

  (defun doc-view-reverse-colors-all-pages ()
    "Inverts document colors on all pages.\n
Requires an installation of ImageMagick (\"convert\")."
    (interactive)
    ;; error out when ImageMagick is not installed
    (if (/= 0 (call-process-shell-command "convert -version"))
        (error "Reverse colors requires ImageMagick (convert)")
      (when (eq major-mode 'doc-view-mode)
        ;; assume current doc-view internals about cache-names
        (let ((orig (doc-view-current-page))
              (page nil))
          (message "Reversing video on all pages...")
          (dotimes (pnum (doc-view-last-page-number))
            (setq page (expand-file-name (format "page-%d.png" (1+ pnum))
                                         (doc-view-current-cache-dir)))
            (call-process-shell-command
             "convert" nil nil nil "-negate" page page))
          (clear-image-cache)
          (doc-view-goto-page orig)
          (message "Done reversing video!")))))

  ;; disable cursor in doc-view
  (add-hook 'doc-view-mode
            (lambda ()
              (set (make-local-variable 'evil-normal-state-cursor) (list nil)))))

;;; org-noter
;; (use-package org-noter
;;   :after (pdf-tools org)
;;   ;; :defer 20
;;   )


(provide 'init-pdf)
;;; init-pdf.el ends here
