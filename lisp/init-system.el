;;; init-system.el --- Emacs integration with other OS packages -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;;; Ensure system packages
;; Basically, you can install all system packages through the package manager of
;; your OS. For example, install R and dependencies, rga and dependencies, system
;; fonts, docker and dependencies, exa, rip, terminal emulators, texlive...
;; See the list of all my installed packages at:
;; https://gitlab.com/kguidonimartins/dotfiles-public/-/blob/main/.local/share/miscellaneous/arch-pkg-list.csv

(use-package use-package-ensure-system-package
  ;; :defer 30
  :config
  (use-package system-requirements
    :no-require t
    :ensure nil
    :if (executable-find "yay")
    :ensure-system-package
    ((pandoc . "yay -S --noconfirm pandoc biber bibtool readability-cli texlive-core texlive-latexextra zathura zathura-pdf-mupdf")
     (make   . "yay -S --noconfirm make ctags tmux grep sed sad entr surfraw wakatime-cli-bin shellcheck")
     (rsync  . "yay -S --noconfirm rsync vimiv rm-improved ripgrep ripgrep-all fd unzip gzip unrar exa dragon-drag-and-drop")
     (languagetool . "yay -S --noconfirm languagetool proselint hunspell hunspell-en_us hunspell-pt-br aspell-pt aspell-en"))))

;;;; Set PATH
(use-package exec-path-from-shell
  ;; :defer 1
  :config
  (exec-path-from-shell-initialize))

;;; conf-mode
(add-to-list 'auto-mode-alist '("/\\.[^/]*rc" . conf-mode) t)
(setq native-comp-async-report-warnings-errors nil)

(provide 'init-system)
;;; init-system.el ends here
