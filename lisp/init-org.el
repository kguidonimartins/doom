;;; init-org.el --- Org-mode configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; initial setup

(defun funk/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.100)
                  (org-level-2 . 1.075)
                  (org-level-3 . 1.050)
                  (org-level-4 . 1.025)
                  (org-level-5 . 1.000)
                  (org-level-6 . 1.000)
                  (org-level-7 . 1.000)
                  (org-level-8 . 1.000)))
    (set-face-attribute (car face) nil :font "Fira Code Nerd Font" :weight 'regular :height (cdr face)))

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil           :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-table nil           :inherit 'fixed-pitch)
  (set-face-attribute 'org-formula nil         :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil            :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil           :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil        :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil       :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil        :inherit 'fixed-pitch))

(defun funk/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1))

;; ;; Add it after refile
;; (advice-add 'org-refile :after
;;             (lambda (&rest _)
;;               (funk/save-all-org-buffers)))

(add-hook 'org-agenda-mode-hook
          (lambda ()
            (add-hook 'auto-save-hook 'org-save-all-org-buffers nil t)
            (auto-save-mode)
            (visual-line-mode -1)
            (toggle-truncate-lines 1)))

;; TODO 2021-08-18: learning about custom agenda commands to show old todos
(use-package org
  ;; :defer 5
  :hook (org-mode . funk/org-mode-setup)
  :config
  ;; (setq org-ellipsis " ▾")
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq org-agenda-include-diary t)
  (setq org-agenda-include-deadlines t)
  (setq org-agenda-include-inactive-timestamps nil)
  (setq org-generic-id-locations-file (expand-file-name "var/org/.org-generic-id-locations" user-emacs-directory))
  ;; (advice-add 'org-refile :after 'org-save-all-org-buffers)
  ;; Control how to org agenda is displayed
  (setq org-agenda-window-setup 'only-window)
  (setq org-agenda-restore-windows-after-quit t)
  (setq org-adapt-indentation nil)
  (setq org-startup-folded t)
  (setq org-agenda-span 'week)
  ;; Scale inline images by default
  (setq org-image-actual-width '(1024))
  ;; Show inline images by default
  (setq org-startup-with-inline-images t)
  ;;warn me of any deadlines in next 7 days
  (setq org-deadline-warning-days 7)
  (funk/org-font-setup)
  (require 'org-habit)
  (add-to-list 'org-modules 'org-habit)
  (setq org-habit-graph-column 60))


(defun funk/projectile-project-name-for-org-agenda ()
  (if (>= (string-width (projectile-project-name)) 10)
      (concat
       (truncate-string-to-width (projectile-project-name) 10)
       (truncate-string-ellipsis))
    (projectile-project-name)
    )
  )

;; Save the corresponding buffers
(defun funk/save-all-org-buffers ()
  "Save `org-agenda-files' buffers without user confirmation.
See also `org-save-all-org-buffers'"
  (interactive)
  (message "Saving org-agenda-files buffers...")
  (save-some-buffers t (lambda ()
                         (when (member (buffer-file-name) org-agenda-files)
                           t)))
  (message "Saving org-agenda-files buffers... done"))

;; save and kill all org agenda files after quit agenda
;; https://emacs.stackexchange.com/a/5742/31478
(defvar opened-org-agenda-files nil)

(defun opened-org-agenda-files ()
  (let ((files (org-agenda-files)))
    (setq opened-org-agenda-files nil)
    (mapcar
     (lambda (x)
       (when (get-file-buffer x)
         (push x opened-org-agenda-files)))
     files)))

(defun kill-org-agenda-files ()
  (let ((files (org-agenda-files)))
    (mapcar
     (lambda (x)
       (when
           (and
            (get-file-buffer x)
            (not (member x opened-org-agenda-files)))
         (kill-buffer (get-file-buffer x))))
     files)))

(defun funk/save-and-kill-all-agenda-files()
  (interactive)
  (org-save-all-org-buffers)
  (kill-org-agenda-files))

(defun funk/save-and-kill-all-agenda-files-and-quit-agenda()
  (interactive)
  (org-save-all-org-buffers)
  (kill-org-agenda-files)
  (async-shell-command "remove-saved-org-agenda-view") ; in my path
  (org-agenda-quit)
  (kill-buffer "diary"))

(defmacro η (fnc)
  "Return function that ignores its arguments and invokes FNC."
  `(lambda (&rest _rest)
     (funcall ,fnc)))

(advice-add 'org-deadline       :after (η #'org-save-all-org-buffers))
(advice-add 'org-schedule       :after (η #'org-save-all-org-buffers))
(advice-add 'org-store-log-note :after (η #'org-save-all-org-buffers))
(advice-add 'org-todo           :after (η #'org-save-all-org-buffers))

(defun funk/org-agenda-to-today-and-evil-zt ()
  (interactive)
  (call-interactively 'org-agenda-goto-today)
  (call-interactively 'evil-scroll-line-to-top))

;;; capture templates
;; create a fake binding
(define-key global-map (kbd "C-c j")
  (lambda () (interactive) (org-capture nil "j")))


;; ;; Automatically get the files in "~/Documents/org"
;; ;; with fullpath
;; (setq org-agenda-files
;;       (mapcar 'file-truename
;;            (file-expand-wildcards "~/Documents/org/*.org")))

(setq org-agenda-files
      '(
        "~/google-drive/kguidonimartins/org/archive.org"
        "~/google-drive/kguidonimartins/org/asana-karlo.org"
        "~/google-drive/kguidonimartins/org/asana-team.org"
        "~/google-drive/kguidonimartins/org/gcal.org"
        "~/google-drive/kguidonimartins/org/gcal.org_archive"
        "~/google-drive/kguidonimartins/org/inbox.org"
        "~/google-drive/kguidonimartins/org/meetings.org"
        "~/google-drive/kguidonimartins/org/projects.org"
        "~/google-drive/kguidonimartins/org/journal.org"
        ;; "~/google-drive/kguidonimartins/org/todoist.org"
        ))

(setq org-archive-location  "~/google-drive/kguidonimartins/org/archive.org::datetree/")


(setq org-todo-keywords
      '((sequence "TODO(t!)" "NEXT(n!)" "REVIEW(r@/!)" "WAITING(w@/!)" "|" "DONE(d!)" "CANCELLED(c@/!)")))

;; (setq org-refile-targets
;;       '(
;;         ("archive.org" :maxlevel . 1)
;;         ("tasks.org" :maxlevel . 1)
;;         ("projects.org" :maxlevel . 1)
;;         )
;;       )

(setq org-refile-targets '((org-agenda-files :maxlevel . 10)))

;; makes org-refile outline works with ivy
(setq org-refile-use-outline-path t)
(setq org-outline-path-complete-in-steps nil)
(setq org-refile-allow-creating-parent-nodes 'confirm)

(defun funk/org-search-agenda-files-via-outline ()
  (interactive)
  (org-refile '(4)))

(setq org-tag-alist
      '((:startgroup)
        ;; Put mutually exclusive tags here
        (:endgroup)
        ("fbds-norad" . ?f)
        ("thesis" . ?t)
        ("meeting" . ?m)
        ("cap_02" . ?c)
        ("cap_03" . ?C)
        ("personal" . ?p)
        ("emacs" . ?e)
        ("errand" . ?E)
        ("home" . ?H)
        ("work" . ?W)
        ("agenda" . ?a)
        ("learning" . ?l)
        ("off-topic" . ?o)
        ("notes" . ?N)
        ("idea" . ?i)))

(setq org-capture-templates
      `(
        ;; https://orgmode.org/manual/Template-expansion.html#Template-expansion

        ("t" "Task" entry (file+olp "~/google-drive/kguidonimartins/org/inbox.org" "Inbox")
         "* TODO %?\n  %a\n  %i"
         :empty-lines 1)

        ;; for org-protocol-capture-html
        ("w" "Web site" entry (file+olp "~/google-drive/kguidonimartins/org/inbox.org" "Web")
         "* %c :website:\n%U \n%?%:initial")

        ("j" "Journal" entry (file+olp+datetree "~/google-drive/kguidonimartins/org/journal.org")
         "\n* %<%a %d-%m-%Y %H:%M>\n\n%?\n\n"
         :clock-in
         :clock-resume
         :empty-lines 1)

        ("m" "Meeting" entry (file+olp+datetree "~/google-drive/kguidonimartins/org/meetings.org")
         "* %<%a %d-%m-%Y %H:%M> - %a :meetings:\n\n%?\n\n"
         :clock-in
         :clock-resume
         :empty-lines 1)

        )
      )

;;; agenda custom command and export style

;; Time grid
(setq org-agenda-time-leading-zero t)
(setq org-agenda-timegrid-use-ampm nil)
(setq org-agenda-use-time-grid t)
(setq org-agenda-show-current-time-in-grid t)
(setq org-agenda-current-time-string
      (concat "Now " (make-string 50 ?-)))
;; (setq org-agenda-time-grid
;;       '((daily today require-timed)
;;         (0600 0700 0800 0900 1000 1100
;;               1200 1300 1400 1500 1600
;;               1700 1800 1900 2000 2100)
;;         " ....." "-----------------"))
(setq org-agenda-default-appointment-duration nil)



(setq org-agenda-custom-commands
      `(

        ("g" "Get Things Done (GTD)"

         (


          (todo "TODO"
                (
                 (org-agenda-skip-function
                  '(org-agenda-skip-entry-if 'deadline 'notscheduled))
                 (org-agenda-block-separator nil)
                 (org-agenda-prefix-format "  %i %-12:c %e ")
                 (org-agenda-overriding-header "\nTasks scheduled\n")
                 )
                )

          (todo "TODO"
                (
                 (org-agenda-skip-function
                  '(org-agenda-skip-entry-if 'deadline 'scheduled))
                 (org-agenda-block-separator nil)
                 (org-agenda-prefix-format "  %i %-12:c %e ")
                 (org-agenda-overriding-header "\nTasks not scheduled\n")
                 )
                )

          (agenda ""
                  (
                   (org-agenda-skip-function
                    '(org-agenda-skip-entry-if 'deadline))
                   (org-agenda-block-separator nil)
                   (org-deadline-warning-days 7)
                   (org-agenda-overriding-header "\nWeek-agenda\n")
                   )
                  )

          (agenda ""
                  (
                   (org-agenda-time-grid nil)
                   (org-agenda-start-on-weekday nil)
                   ;; We don't want to replicate the previous section's
                   ;; three days, so we start counting from the day after.
                   (org-agenda-start-day "+1d")
                   (org-agenda-span 20)
                   (org-agenda-show-all-dates nil)
                   (org-deadline-warning-days 0)
                   (org-agenda-block-separator nil)
                   (org-agenda-entry-types '(:deadline))
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "\nUpcoming deadlines (+14d)\n")
                   )
                  )

          )

         )

        ("G" "Daily agenda and top priority tasks"

         (

          (tags-todo "*"
                     ((org-agenda-skip-function '(org-agenda-skip-if nil '(timestamp)))
                      (org-agenda-skip-function
                       `(org-agenda-skip-entry-if
                         'notregexp ,(format "\\[#%s\\]" (char-to-string org-priority-highest))))
                      (org-agenda-block-separator nil)
                      (org-agenda-overriding-header "Important tasks without a date\n")))

          (agenda ""
                  (
                   (org-agenda-span 1)
                   (org-deadline-warning-days 0)
                   (org-agenda-block-separator nil)
                   (org-scheduled-past-days 0)
                   ;; We don't need the `org-agenda-date-today'
                   ;; highlight because that only has a practical
                   ;; utility in multi-day views.
                   (org-agenda-day-face-function (lambda (date) 'org-agenda-date))
                   (org-agenda-format-date "%A %-e %B %Y")
                   (org-agenda-overriding-header "\nToday's agenda\n")
                   )
                  )

          (agenda ""
                  (
                   (org-agenda-start-on-weekday nil)
                   (org-agenda-start-day "+1d")
                   (org-agenda-span 3)
                   (org-deadline-warning-days 0)
                   (org-agenda-block-separator nil)
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "\nNext three days\n")
                   )
                  )

          (agenda ""
                  (
                   (org-agenda-time-grid nil)
                   (org-agenda-start-on-weekday nil)
                   ;; We don't want to replicate the previous section's
                   ;; three days, so we start counting from the day after.
                   (org-agenda-start-day "+3d")
                   (org-agenda-span 14)
                   (org-agenda-show-all-dates nil)
                   (org-deadline-warning-days 0)
                   (org-agenda-block-separator nil)
                   (org-agenda-entry-types '(:deadline))
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "\nUpcoming deadlines (+14d)\n")
                   )
                  )

          )
         )

        )

      )

;; (setq org-agenda-custom-commands
;;       '(("X" agenda ""
;;          ((org-agenda-remove-tags t))
;;          ("~/google-drive/kguidonimartins/org/public/index.html"))))

(setq org-agenda-export-html-style
      "
       <style>
      body {
        color: #000000;
        background-color: #FFFFFF;
      }
      .org-agenda-calendar-event {
        /* org-agenda-calendar-event */
        color: #000000;
      }
      .org-agenda-current-time {
        /* org-agenda-current-time */
        color: #0000c0;
        font-weight: bold;
      }
      .org-agenda-date {
        /* org-agenda-date */
        color: #00538b;
      }
      .org-agenda-date-today {
        /* org-agenda-date-today */
        color: #000000;
        font-weight: bold;
        text-decoration: underline;
      }
      .org-agenda-date-weekend {
        /* org-agenda-date-weekend */
        color: #005a5f;
      }
      .org-agenda-done {
        /* org-agenda-done */
        color: #315b00;
      }
      .org-agenda-structure {
        /* org-agenda-structure */
        color: #2544bb;
      }
      .org-done {
        /* org-done */
        color: #005e00;
        background-color: #f2eff3;
        font-weight: bold;
      }
      .org-scheduled-previously {
        /* org-scheduled-previously */
        color: #863927;
      }
      .org-time-grid {
        /* org-time-grid */
        color: #56576d;
      }
      .org-todo {
        /* org-todo */
        color: #6666ff;
        background-color: #F1FA8C;
        font-weight: bold;
      }
      .org-upcoming-deadline {
        /* org-upcoming-deadline */
        color: #a0132f;
      }
      .org-upcoming-distant-deadline {
        /* org-upcoming-distant-deadline */
        color: #5f0000;
      }
      .org-warning {
        /* org-warning */
        color: #a0132f;
        font-weight: bold;
      }

      a {
        color: inherit;
        background-color: inherit;
        font: inherit;
        text-decoration: inherit;
      }
      a:hover {
        text-decoration: underline;
      }
       </style>
       "
      )

(setq org-agenda-prefix-format
      '((agenda . " %i %-12:c%?-12t% s")
        (todo   . " %i %-12:c")
        (tags   . " %i %-12:c")
        (search . " %i %-12:c")))


;;; org dispatch height
(defadvice org-agenda (around split-horizontally activate)
  (let ((split-height-threshold 50))  ; or whatever width makes sense for you
    ad-do-it))

;;; custom holidays

(load-file (expand-file-name "lisp/github/emacs-brazilian-holidays/brazilian-holidays.el" user-emacs-directory))

;;; org-gcal

(use-package org-gcal
  ;; https://github.com/kidd/org-gcal.el
  ;; :defer 5
  :after org
  :config
  (setq org-gcal-client-id (funk-auth 'gcalid))
  (setq org-gcal-client-secret (funk-auth 'gcalkey))
  (setq org-gcal-remove-api-cancelled-events t)
  (setq org-gcal-notify-p nil)
  (setq org-gcal-recurring-events-mode "nested")
  (setq org-gcal-file-alist '(("kguidonimartins@gmail.com" . "~/google-drive/kguidonimartins/org/gcal.org")))
  (add-hook 'org-agenda-mode-hook (lambda () (org-gcal-sync)))
  (add-hook 'org-capture-after-finalize-hook (lambda () (org-gcal-sync))))

;;; org-bullets

(use-package org-bullets
  ;; :defer 5
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("●" "●" "●" "●" "●" "•")))

;;; center org buffers

;; **** Center Org Buffers
;; We use
;; [[https://github.com/joostkremers/visual-fill-column][visual-fill-column]]
;; to center =org-mode= buffers for a more pleasing writing experience
;; as it centers the contents of the buffer horizontally to seem more
;; like you are editing a document.  This is really a matter of
;; personal preference so you can remove the block below if you don't
;; like the behavior.

;; (defun funk/org-mode-visual-fill ()
;;   (setq visual-fill-column-width 100
;;         visual-fill-column-center-text t)
;;   (visual-fill-column-mode 1))

;; (use-package visual-fill-column
;;   :hook (org-mode . funk/org-mode-visual-fill))

;;; babel

;; **** Configure Babel Languages
;; To execute or export code in =org-mode= code blocks, you'll need to
;; set up =org-babel-load-languages= for each language you'd like to
;; use. [[https://orgmode.org/worg/org-contrib/babel/languages.html][This
;; page]] documents all of the languages that you can use with
;; =org-babel=.

;; NOTE: just type `<r' and hit `<TAB>' to R block
;; TODO: include https://github.com/nnicandro/emacs-jupyter

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (shell . t)
   (R . t)
   (python . t)
   ;; based on: https://sqrtminusone.xyz/posts/2021-05-01-org-python/
   (jupyter . t)
   (clojure .t)
   (java .t)
   ))

;; based on: https://sqrtminusone.xyz/posts/2021-05-01-org-python/
(org-babel-jupyter-override-src-block "python")
(setq ob-async-no-async-languages-alist '("python" "jupyter-python"))

(push '("conf-unix" . conf-unix) org-src-lang-modes)

;; skip confirmation when evaluating source blocks
(defun funk/org-confirm-babel-evaluate (lang body)
  (not (member lang '("emacs-lisp" "python" "shell" "R"))))
(setq org-confirm-babel-evaluate 'funk/org-confirm-babel-evaluate)

(defun funk/org-src-sha-to-image ()
  "Generate a string to plot filename in R source block.
Based on: https://blog.tecosaur.com/tmio/2021-11-30-element.html#functions-as-default"
  (concat "generated-"
          (substring
           (sha1 (org-element-property :value (org-element-at-point)))
           0 8)
          ".svg"))

(defun funk/org-src-guess-results-type ()
  "Guess the argument header types of the R source block.
Based on: https://blog.tecosaur.com/tmio/2021-11-30-element.html#functions-as-default"
  (if (string-match-p "^ *\\(?:plot\\|ggplot\\)([^\n]+\n?\\'"
                      (org-element-property :value (org-element-at-point)))
      "graphics file" "replace"))

(setq org-babel-default-header-args:R
      '((:results . funk/org-src-guess-results-type)
        (:file . funk/org-src-sha-to-image)))

(defun funk/org-previous-source-block-headers ()
  "Returns the previous source block's headers."
  (save-excursion
    (org-babel-previous-src-block)
    (let ((element (org-element-at-point)))
      (when (eq (car element) 'src-block)
        (let* ((content  (cadr element))
               (lang     (plist-get content :language))
               (switches (plist-get content :switches))
               (parms    (plist-get content :parameters)))
          (delq nil (list lang switches parms)))))))

(defun funk/org-previous-source-block-headers-string ()
  "Returns the previous source block's headers as a string."
  (mapconcat #'identity
             (funk/org-previous-source-block-headers)
             " "))

(defun funk/org-previous-source-block-without-content ()
  "Returns the previous source block, except its contents."
  (format
   (concat "#+begin_src %s\n"
           "\n"
           "#+end_src\n")
   (funk/org-previous-source-block-headers-string)))

(defun funk/org-repeat-previous-source-block-without-content ()
  "Inserts the previous source block, except the actual code in
the block, into the buffer."
  (interactive)
  (when-let (result (funk/org-previous-source-block-without-content))
    (insert result)
    (previous-line 2)))

;; (use-package company-org-block
;;   :defer 10
;;   :after org
;;   :config
;;   (setq company-org-block-edit-style 'inline) ;; 'auto, 'prompt, or 'inline
;;   ;; :hook ((org-mode . (lambda ()
;;   ;;                      (setq-local company-backends '(company-org-block))
;;   ;;                      (setq-local company-minimum-prefix-length 4)
;;   ;;                      (company-mode +1))))
;;   )

;; Now, you should be able to use source blocks with names like
;; jupyter-LANG, e.g. jupyter-python. To use just LANG src blocks,
;; call the following function after org-babel-do-load-languages:
;; (org-babel-jupyter-override-src-block "python")

;; **** Structure Templates
;; Org Mode's
;; [[https://orgmode.org/manual/Structure-Templates.html][structure
;; templates]] feature enables you to quickly insert code blocks into
;; your Org files in combination with =org-tempo= by typing =<=
;; followed by the template name like =el= or =py= and then press
;; =TAB=.  For example, to insert an empty =emacs-lisp= block below,
;; you can type =<el= and press =TAB= to expand into such a block.
;; You can add more =src= block templates below by copying one of the
;; lines and changing the two strings at the end, the first to be the
;; template name and the second to contain the name of the language
;; [[https://orgmode.org/worg/org-contrib/babel/languages.html][as it
;; is known by Org Babel]].
;; This is needed as of Org 9.2
(require 'org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("rstats" . "src R"))


;; ;; **** Auto-tangle Configuration Files
;; ;; This snippet adds a hook to =org-mode= buffers so that
;; ;; =funk/org-babel-tangle-config= gets executed each time such a
;; ;; buffer gets saved.  This function checks to see if the file being
;; ;; saved is the Emacs.org file you're looking at right now, and if so,
;; ;; automatically exports the configuration here to the associated
;; ;; output files.
;; ;; Automatically tangle our Emacs.org config file when we save it
;; (defun funk/org-babel-tangle-config ()
;;   (when (string-equal (file-name-directory (buffer-file-name))
;;                       (expand-file-name user-emacs-directory))
;;     ;; Dynamic scoping to the rescue
;;     (let ((org-confirm-babel-evaluate nil))
;;       (org-babel-tangle))))
;;
;; (add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'funk/org-babel-tangle-config)))

;; **** Outline
;; Outline is a must when writing long document, use =org-tree-to-indirect-buffer= to split that subtree in another buffer.

;; (use-package org-sidebar)

;;; org-download

(use-package org-download
  ;; :defer 10
  )

;;; evil-org

(use-package evil-org
  ;; :defer 5
  ;; maps modified in the source code
  :load-path
  (lambda () (expand-file-name "lisp/github/evil-org-mode/" user-emacs-directory))
  :after org
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys)
  (add-hook 'org-mode-hook 'evil-org-mode))

;;; org-projectile

;; (use-package org-projectile
;;   :defer 5
;;   :config
;;   (setq org-confirm-elisp-link-function nil)
;;   (setq org-projectile-capture-template "* %(clean-todo-string (description-text))\n %a\n %i \n")
;;   (progn
;;     (setq org-projectile-projects-file "~/google-drive/kguidonimartins/org/projectile-projects.org")
;;     (setq org-agenda-files (append org-agenda-files (org-projectile-todo-files)))
;;     (push (org-projectile-project-todo-entry) org-capture-templates)))

;;; org-protocol-capture-html

(use-package org-protocol-capture-html
  :after org
  ;; :defer 5
  :load-path (lambda () (expand-file-name "lisp/github/org-protocol-capture-html" user-emacs-directory))
  :config
  (require 'org-protocol-capture-html)
  )

;;; emacs-todoist

(use-package todoist
  ;; :defer 5
  :config
  (setq todoist-token (funk-auth 'todoist))
  (setq todoist-backing-buffer "~/google-drive/kguidonimartins/org/todoist.org")
  ;; (define-key map (kbd "C-x t") 'todoist-task-menu)
  ;; (define-key map (kbd "C-x p") 'todoist-project-menu)
  )

;;; org-notifications

;; (use-package org-notifications
;;   :init
;;   (org-notifications-start)
;;   :config
;;   (setq org-notifications-play-sounds nil))

;; (use-package org-wild-notifier)

;; (use-package org
;;   :ensure org-plus-contrib)

;; (use-package org-notify
;;   :ensure nil
;;   :after org
;;   :config
;;   (org-notify-start))

;; ;; https://blog.dachary.org/2017/09/05/gnome3-libnotify-notification-for-org-mode-appointments/
;; ;; Desktop notifications
;; (setq alert-default-style 'libnotify)
;; (setq appt-disp-window-function (lambda (min-to-app new-time appt-msg)
;;                                   (alert appt-msg)))
;; (setq appt-delete-window-function (lambda ()))
;; ;; Rebuild the reminders everytime the agenda is displayed
;; (add-hook 'org-agenda-finalize-hook (lambda () (org-agenda-to-appt t)))
;; ;; Run once when Emacs starts
;; (org-agenda-to-appt t)
;; ;; Activate appointments so we get notifications
;; (appt-activate t)

;; (use-package org-alert
;;   :config
;;   (setq alert-default-style 'libnotify))

;;; org-pandoc-import
;; (use-package org-pandoc-import
;;   :after org
;;   :load-path (lambda () (expand-file-name "lisp/github/org-pandoc-import" user-emacs-directory)))

;;; org-rifle
(use-package helm-org-rifle
  :config
  (define-key helm-org-rifle-map (kbd "C-j") 'helm-next-line)
  (define-key helm-org-rifle-map (kbd "C-k") 'helm-previous-line))

;;; org-edna
;; REVIEW: https://www.nongnu.org/org-edna-el/

;;; poly-org
;; (use-package poly-org
;;   :disabled
;;   ;; this enable the syntax highlighting and auto-complete into the source blocks
;;   ;; without the need of C-c \'.
;;   ;; :defer 3
;;   :config
;;   (add-to-list 'auto-mode-alist '("\\.org" . poly-org-mode)))

;;; org-appear

(use-package org-appear
  :after org
  :config
  (add-hook 'org-mode-hook 'org-appear-mode)
  (setq org-appear-trigger 'manual)
  (add-hook 'evil-insert-state-entry-hook #'org-appear-manual-start nil t)
  (add-hook 'evil-insert-state-exit-hook #'org-appear-manual-stop nil t))

(use-package org-ros
  :config
  (defun org-ros ()
    "Screenshots an image to an org-file."
    (interactive)
    (make-directory "img/capture" :parents)
    (if buffer-file-name
        (progn
          (message "Waiting for region selection with mouse...")
          (let ((filename
                 (concat "./img/capture/"
                         (file-name-nondirectory buffer-file-name)
                         "_"
                         (format-time-string "%Y%m%d_%H%M%S")
                         ".png")))
            (if (executable-find org-ros-primary-screencapture)
                (progn
                  (shell-command-to-string "killall picom")
                  (call-process org-ros-primary-screencapture nil nil nil org-ros-primary-screencapture-switch filename))
              (call-process org-ros-secondary-screencapture nil nil nil org-ros-secondary-screencapture-switch filename))
            (insert "#+attr_org: :width 300px\n")
            (insert "[[" filename "]]")
            (org-display-inline-images t t))
          (message "File created and linked..."))
      (message "You're in a not saved buffer! Save it first!")))
  )

;;; Weekly review functions
(defun funk/org-file-from-subtree (&optional name)
  "Cut the subtree currently being edited and create a new file
from it.

If called with the universal argument use the subtree title, otherwise prompt for new filename.

Adapted from:
https://superuser.com/a/568300/650180
https://emacs.stackexchange.com/a/60555/31478 "
  (interactive "P")
  (org-back-to-heading)
  (forward-word)
  (let* ((link (org-element-lineage (org-element-context) '(link) t))
         (type (org-element-property :type link))
         (url (org-element-property :path link))
         (url (concat type ":" url))
         (filename (cond
                    (t
                     (expand-file-name
                      (read-file-name "New file name: " "~/google-drive/kguidonimartins/git-repos/vimwiki/docs/til/")))
                    (current-prefix-arg
                     (concat
                      (expand-file-name
                       (org-element-property :title
                                             (org-element-at-point))
                       default-directory)
                      ".org")))))
    (org-cut-subtree)
    (find-file-noselect filename)
    (make-directory (file-name-directory filename) :parents)
    (with-temp-file filename
      ;; (org-mode)
      (insert "# Original subtree\n")
      (yank)
      (newline)
      (unless (equal url ":")
        (progn
          (insert "# Scraped content\n\n")
          (insert (shell-command-to-string (concat "link2org 2> /dev/null " url))))))))

;;; notify org-clock-in

(defun funk/notify-send-org-clock-running-string ()
  "System notification, via notify-send, when there is a org-clock running.
Inspired by: https://github.com/freddez/gnome-shell-simple-message"
  (interactive)
  (when (fboundp 'org-clocking-p)
    (when (org-clocking-p)
      (call-process "notify-send" nil nil nil
                    "-i" "/usr/share/emacs/29.0.50/etc/images/icons/hicolor/128x128/apps/emacs.png"
                    "-t" "60000"
                    "org-clock running"
                    (concat (org-clock-get-clock-string))))))
;; (funk/notify-send-org-clock-running-string)
(run-with-timer 0 60 'funk/notify-send-org-clock-running-string)

;;; change directory before open org agenda

(defun funk/open-agenda ()
  (interactive)
  (progn
    (cd "~/")
    (org-agenda)))

(global-set-key (kbd "C-c a") 'funk/open-agenda)





(provide 'init-org)
;;; init-org.el ends here
