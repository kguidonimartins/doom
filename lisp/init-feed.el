;;; -*- lexical-binding: t; -*-
(use-package elfeed
  ;; :defer 10
  :config
  (setq elfeed-feeds
        '(
          ("https://www.reddit.com/r/Rlanguage.rss" rstats)
          ("https://www.reddit.com/r/dataengineering.rss" dataengineering)
          ("https://www.reddit.com/r/emacs.rss" emacs)
          ("https://www.reddit.com/r/evilmode.rss" evil-mode)
          ("https://www.reddit.com/r/orgmode.rss" org-mode)
          ("https://www.reddit.com/r/planetemacs.rss" planetemacs)
          ))
  (defun elfeed-mark-all-as-read ()
    (interactive)
    (mark-whole-buffer)
    (elfeed-search-untag-all-unread)))

(provide 'init-feed)
