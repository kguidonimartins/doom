;;; init-indent.el --- indentation configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; indentation guides

;; (use-package highlight-indent-guides
;;   :config
;;   (setq highlight-indent-guides-method 'bitmap)
;;   (setq highlight-indent-guides-responsive 'top)
;;   (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
;;   (add-hook 'ess-mode-hook 'highlight-indent-guides-mode))

;; (use-package indent-guide
;;   :config
;;   (indent-guide-global-mode))

;;; aggressive-indent
(use-package! aggressive-indent
  :hook
  ((emacs-lisp-mode) . aggressive-indent-mode)
  :config
  ;; tab complete and indent
  (setq tab-always-indent 'complete)
  (setq-default indent-tabs-mode nil))

;;; electric-operator

(use-package! electric-operator
  ;; Electric operator will turn ~a=10*5+2~ into ~a = 10 * 5 + 2~, so let's
  ;; enable it for R:
  :after (ess python)
  :hook
  ((ess-r-mode python-mode) . electric-operator-mode)
  ((ess-r-mode python-mode) . electric-pair-mode)
  :config
  (setq electric-pair-preserve-balance nil)
  (setq electric-operator-R-named-argument-style 'spaced)
  (electric-operator-add-rules-for-mode 'ess-r-mode (cons "in" nil) (cons "%" nil)))

(provide 'init-indent)
;;; init-indent.el ends here
