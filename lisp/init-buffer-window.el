;;; init-buffer-window.el --- buffer and window configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; window management


(defun funk/only-current-buffer ()
  "Kill all other buffers: like ':only' in vim."
  (interactive)
  (mapc 'save-buffer (cdr (buffer-list (current-buffer))))
  (mapc 'kill-buffer (cdr (buffer-list (current-buffer))))
  (delete-other-windows))

(defun funk/clean-up-buffer-list ()
  (interactive)
  (tramp-cleanup-all-connections)
  ;; (ignore-errors (funk/save-and-kill-all-agenda-files-and-quit-agenda))
  (visit-initel)
  (funk/only-current-buffer)
  (switch-to-buffer "*Messages*")
  (mapc 'kill-buffer (cdr (buffer-list (current-buffer))))
  (message "Cleaned buffer list!"))

(defalias 'cb 'funk/clean-up-buffer-list)

;;; eyebrowse
;; workspace management (dont preserve buffer list by projects)
(use-package! eyebrowse
  :init
  (setq eyebrowse-keymap-prefix (kbd "C-c w"))
  :config
  (eyebrowse-mode)
  (setq eyebrowse-mode-line-style 'always)
  (setq eyebrowse-new-workspace 'dired-jump)
  (when (and (featurep 'desktop)
             (featurep 'eyebrowse))
    (dolist ($param '(eyebrowse-window-config
                      eyebrowse-current-slot
                      eyebrowse-last-slot))
      (add-to-list 'frameset-filter-alist '($param . :save))))

  (add-to-list 'window-persistent-parameters '(window-side . writable))
  (add-to-list 'window-persistent-parameters '(window-slot . writable)))

;;; popper
;; popup windows
(use-package! popper
  :init
  (setq popper-reference-buffers
        '(
          ;; "\\*Messages\\*"
          "Output\\*$"
          "\\*ESS\\*"
          "\\*vterminal"
          ;; "^\\*Warnings\\*"
          ;; "^\\*Helpful\\*"
          "^\\*PDF-Occur\\*"
          "^\\*helm-ag\\*"
          ;; "^\\*R dired\\*"
          "^\\*Async Shell Command\\*"
          "^\\*Occur\\*"
          ;; "^\\*R:"
          "^\\*R Data View:"
          "^\\*compilation"
          "^\\*interpretation\\*$"
          "^\\*Compile-Log\\*$"
          "\\*HTTP Response\\*"
          "\\*Async-native-compile-log\\*"
          ))
  (popper-mode +1)
  :config
  (setq popper-display-control nil)
  (setq popper-group-function #'popper-group-by-projectile)
  ;; NOTE: this is related to `vterm' and `multi-vterm' packages.
  ;; As `vterm' terminals are opened as a `POP' window, I cannot
  ;; figure out how to close the terminal window after `exit' command.
  ;; See: https://github.com/akermu/emacs-libvterm/issues/24
  ;; So, I mapped `(global-set-key (kbd "C-c C-x") 'popper-kill-latest-popup)'
  ;; and add a advice to auto-confirm the kill.
  (advice-add #'popper-kill-latest-popup :around #'auto-yes))

;;; shackle
;; windows management
(use-package! shackle
  :after (org vterm ess)
  :config
  (progn
    (setq shackle-lighter "")
    (setq shackle-select-reused-windows nil) ; default nil
    (setq shackle-default-alignment 'below) ; default below
    (setq shackle-rules
          ;;                          :regexp nil :select nil :inhibit-window-quit nil :size 0.00 :align nil :other nil :same|:popup
          '(("*undo-tree*"                                                             :size 0.25 :align right)
            ("*eshell*"                           :select t                                                  :other t)
            ("*Messages*"                         :select t   :align below                                   :other t)
            ("*Warnings*"                         :select nil :align below                                   :other t)
            ("*HTTP Response*"                         :select nil :align right                                   :other t)
            ("*Shell Command Output*"             :select nil)
            ("*Async Shell Command*"              :select nil :inhibit-window-quit nil :size 0.5  :align below)
            ("*PDF-Occur*"                        :select t   :inhibit-window-quit nil                       :other t)
            ("*compilation*"                      :select nil :inhibit-window-quit nil :size 0.5  :align below)
            ("*helm-ag*"                          :select nil :inhibit-window-quit t                         :other t)
            ("*rg*"                               :select t   :inhibit-window-quit nil :size 0.5  :align left)
            (occur-mode                           :select t                                       :align t)
            ;; ("*Help*"                             :select t   :inhibit-window-quit t                         :other t)
            ("*Python Doc*"                       :select t)
            ("*Completions*"                                                           :size 0.3  :align t)
            ("*Occur*"                            :select t                            :size 0.5  :align left)
            ("*Register Preview*"                 :select t                            :size 0.5  :align below)
            ("^\\*vterminal *?"       :regexp t   :select t   :inhibit-window-quit nil   :size 0.5  :align below)
            ("^\\*interpretation\\*$" :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
            ("^\\*Compile-Log\\*$"    :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
            ("\\*[Wo]*Man.*\\*"       :regexp t   :select t   :inhibit-window-quit t                         :other t)
            ("\\*poporg.*\\*"         :regexp t   :select t                                                  :other t)
            ("\\`\\*helm.*?\\*\\'"    :regexp t                                        :size 0.3  :align t)
            ("*Calendar*"                         :select t                            :size 0.3  :align below)
            ("*info*"                             :select t   :inhibit-window-quit t                                    :same t)
            (magit-status-mode                    :select t   :inhibit-window-quit t   :size 0.5  :align below          :same nil)
            (magit-log-mode                       :select t   :inhibit-window-quit t                                    :same t)
            ("\\`\\*R Data View:.*?\\*\\'" :regexp t   :select t :inhibit-window-quit nil :size 0.5 :align below )
            ))
    (shackle-mode 1))
  )

;;; transpose-frame
(use-package! transpose-frame
  ;; :defer 5
  :preface
  (defun funk/frame-flip (&optional arg)
    "Flip window layout.
With `\\[universal-argument]' prefix argument ARG, flip
vertically, else, flip horizontally."
    (interactive "P")
    (if arg
        (flip-frame)
      (flop-frame)))

  (defun funk/frame-rotate (&optional arg)
    "Rotate window layout.
With `\\[universal-argument]' prefix argument ARG, rotate
clockwise, else, rotate counterclockwise."
    (interactive "P")
    (if arg
        (rotate-frame-clockwise)
      (rotate-frame-anticlockwise)))

  :config
  (global-set-key (kbd "C-c w f") 'funk/frame-flip)
  (global-set-key (kbd "C-c w R") 'funk/frame-rotate)
  (global-set-key (kbd "C-c w t") 'transpose-frame))

;;; winner
;; undo and redo window positions
(use-package! winner
  ;; :defer 5
  :demand t
  :config
  (global-set-key (kbd "C-c w u") 'winner-undo)
  (global-set-key (kbd "C-c w r") 'winner-redo)
  (winner-mode))

;;; zoom-window
;; like tmux `prefix-z', but better!
(use-package! zoom-window
  ;; :defer 5
  :config
  (global-set-key (kbd "<f8>") 'zoom-window-next)
  (global-set-key (kbd "<f9>") 'zoom-window-zoom)
  (custom-set-variables
   '(zoom-window-mode-line-color "#FF6E67")))


;;; custom functions to narrowing windows
;; this is the helper code from funk/common.el
(defun funk/common-window-bounds ()
  "Determine start and end points in the window."
  (list (window-start) (window-end)))

(defun funk/simple-narrow-visible-window ()
  "Narrow buffer to wisible window area.
Also check `funk/simple-narrow-dwim'."
  (interactive)
  (let* ((bounds (funk/common-window-bounds))
         (window-area (- (cadr bounds) (car bounds)))
         (buffer-area (- (point-max) (point-min))))
    (if (/= buffer-area window-area)
        (narrow-to-region (car bounds) (cadr bounds))
      (user-error "Buffer fits in the window; won't narrow"))))

(defun funk/simple-narrow-dwim ()
  "Do-what-I-mean narrowing.
If region is active, narrow the buffer to the region's
boundaries.

If no region is active, narrow to the visible portion of the
window.

If narrowing is in effect, widen the view."
  (interactive)
  (unless mark-ring                  ; needed when entering a new buffer
    (push-mark (point) t nil))
  (cond
   ((and (use-region-p)
         (null (buffer-narrowed-p)))
    (let ((beg (region-beginning))
          (end (region-end)))
      (narrow-to-region beg end)))
   ((null (buffer-narrowed-p))
    (funk/simple-narrow-visible-window))
   (t
    (widen)
    (recenter))))

;;; easily rename file and buffer
(defun funk/rename-file-and-buffer (name)
  "Apply NAME to current file and rename its buffer.
Do not try to make a new directory or anything fancy."
  (interactive
   (list (read-file-name "Rename current file: " (buffer-file-name))))
  (let ((file (buffer-file-name)))
    (if (vc-registered file)
        (vc-rename-file file name)
      (rename-file file name))
    (set-visited-file-name name t t)))

;;; ibuffer
(use-package! ibuffer
  ;; :defer 5
  :config
  (setq ibuffer-use-other-window nil)
  (setq ibuffer-movement-cycle nil)
  (setq ibuffer-default-sorting-mode 'filename/process)
  (setq ibuffer-use-header-line t)
  (setq ibuffer-default-shrink-to-minimum-size t)
  (setq ibuffer-old-time 48)
  (setq ibuffer-formats
        '((mark modified read-only locked " "
                (name 50 50 :left :elide)
                " "
                (size 9 -1 :right)
                " "
                (mode 16 16 :left :elide)
                " " filename-and-process)
          (mark " "
                (name 16 -1)
                " " filename
                project-relative-file)))
  (add-hook 'ibuffer-mode-hook #'hl-line-mode))

;;; all-the-icons-ibuffer
(use-package! all-the-icons-ibuffer
  ;; :defer 5
  :init
  (all-the-icons-ibuffer-mode 1)
  :config
  ;; The default icon size in ibuffer.
  (setq all-the-icons-ibuffer-icon-size 1.0)
  ;; The default vertical adjustment of the icon in ibuffer.
  (setq all-the-icons-ibuffer-icon-v-adjust 0.0)
  ;; Use human readable file size in ibuffer.
  (setq  all-the-icons-ibuffer-human-readable-size t)
  ;; Slow Rendering
  ;; If you experience a slow down in performance when rendering multiple icons simultaneously,
  ;; you can try setting the following variable
  (setq inhibit-compacting-font-caches t))


;;; when opening multiple files

;; Removes *Completions* from buffer after you've opened a file.
(add-hook 'minibuffer-exit-hook
          #'(lambda ()
              (let ((buffer "*Completions*"))
                (and (get-buffer buffer)
                     (kill-buffer buffer)))))

;; Don't show *Buffer list* when opening multiple files at the same time.
(setq inhibit-startup-buffer-menu t)

;; Show only one active window when opening multiple files at the same time.
(add-hook 'window-setup-hook 'delete-other-windows)

;;; uniquify
(use-package! uniquify
  :config
  (setq uniquify-buffer-name-style 'forward)
  (setq uniquify-trailing-separator-p t))

;;; autorevert
(use-package! autorevert
  :config
  (global-auto-revert-mode)
  (setq global-auto-revert-non-file-buffers t)
  (setq auto-revert-verbose nil)
  :delight auto-revert-mode)

;;; custom functions to switch between buffers
(defun funk/switch-to-previous-buffer ()
  "Switch to previously open buffer.
Repeated invocations toggle between the two most recently open buffers."
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))

;; Split the windows sensibly.
;; https://github.com/sje30/emacs#alternative-c-x-3-and-c-x-2
;; https://gitlab.com/jabranham/emacs/blob/master/init.el#L2537
(defun funk/split-below-last-buffer (prefix)
  "Split the window above/below and display the previous buffer.
If prefix arg is provided, show current buffer twice."
  (interactive "p")
  (split-window-below)
  (other-window 1 nil)
  (if (= prefix 1)
      (switch-to-next-buffer)))

(defun funk/split-right-last-buffer (prefix)
  "Split the window left/right and display the previous buffer
If prefix arg is provided, show current buffer twice."
  (interactive "p")
  (split-window-right)
  (other-window 1 nil)
  (if (= prefix 1)
      (switch-to-next-buffer)))

(defun funk/split-below-projectile-project-root ()
  "Split the window above/below and display the previous buffer.
If prefix arg is provided, show current buffer twice."
  (interactive)
  (split-window-below)
  (other-window 1 nil)
  (switch-to-buffer (dired (projectile-project-root))))

(advice-add 'funk/split-below-projectile-project-root :after #'ivy-switch-buffer)

(defun funk/split-right-projectile-project-root ()
  "Split the window left/right and display the previous buffer
If prefix arg is provided, show current buffer twice."
  (interactive)
  (split-window-right)
  (other-window 1 nil)
  (switch-to-buffer (dired (projectile-project-root))))

(advice-add 'funk/split-right-projectile-project-root :after #'ivy-switch-buffer)

(setq switch-to-prev-buffer-skip 'this)


;;; auto-save when switching buffers
;; https://stackoverflow.com/a/1413990/5974372
(defadvice switch-to-buffer (before save-buffer-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice other-window (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice other-frame (before other-frame-now activate)
  (when buffer-file-name (save-buffer)))

;;; create scratch based on the current major mode
(defcustom funk/simple-scratch-buffer-default-mode 'markdown-mode
  "Default major mode for `funk/simple-scratch-buffer'."
  :type 'symbol
  :group 'funk/simple)

;; The idea is based on the `scratch.el' package by Ian Eure:
;; <https://github.com/ieure/scratch-el>.

;; Adapted from the `scratch.el' package by Ian Eure.
(defun funk/simple--scratch-list-modes ()
  "List known major modes."
  (cl-loop for sym the symbols of obarray
           for name = (symbol-name sym)
           when (and (functionp sym)
                     (not (member sym minor-mode-list))
                     (string-match "-mode$" name)
                     (not (string-match "--" name)))
           collect name))

(defun funk/clean-symbol (s)
  (string-replace "-mode" "" (symbol-name s)))

(defun funk/simple--scratch-buffer-setup (region &optional mode)
  "Add contents to `scratch' buffer and name it accordingly.

REGION is added to the contents to the new buffer.

Use the current buffer's major mode by default.  With optional
MODE use that major mode instead."
  (let* ((major (or mode major-mode))
         (time-string (shell-command-to-string "echo -n $(date '+%Y%m%d-%H%M%S')"))
         (fmajor (if (not mode) (funk/clean-symbol (symbol-value (intern "major-mode")))
                   (funk/clean-symbol funk/simple-scratch-buffer-default-mode)))
         (string (format "-*- mode: %s; -*-\nScratch buffer for: %s\nCreated at: %s\n\n" fmajor major time-string))
         (text (concat string region))
         (buf (format "*Scratch for %s %s*" major time-string))
         (proj (if (or (equal (projectile-project-name) "karlo")
                       (equal (projectile-project-name) "-"))
                   "unknown"
                 (projectile-project-name)))
         (dir (concat "~/.local/share/scratch-buffers/" proj))
         (fbuf (concat dir "/" proj "_" fmajor "_" time-string)))
    (with-current-buffer
        (get-buffer-create buf)
      (funcall major)
      (save-excursion
        (insert text)
        (goto-char (point-min))
        (comment-region (point-at-bol) (progn (vertical-motion 2) (point-at-eol))))
      (vertical-motion 4))
    (pop-to-buffer buf)
    (make-directory dir :parents)
    (write-file fbuf)
    (when (equal region "")
      (goto-line 5)
      (evil-insert 0)
      (newline))))

;;;###autoload
(defun funk/simple-scratch-buffer (&optional arg)
  "Produce a bespoke scratch buffer matching current major mode.

With optional ARG as a prefix argument (\\[universal-argument]),
use `funk/simple-scratch-buffer-default-mode'.

With ARG as a double prefix argument, prompt for a major mode
with completion.

If region is active, copy its contents to the new scratch
buffer."
  (interactive "P")
  (let* ((default-mode funk/simple-scratch-buffer-default-mode)
         (modes (funk/simple--scratch-list-modes))
         (region (with-current-buffer (current-buffer)
                   (if (region-active-p)
                       (buffer-substring-no-properties
                        (region-beginning)
                        (region-end))
                     "")))
         (m))
    (pcase (prefix-numeric-value arg)
      (16 (progn
            (setq m (intern (completing-read "Select major mode: " modes nil t)))
            (funk/simple--scratch-buffer-setup region m)))
      (4 (funk/simple--scratch-buffer-setup region default-mode))
      (_ (funk/simple--scratch-buffer-setup region)))))

(defun funk/simple-markdown-mode-scratch-buffer ()
  (interactive)
  (funk/simple-scratch-buffer '(4)))

(defalias 'sm 'funk/simple-markdown-mode-scratch-buffer)
(defalias 'sc 'funk/simple-scratch-buffer)

(provide 'init-buffer-window)
;;; init-buffer-window.el ends here
