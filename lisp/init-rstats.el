;;; init-rstats.el --- rstats configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; ESS (ess-mode)
(use-package! ess
  :init
  (require 'ess-site)
  (progn
    (require 'ess-site)
    (setq abbrev-mode t)
    (define-abbrev-table 'ess-mode-abbrev-table
      '(
        ("pp" "%>%" nil 0)
        )
      )
    (dolist (hook '(ess-mode-hook inferior-ess-mode-hook))
      (add-hook hook
                (lambda ()
                  (make-local-variable 'local-abbrev-table)
                  (setq local-abbrev-table ess-mode-abbrev-table)
                  )
                )
      ))
  :ensure ess
  :config

  (custom-set-faces
   '(ess-function-call-face ((t (:weight bold :inherit font-lock-function-name-face))))
   '(ess-keyword-face ((t (:inherit ess-function-call-face))))
   '(ess-modifiers-face ((t (:inherit ess-function-call-face))))
   )

  (general-define-key
   :keymaps '(ess-mode-map)
   :states '(normal)
   ",," #'funk/ess-eval-and-recenter
   "SPC SPC" #'funk/ess-eval-and-recenter
   ",a" #'funk/ess-select-bracket-block
   "SPC a" #'funk/ess-select-bracket-block
   ",ec" #'funk/polymode-eval-R-chunk
   "SPC ec" #'funk/polymode-eval-R-chunk
   ",ei" #'funk/add-in-here
   "SPC ei" #'funk/add-in-here
   ",en" #'funk/polymode-eval-R-chunk-and-next
   "SPC en" #'funk/polymode-eval-R-chunk-and-next
   ",ew" #'funk/ess-eval-word
   "SPC ew" #'funk/ess-eval-word
   ",q" #'funk/ess-quit-kill-inferior
   "SPC q" #'funk/ess-quit-kill-inferior
   ",rp" #'funk/ess-baseplot-by-wrap-object
   "SPC rp" #'funk/ess-baseplot-by-wrap-object
   "C-]" #'funk/ess-xref-find-etags
   ",m" #'funk/add-pipe
   )

  (general-define-key
   :keymaps '(ess-mode-map)
   :states '(motion normal visual)
   :prefix "SPC"
   "rb" #'jupyter-eval-buffer
   "rr" #'jupyter-eval-line-or-region

   "rz" #'jupyter-repl-associate-buffer
   "rZ" #'jupyter-repl-restart-kernel)

  (general-define-key
   :keymaps '(ess-mode-map)
   :states '(visual)
   ",," #'funk/ess-eval-and-recenter
   "SPC SPC" #'funk/ess-eval-and-recenter
   )

  (key-chord-define evil-insert-state-map ",m" 'funk/add-pipe)
  (key-chord-define evil-insert-state-map ",k" 'funk/add-pipe-inline)
  (key-chord-define evil-insert-state-map ",j" 'funk/add-pipe-here)

  ;; fonts
  (setq ess-R-font-lock-keywords
        '((ess-R-fl-keyword:modifiers . t)
          (ess-R-fl-keyword:fun-defs . t)
          (ess-R-fl-keyword:keywords . t)
          (ess-R-fl-keyword:assign-ops . t)
          (ess-R-fl-keyword:constants . t)
          (ess-fl-keyword:fun-calls . t)
          (ess-fl-keyword:numbers . t)
          (ess-fl-keyword:operators . t)
          (ess-fl-keyword:delimiters . t)
          (ess-fl-keyword:= . t)
          (ess-R-fl-keyword:F&T . t)
          (ess-R-fl-keyword:%op% . t)))
  (setq inferior-ess-r-font-lock-keywords
        '((ess-S-fl-keyword:prompt . t)
          (ess-R-fl-keyword:messages . t)
          (ess-R-fl-keyword:modifiers . t)
          (ess-R-fl-keyword:fun-defs . t)
          (ess-R-fl-keyword:keywords . t)
          (ess-R-fl-keyword:assign-ops . t)
          (ess-R-fl-keyword:constants . t)
          (ess-fl-keyword:matrix-labels . t)
          (ess-fl-keyword:fun-calls . t)
          (ess-fl-keyword:numbers . t)
          (ess-fl-keyword:operators . t)
          (ess-fl-keyword:delimiters . t)
          (ess-fl-keyword:= . t)
          (ess-R-fl-keyword:F&T . t)))
  ;; others
  (setq ansi-color-for-comint-mode 'filter)
  (setq comint-move-point-for-output t)
  (setq comint-scroll-to-bottom-on-input t)
  (setq comint-scroll-to-bottom-on-output t)
  (setq ess-ask-for-ess-directory nil)
  (setq ess-directory-function #'projectile-project-root)
  (setq ess-auto-width 'window)
  (setq ess-eval-visibly 'nowait)
  (setq ess-execute-in-process-buffer t)
  (setq ess-history-directory (expand-file-name "ess-history/" no-littering-var-directory))
  (setq ess-indent-with-fancy-comments nil)
  (setq ess-local-process-name "R")
  (setq ess-nuke-trailing-whitespace-p t)
  (setq ess-pdf-viewer-pref "emacsclient")
  (setq ess-plain-first-buffername nil)
  (setq ess-style 'RStudio)
  (setq ess-tab-complete-in-script t)
  (setq ess-use-ido nil)
  (setq ess-write-to-dribble nil)
  (setq inferior-R-args "--no-restore --no-save --quiet")
  (setq inferior-ess-fix-misaligned-output t)
  (setq ess-use-flymake nil)
  ;; package dev
  (setq ess-roxy-str "#'")
  ;; (setq ess-r--company-meta nil)
  (setq ess-roxy-fill-param-p t)
  (setq ess-gen-proc-buffer-name-function 'ess-gen-proc-buffer-name:project-or-directory)

  (define-key ess-mode-map (kbd "C-S-m") 'funk/then_R_operator)
  (define-key ess-mode-map (kbd "M--") 'ess-cycle-assign)
  (define-key ess-mode-map (kbd "M-p") 'funk/add-pipe)
  (define-key ess-mode-map (kbd "C-\\") 'funk/ess-eval-pipe-through-line)
  (define-key ess-mode-map (kbd "C-\|") 'funk/ess-eval-pipe-through-line-and-assign)
  ;; (define-key ess-mode-map (kbd "<f6>") 'funk/ess-eval-and-recenter)
  (define-key ess-mode-map (kbd "C-c C-c") 'funk/ess-eval-and-recenter)

  (define-key inferior-ess-mode-map (kbd "C-S-m") 'funk/add_pipe)
  (define-key inferior-ess-mode-map (kbd "M--") 'ess-cycle-assign)

  (add-hook 'inferior-ess-mode-hook
            #'(lambda()
                (linum-mode 0)
                (local-set-key [C-up] 'comint-previous-input)
                (local-set-key [C-down] 'comint-next-input)))

  (add-hook 'ess-mode-hook
            #'(lambda()
                (local-set-key [(shift return)] 'funk/ess-eval)))

  (add-hook 'ess-post-run-hook 'ess-set-language t)

  (add-hook 'ess-mode-hook #'electric-operator-mode)

  (add-to-list 'auto-mode-alist '("\\^DESCRIPTION$" . ess-r-mode))
  )

(load! "./lisp-local/funk-rstats.el")
(defalias 'qr 'funk/quick-r)

(after! ess-r-mode
        (appendq! +ligatures-extra-symbols
                  '(:assign "⟵"
                            :multiply "×"))
        (set-ligatures! 'ess-r-mode
                        ;; Functional
                        :def "function"
                        ;; Types
                        :null "NULL"
                        :true "TRUE"
                        :false "FALSE"
                        :int "int"
                        :floar "float"
                        :bool "bool"
                        ;; Flow
                        :not "!"
                        :and "&&" :or "||"
                        :for "for"
                        :in "%in%"
                        :return "return"
                        ;; Other
                        :assign "<-"
                        :multiply "%*%"))

;;; RMarkdown via polymode
(use-package! poly-R
              ;; :defer 3
              )

(use-package! poly-markdown
  ;; :defer 3
  )
(use-package! polymode
  ;; :defer 3
  :config
  (add-to-list 'auto-mode-alist '("\\.Rmd" . poly-markdown+r-mode))
  (add-to-list 'auto-mode-alist '("\\.rmd" . poly-markdown+r-mode))
  (define-key polymode-mode-map "\M-n s" 'funk/ess-rmarkdown))

;;; ess-view-data
(use-package! ess-view-data
  ;; :defer 5
  :config
  (setq ess-view-data-read-string #'ivy-completing-read)
  (evil-define-key 'normal ess-view-data-mode-map
    (kbd "q") 'popper-kill-latest-popup)

  (general-define-key
   :keymaps '(ess-view-data-mode-map)
   :states '(normal)
   "C-j" #'ess-view-data-goto-next-page
   "C-k" #'ess-view-data-goto-previous-page)

  (global-set-key (kbd "C-c v") #'ess-view-data-print))

(provide 'init-rstats)
;;; init-rstats.el ends here
