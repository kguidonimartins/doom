;;; init-projectile.el --- Projectile configuration -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; projectile
(use-package! projectile
  :init
  (projectile-mode)
  :config
  (global-set-key (kbd "<f5>") 'projectile-compile-project)
  ;; If you want use a different command to compile your project,
  ;; create a `.dir-locals.el' file and the following inside it:
  ;; ((nil .
  ;;       ((projectile-project-compilation-cmd . "make all"))))

  (add-to-list 'projectile-globally-ignored-directories (expand-file-name "elpa" user-emacs-directory))
  (add-to-list 'projectile-globally-ignored-directories "./elpa")
  (add-to-list 'projectile-globally-ignored-directories "elpa")
  ;; (add-to-list 'projectile-globally-ignored-directories '("node_modules" "straight" "elpa"))
  (setq projectile-completion-system 'ivy)
  (setq projectile-switch-project-action #'projectile-dired)
  (setq projectile-known-projects-file "~/.doom.d/etc/projectile/projectile-bookmarks.el")
  (setq projectile-git-submodule-command nil) ;; https://github.com/bbatsov/projectile/issues/1302#issuecomment-433894379
  (setq projectile-enable-caching t)
  (setq projectile-remember-window-configs t)
  (setq-default projectile-mode-line-prefix " ")
  (setq projectile-auto-discover nil)
  (setq projectile-mode-line-function '(lambda () (format " [%s]" (projectile-project-name))))
  (setq projectile-project-search-path '(("~/google-drive/kguidonimartins/git-repos" . 4)
                                         ("~/google-drive/fbds1992" . 4)))
  (add-hook 'find-file-hook
            (lambda ()
              (when (file-remote-p default-directory)
                (setq-local projectile-mode-line "Projectile")))))

;;; ibuffer-projectile
(use-package! ibuffer-projectile
  :after projectile
  ;; :defer 5
  :config
  (setq ibuffer-projectile-prefix "")
  (add-hook 'ibuffer-hook
            (lambda ()
              (ibuffer-projectile-set-filter-groups)
              (unless (eq ibuffer-sorting-mode 'alphabetic)
                (ibuffer-do-sort-by-alphabetic)))))

;;; counsel-projectile
(use-package! counsel-projectile
  :after (counsel projectile)
  :config

  (setq counsel-switch-buffer-preview-virtual-buffers nil)
  ;; change the defaut action for counsel-projectile:
  ;; don't ask anything, just jump to dired
  ;; https://github.com/ericdanan/counsel-projectile/issues/62#issuecomment-353732566
  (counsel-projectile-modify-action
   'counsel-projectile-switch-project-action
   '((move counsel-projectile-switch-project-action-dired 1)
     (setkey counsel-projectile-switch-project-action-dired "o")
     (setkey counsel-projectile-switch-project-action " ")))
  (counsel-projectile-mode)

  (defun counsel-projectile-switch-project-open-project (&optional default-action)
    "Switch project.

Optional argument DEFAULT-ACTION is the key, function, name, or
index in the list `counsel-projectile-switch-project-action' (1
for the first action, etc) of the action to set as default."
    (interactive)
    (ivy-read (projectile-prepend-project-name "Switch to project: ")
              (if counsel-projectile-remove-current-project
                  (projectile-relevant-known-projects)
                (projectile-relevant-open-projects))
              :preselect (and (projectile-project-p)
                              (abbreviate-file-name (projectile-project-root)))
              :action (or (and default-action
                               (listp counsel-projectile-switch-project-action)
                               (integerp (car counsel-projectile-switch-project-action))
                               (cons (counsel-projectile--action-index
                                      default-action
                                      counsel-projectile-switch-project-action)
                                     (cdr counsel-projectile-switch-project-action)))
                          counsel-projectile-switch-project-action)
              :require-match t
              :sort counsel-projectile-sort-projects
              :caller 'counsel-projectile-switch-project)))

(provide 'init-projectile)
;;; init-projectile.el ends here
