;;; init-online-search.el --- Packages for searching things online -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; counsel-surfraw
;; :defer 5
(after! (ivy counsel)
  (load! "~/.doom.d/lisp/github/counsel-surfraw/counsel-surfraw.el")

  (defun funk/counsel-surfraw ()
    "Search for something online, using the surfraw command."
    (interactive)
    (setq-local thing (if (use-region-p)
                          (buffer-substring-no-properties
                           (region-beginning) (region-end))
                        (thing-at-point 'symbol)))
    (let ((search-for (read-string "Search for: " thing 'counsel-surfraw-search-history)))
      (ivy-read (format "Search for `%s` with: " search-for)
                #'counsel-surfraw-elvi
                :require-match t
                :history 'counsel-surfraw-engine-history
                :sort t
                :action
                (lambda (selected-elvis)
                  (browse-url (shell-command-to-string
                               (format "sr %s -p %s"
                                       (get-text-property 0 'elvis selected-elvis)
                                       search-for)))))))
  )

;;; 0x0: easily upload files
(use-package! 0x0)

(provide 'init-online-search)
;;; init-online-search.el ends here
