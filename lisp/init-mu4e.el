;;; init-mu4e.el --- mu4e configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(defun me()
  (interactive)
  (insert "Karlo Guidoni <karloguidoni2@gmail.com>"))

;; Via =mu4e=.

;; ** Installing =mu=

;; Refer to
;; [[https://github.com/daviwil/emacs-from-scratch/blob/master/show-notes/Emacs-Mail-01.org][this]]
;; documentation to get =mu4e= working.

;; In Arch Linux, you can get =mu= via =yay=.

;; #+begin_src shell
;; yay -S mu
;; #+end_src

;; After install =mu=, initialize with (after any =mu= update too):

;; #+begin_src shell
;; mu init --maildir=~/.local/share/mail/kguidonimartins --my-address=kguidonimartins@gmail.com
;; #+end_src

;; If you want setup two or more accounts, pass the basename for the maildir then list all email:
;; #+begin_src shell
;; mu init --maildir=~/.local/share/mail/kguidonimartins --my-address=kguidonimartins@gmail.com --my-address=karloguidoni2@gmail.com
;; #+end_src

;; when the following error occur:
;; error: failed to open store @ /home/karlo/.cache/mu/xapian: Unable to get write lock on /home/karlo/.cache/mu/xapian: already locked
;; just run:
;; pkill -2 -u $UID mu
;; then:
;; mu init --maildir=~/.local/share/mail/kguidonimartins --my-address=kguidonimartins@gmail.com
;; then:
;; mu index

;; ** Authentication

;; Also, because the two factor authentication, use
;; [[[https://myaccount.google.com/apppasswords][=apps password=]]
;; when send the first email.

;;; mu4e config


(use-package mu4e
  ;; :defer 10
  :if (not (in-slow-ssh))
  :ensure nil
  :config
  (require 'mu4e)
  (require 'mu4e-contrib) ; just for `mu4e-headers-mark-all-unread-read' (mark as read all unread messages).

  (general-define-key
   :keymaps '(mu4e-main-mode-map)
   :states '(normal)
   "j" #'mu4e~headers-jump-to-maildir)

  (general-define-key
   :keymaps '(mu4e-view-mode-map)
   :states '(normal)
   "SPC" nil)

  mu4e-view-mode-map

  ;; This is set to 't' to avoid mail syncing issues when using mbsync
  (setq mu4e-change-filenames-when-moving t)

  ;; Refresh mail using isync every 10 minutes
  (setq mu4e-update-interval (* 10 60))
  (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-maildir "~/.local/share/mail")
  (setq mu4e-index-update-in-background t)
  (setq mu4e-headers-auto-update t)
  (setq mu4e-hide-index-messages t)


  ;; Make sure plain text mails flow correctly for recipients
  (setq mu4e-compose-format-flowed t)

  ;; Configure the function to use for sending mail
  (setq message-send-mail-function 'smtpmail-send-it)

  ;; NOTE: Only use this if you have set up a GPG key!
  ;; Automatically sign all outgoing mails
  ;; (add-hook 'message-send-hook 'mml-secure-message-sign-pgpmime)

  (setq mu4e-contexts
        `(
          ;; University/Work account
          ,(make-mu4e-context
            :name "Work"
            :enter-func (lambda () (mu4e-message "Entering Work context"))
            :leave-func (lambda () (mu4e-message "Leaving Work context"))
            :match-func
            (lambda (msg)
              (when msg
                (string-match-p "^/kguidonimartins" (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address      . "kguidonimartins@gmail.com")
                    (user-full-name         . "Karlo Guidoni Martins")
                    (smtpmail-smtp-server   . "smtp.gmail.com")
                    (smtpmail-smtp-service  . 465)
                    (smtpmail-stream-type   . ssl)
                    (mu4e-compose-signature . (concat "Karlo Guidoni\n"))
                    (mu4e-drafts-folder     . "/kguidonimartins/[Gmail].Drafts")
                    (mu4e-sent-folder       . "/kguidonimartins/[Gmail].Sent Mail")
                    (mu4e-refile-folder     . "/kguidonimartins/[Gmail].All Mail")
                    (mu4e-trash-folder      . "/kguidonimartins/[Gmail].Trash")))
          ;; New account for 2022: migrating all services
          ,(make-mu4e-context
            :name "New"
            :enter-func (lambda () (mu4e-message "Entering Personal context"))
            :leave-func (lambda () (mu4e-message "Leaving Personal context"))
            :match-func
            (lambda (msg)
              (when msg
                (string-match-p "^/karloguidoni2" (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address      . "karloguidoni2@gmail.com")
                    (user-full-name         . "Karlo Guidoni")
                    (smtpmail-smtp-server   . "smtp.gmail.com")
                    (smtpmail-smtp-service  . 465)
                    (smtpmail-stream-type   . ssl)
                    (mu4e-compose-signature . (concat "Karlo Guidoni\n"))
                    (mu4e-drafts-folder     . "/karloguidoni2/[Gmail].Drafts")
                    (mu4e-sent-folder       . "/karloguidoni2/[Gmail].Sent Mail")
                    (mu4e-refile-folder     . "/karloguidoni2/[Gmail].All Mail")
                    (mu4e-trash-folder      . "/karloguidoni2/[Gmail].Trash")))
          )
        )

  ;; select the first context without asking
  (setq mu4e-context-policy 'pick-first)
  (eval-after-load 'mu4e #'(lambda() (mu4e 0)))

  (setq mu4e-maildir-shortcuts
        '(("/kguidonimartins/INBOX"             . ?i)
          ("/kguidonimartins/[Gmail].Sent Mail" . ?s)
          ("/kguidonimartins/[Gmail].Trash"     . ?t)
          ("/kguidonimartins/[Gmail].Drafts"    . ?d)
          ("/kguidonimartins/[Gmail].All Mail"  . ?a)
          ("/karloguidoni2/INBOX"               . ?I)
          ("/karloguidoni2/[Gmail].Sent Mail"   . ?S)
          ("/karloguidoni2/[Gmail].Trash"       . ?T)
          ("/karloguidoni2/[Gmail].Drafts"      . ?D)
          ("/karloguidoni2/[Gmail].All Mail"    . ?A)))

  (defun funk/fix-mu4e-display ()
    (interactive)
    (progn
      (visual-line-mode -1)
      (toggle-truncate-lines +1)))

  (add-hook 'mu4e-headers-mode-hook 'funk/fix-mu4e-display)

  (add-to-list 'mu4e-header-info-custom
               '(:empty . (:name "Empty"
                                 :shortname ""
                                 :function (lambda (msg) "  "))))

  (setq mu4e-headers-fields '((:empty         .    2)
                              (:flags         .    6)
                              (:human-date    .   12)
                              (:empty         .    2)
                              (:mailing-list  .   12)
                              (:empty         .    2)
                              (:from          .   27)
                              (:to            .   27)
                              (:empty         .    2)
                              (:subject       .   110)))

  (setq
   mu4e-use-fancy-chars t
   mu4e~headers-mode-line-label "~~mu4e~~"
   mu4e-headers-date-format "%Y/%m/%d"
   mu4e-headers-include-related t
   mu4e-headers-skip-duplicates t
   mu4e-headers-thread-child-prefix '("L " . "│ ")
   mu4e-headers-thread-connection-prefix '("| " . "│ ")
   mu4e-headers-thread-duplicate-prefix '("= " . "≡ ")
   mu4e-headers-thread-first-child-prefix '("L " . "⚬ ")
   mu4e-headers-thread-last-child-prefix '("L " . "└ ")
   mu4e-html2text-command "w3m -dump -T text/html -cols 80 -o display_link_number=true -o auto_image=false -o display_image=false -o ignore_null_img_alt=true"
   mu4e-view-prefer-html t
   )

  ;; rewrite the original mode-line for mu4e-header
  (defun mu4e~headers-update-mode-line ()
    "Update mode-line settings."
    (let* ((flagstr "")
           (name "mu4e-headers"))

      (setq mode-name name)
      (setq mu4e~headers-mode-line-label (concat flagstr " " mu4e~headers-last-query))

      (make-local-variable 'global-mode-string)

      (add-to-list 'global-mode-string
                   `(:eval
                     (concat
                      (propertize
                       (mu4e~quote-for-modeline ,mu4e~headers-mode-line-label)
                       'face 'mu4e-modeline-face)
                      " "
                      (if (and mu4e-display-update-status-in-modeline
                               (buffer-live-p mu4e~update-buffer)
                               (process-live-p (get-buffer-process
                                                mu4e~update-buffer)))
                          (propertize " (updating)" 'face 'mu4e-modeline-face)
                        ""))))))


  )

;;; org-mime and htmlize

;; Composing with =org-mode=:

;; =M-x org-mime-edit-mail-in-org-mode=

;; Check
;; [[https://github.com/daviwil/emacs-from-scratch/blob/master/show-notes/Emacs-Mail-04.org#composing-org-mail-comfortably][this]]
;; out.

;; - =org-mime-org-buffer-htmlize= - Send an entire org-mode buffer as an e-mail message
;; - =org-mime-org-subtree-htmlize= - Send a subtree of the current org-mode buffer as an e-mail message
;; composing with org mode
(use-package org-mime
  ;; :defer 5
  )

(use-package htmlize
  ;; :defer 5
  :config
  (setq org-mime-export-options '(:section-numbers nil
                                                   :with-author nil
                                                   :with-toc nil))
  ;; (add-hook 'message-send-hook 'org-mime-htmlize)
  ;; (add-hook 'message-send-hook 'org-mime-confirm-when-no-multipart)
  )


;;; mu4e-marker-icons

(use-package mu4e-marker-icons
  ;; :defer 5
  :init (mu4e-marker-icons-mode 1)
  :config
  (setq mu4e-headers-unread-mark    '("u" . " "))
  (setq mu4e-headers-draft-mark     '("D" . " "))
  (setq mu4e-headers-flagged-mark   '("F" . " "))
  (setq mu4e-headers-new-mark       '("N" . " "))
  (setq mu4e-headers-passed-mark    '("P" . " "))
  (setq mu4e-headers-replied-mark   '("R" . " "))
  (setq mu4e-headers-seen-mark      '("S" . " "))
  (setq mu4e-headers-trashed-mark   '("T" . " "))
  (setq mu4e-headers-attach-mark    '("a" . " "))
  (setq mu4e-headers-encrypted-mark '("x" . " "))
  (setq mu4e-headers-signed-mark    '("s" . " "))
  (setq mu4e-marker-icons-use-unicode nil))

;;; notmuch
(use-package notmuch
  ;; :defer 5
  :config
  (setq notmuch-search-oldest-first nil))

(provide 'init-mu4e)
;;; init-mu4e.el ends here
