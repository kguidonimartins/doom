;;; init-fonts.el --- font configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;;; default-text-scale
(use-package! default-text-scale
  ;; :defer 5
  )

;;; general fonts
(defvar funk/default-font-size 100)
(defvar funk/default-font-type "Fira Code Nerd Font")

  ;;;###autoload
(defun funk/general-font-setup()
  (interactive)
  (set-face-attribute 'default nil :height funk/default-font-size :weight 'regular)
  (set-face-attribute 'fixed-pitch nil :height funk/default-font-size)
  (set-face-attribute 'variable-pitch nil :height funk/default-font-size :weight 'regular)
  (message "Set font size to %s" funk/default-font-size))

(funk/general-font-setup)

;;; font for daemon

  ;;;###autoload
(defun funk/set-font-faces ()
  (set-face-attribute 'default nil :font "Fira Code Nerd Font" :height funk/default-font-size)
  (set-face-attribute 'fixed-pitch nil :font "Fira Code Nerd Font" :height funk/default-font-size)
  (set-face-attribute 'variable-pitch nil :font "Fira Code Nerd Font" :height funk/default-font-size :weight 'regular))

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (setq doom-modeline-icon t)
                (with-selected-frame frame
                  (funk/set-font-faces))))
  (funk/set-font-faces))
;; (server-start)
;; (require 'org-protocol)

;;; ligatures
;; SOURCE: https://alpha2phi.medium.com/ligature-fonts-for-terminal-vs-code-neovim-and-emacs-1187c6987491
(use-package! ligature
  ;; :defer 2
  :load-path "~/.doom.d/lisp/github/ligature/"
  ;; :straight (ligature
  ;;            :type git
  ;;            :host github
  ;;            :branch "master"
  ;;            :repo "mickeynp/ligature.el")
  :config
  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  ;; Enable all Cascadia Code ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "~~>" "***" "||=" "||>"
                                       ":::" "::=" "=:=" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" "/*" "/=" "/>" "//" "(*" "*)"
                                       ))
  (ligature-set-ligatures 'markdown-mode '("|||>" "<|||" "<==>" "~~>" "***" "||=" "||>"
                                           ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                           "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-<<"
                                           "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                           "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "#_(" "..<"
                                           "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~@" "~="
                                           "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                           "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" ">:"
                                           ">=" ">>" ">-" "-~" "-|" "-<" "<~" "<*" "<|" "<:"
                                           "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                           "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                           "?=" "?." "??" "/*" "/=" "/>" "//" "(*" "*)"
                                           ))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))

;;; pretty symbols
;; (defun funk/pretty-symbols ()
;;   (prettify-symbols-mode +1)
;;   (setq prettify-symbols-alist
;;         '(("lambda" . 955)
;;           ("map" . 8614)
;;           ("%>%" . ?▷)
;;           ("function" . ?ƒ)
;;           ("defun" . ?ƒ))))
;; (add-hook 'emacs-lisp-mode-hook 'funk/pretty-symbols)
;; (add-hook 'clojure-mode-hook 'funk/pretty-symbols)
;; (add-hook 'ess-mode-hook 'funk/pretty-symbols)


(provide 'init-fonts)
;;; init-fonts.el ends here
