;;; init-tree-sitter.el --- tree-sitter config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'tree-sitter)
(straight-use-package 'tree-sitter-langs)

(require 'tree-sitter)
(require 'tree-sitter-langs)

;; make sure I do not re-add the submodule multiple times
(unless (f-exists-p (concat tree-sitter-langs-git-dir "/repos/r"))
  (let ((default-directory tree-sitter-langs-git-dir))
    (tree-sitter-langs--call
     "git" "submodule" "add" "https://github.com/r-lib/tree-sitter-r" "repos/r")))

;; compile only if haven't done so
(unless (--filter (string= (f-base it) "r") (f-entries (tree-sitter-langs--bin-dir)))
  ;; do not display compile information in a new buffer
  (cl-letf (((symbol-function 'tree-sitter-langs--buffer) (lambda (&rest _) nil)))
    (tree-sitter-langs-compile 'r)))

(unless (assq 'ess-r-mode tree-sitter-major-mode-language-alist)
  (add-to-list 'tree-sitter-major-mode-language-alist '(ess-r-mode . r)))

(defun funk/tree-sitter-load-r ()
  ;; register ess-r-mode with r grammar
  (unless (assq 'ess-r-mode tree-sitter-major-mode-language-alist)
    (add-to-list 'tree-sitter-major-mode-language-alist '(ess-r-mode . r)))

  ;; register tree-sitter
  (tree-sitter-require 'r))


(add-hook 'ess-r-mode 'funk/tree-sitter-load-r)

(use-package evil-textobj-tree-sitter
  :straight t
  :config
  ;; bind `function.outer`(entire function block) to `f` for use in things like `vaf`, `yaf`
  (define-key evil-outer-text-objects-map "f" (evil-textobj-tree-sitter-get-textobj "function.outer"))
  ;; bind `function.inner`(function block without name and args) to `f` for use in things like `vif`, `yif`
  (define-key evil-inner-text-objects-map "f" (evil-textobj-tree-sitter-get-textobj "function.inner"))
  (add-to-list 'evil-textobj-tree-sitter-major-mode-language-alist '(ess-r-mode . r)))

;; ;;; tree-sitter for R
;; https://github.com/junyi-hou/tree-sitter-fold
;; https://github.com/junyi-hou/dotfiles/blob/main/main.org#rtree-sitter

;; (use-package tree-sitter-ess-r
;;   :straight (:host github :repo "ShuguangSun/tree-sitter-ess-r"))

(provide 'init-tree-sitter)
;;; init-tree-sitter.el ends here
