;;; init-jupyter.el --- jupyter config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; jupyter
;; Setup jupyter for org-mode
;; https://sqrtminusone.xyz/posts/2021-05-01-org-python/
;; https://www.reddit.com/r/Julia/comments/cyhhok/anyone_use_emacs_for_julia_development/
;; https://github.com/nnicandro/emacs-jupyter/issues/253

;; NOTE: Workflow based on python, conda, and jupyter
;; CHECK: https://github.com/kguidonimartins/dsml-py-mx
;; 0. Setting up the environment
;;   0.a install miniconda or anaconda
;;   0.b conda create --name py39 python=3.9
;;   0.c conda activate py39
;;   0.d pip install --user ipykernel
;;   0.e python -m ipykernel install --user --name=py39
;;   0.f conda install -n py39 ipython jupyter matplotlib numpy pandas pylint seaborn
;;   0.g conda install -n py39 -c conda-forge tabula-py
;;   0.h conda install -n py39 scikit-learn
;;   0.i conda install -n py39 statsmodels
;;   0.j conda env export --name py39 | grep -v "^prefix: " > environment.yml
;; 1. Create `.dir.locals.el' with the following content inside the project:
;;   ((nil . ((eval . (conda-env-activate "py39")))))
;; 2. Access the project folder, and run `jupyter-run-repl'
;; 3. Make a minimal check up with:
;;   import sys
;;   sys.executable
;;   !conda info

(use-package jupyter
  ;; ## this is for R
  ;; ## on the top of the R script
  ;; options(
  ;;         repr.plot.width=10,
  ;;         repr.plot.height=10,
  ;;         repr.matrix.max.rows = 10L,
  ;;         repr.matrix.max.cols = 5L
  ;;         )
  ;; ## this is for python
  ;; ## include the following in the file:
  ;; ## .config/jupyter/profile_default/ipython_kernel_config.py
  ;; c.InlineBackend.rc = { 'figure.figsize': (20.0, 10.0) }
  :config
  (setq jupyter-repl-prompt-margin-width 4)
  (setq jupyter-repl-echo-eval-p t)
  (setq jupyter-repl-maximum-size 12000)

  (defun jupyter-command-venv (&rest args)
    "This overrides jupyter-command to use the virtualenv's jupyter."
    (let ((jupyter-executable (executable-find "jupyter")))
      (with-temp-buffer
        (when (zerop (apply #'process-file jupyter-executable nil t nil args))
          (string-trim-right (buffer-string))))))
  (advice-add 'jupyter-command :override #'jupyter-command-venv)

  (defun funk/jupyter--pop-repl (&rest _)
    "Pop repl buffer, then go back to the code buffer."
    (interactive)
    (let* ((code-buffer (current-buffer)))
      (jupyter-repl-pop-to-buffer)
      (switch-to-buffer-other-window code-buffer)))

  (advice-add #'jupyter-eval-line-or-region :before #'funk/jupyter--pop-repl)

  (defun funk/jupyter--deactivate-mark (&rest _)
    "Deactivate mark, use &rest to satisfies the number of arguments."
    (deactivate-mark))

  (advice-add #'jupyter-eval-region :after #'funk/jupyter--deactivate-mark)

  (defun funk/jupyter-refresh-kernelspecs ()
    "Refresh Jupyter kernelspecs."
    (interactive)
    (jupyter-available-kernelspecs t))

  (defvar funk--space-str " \t\n")

  (defun funk/mark-paragraph ()
    "Marks one paragraph."
    (interactive)
    (mark-paragraph)
    (exchange-point-and-mark)
    (skip-chars-backward funk--space-str)
    (exchange-point-and-mark)
    (skip-chars-forward funk--space-str))

  (defun funk/jupyter-eval-paragraph-and-step ()
    "Eval a python paragraph and step."
    (interactive)
    (funk/mark-paragraph)
    (let ((beg (region-beginning))
          (end (region-end)))
      (pulse-momentary-highlight-region beg end)
      (jupyter-eval-region beg end)
      (deactivate-mark)
      (end-of-paragraph-text)
      (python-nav-forward-statement)
      (recenter)))

  (defun funk/jupyter-eval-region-or-line (&optional stay)
    "Eval a python paragraph and step."
    (interactive)
    (let ((beg-line (line-beginning-position))
          (end-line (line-end-position))
          (beg-region (region-beginning))
          (end-region (region-end)))
      (if (region-active-p)
          (progn
            (pulse-momentary-highlight-region beg-region end-region)
            (jupyter-eval-region beg-region end-region))
        (progn
          (pulse-momentary-highlight-region beg-line end-line)
          (jupyter-eval-region beg-line end-line)))
      (when (not stay)
        (python-nav-forward-statement))))

  (defun funk/jupyter-eval-region-or-line-and-stay ()
    (interactive)
    (funk/jupyter-eval-region-or-line t))

  (general-define-key
   :keymaps '(jupyter-repl-interaction-mode-map)
   :states '(normal)
   ",," #'funk/jupyter-eval-paragraph-and-step
   "SPC SPC" #'funk/jupyter-eval-paragraph-and-step)

  (define-key jupyter-repl-interaction-mode-map (kbd "C-c C-c") 'funk/jupyter-eval-paragraph-and-step)
  ;; (advice-add #'funk/jupyter-eval-paragraph-and-step :before #'funk/jupyter--pop-repl)
  (define-key jupyter-repl-interaction-mode-map (kbd "C-<return>") 'funk/jupyter-eval-region-or-line)
  (advice-add #'funk/jupyter-eval-region-or-line :before #'funk/jupyter--pop-repl)
  (define-key jupyter-repl-interaction-mode-map (kbd "S-<return>") 'funk/jupyter-eval-region-or-line-and-stay)
  ;; (advice-add #'funk/jupyter-eval-region-or-line-and-stay :before #'funk/jupyter--pop-repl)
  (define-key python-mode-map (kbd "C-c C-p") 'jupyter-run-repl))

(provide 'init-jupyter)
;;; init-jupyter.el ends here
