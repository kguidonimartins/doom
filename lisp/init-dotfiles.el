;;; init-dotfiles.el --- dotfiles configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; Quickly find my dotfiles from anywhere
(use-package emacs
  :ensure nil
  :config

  (defvar funk/counsel-git-cmd-dotfiles "git --git-dir='/home/karlo/dotfiles-public/' --work-tree='/home/karlo/' ls-files -z --full-name --"
    "Command for `counsel-git'.")

  (defun funk/counsel-git-cands-dotfiles (dir)
    (let ((default-directory dir))
      (split-string
       (shell-command-to-string funk/counsel-git-cmd-dotfiles)
       "\0"
       t)))

  (defun funk/counsel-git-dotfiles-public (&optional initial-input)
    "Quick find my public dotfiles (bare repository) from anywhere."
    (interactive)
    (counsel-require-program funk/counsel-git-cmd-dotfiles)
    (let ((default-directory "/home/karlo"))
      (ivy-read "Find file: " (funk/counsel-git-cands-dotfiles default-directory)
                :initial-input initial-input
                :action #'counsel-git-action
                :caller 'counsel-git)))

  (defvar funk/counsel-git-cmd-emacs "git ls-files -z --full-name -- /home/karlo/.emacs.d.vanilla/"
    "Command for `counsel-git'.")

  (defun funk/counsel-git-cands-emacs (dir)
    (let ((default-directory dir))
      (split-string
       (shell-command-to-string funk/counsel-git-cmd-emacs)
       "\0"
       t)))

  (defun funk/counsel-git-emacs (&optional initial-input)
    "Quick find my emacs files from anywhere."
    (interactive)
    (counsel-require-program funk/counsel-git-cmd-emacs)
    (let ((default-directory "/home/karlo/.emacs.d.vanilla/"))
      (ivy-read "Find file: " (funk/counsel-git-cands-emacs default-directory)
                :initial-input initial-input
                :action #'counsel-git-action
                :caller 'counsel-git))))


(provide 'init-dotfiles)
;;; init-dotfiles.el ends here
