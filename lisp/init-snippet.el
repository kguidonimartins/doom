;;; init-snippet.el --- yasnippet configuration -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; yasnippet

;; (add-to-list 'warning-suppress-types '(yasnippet backquote-change))
(use-package! yasnippet
  ;; :defer 5
  :config
  (yas-global-mode 1))

;;; yasnippet-snippets
(use-package! yasnippet-snippets
  ;; :defer 5
  :config
  (defun funk/yasnippets-snippets-initialize ()
    (let ((snip-dir  "~/.doom.d/etc/yasnippet/snippets"))
      (when (boundp 'yas-snippet-dirs)
        (add-to-list 'yas-snippet-dirs snip-dir t))
      (yas-load-directory snip-dir)))
  (funk/yasnippets-snippets-initialize)
  (add-to-list 'warning-suppress-types '(yasnippet backquote-change)))

(provide 'init-snippet)
;;; init-snippet.el ends here
