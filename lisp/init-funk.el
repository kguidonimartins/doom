;;; -*- lexical-binding: t; -*-

(setq force-load-messages nil)

(defun funk-auth (key)
  (with-temp-buffer
    (insert-file-contents-literally "~/.config/api/emacs-keys.el")
    (alist-get key (read (current-buffer)))))

(defun visit-initel ()
  (interactive)
  (find-file (expand-file-name "init.el" user-emacs-directory)))

(defun reload-initel ()
  (interactive)
  (load-file (expand-file-name "init.el" user-emacs-directory)))

(defun getel-files (path)
  "Get *.el files in a custom path.
ARG define the PATH to the *.el files."
  (let* ((path (expand-file-name path user-emacs-directory))
         (local-pkgs (mapcar 'file-name-directory (directory-files-recursively path ".*\\.el"))))
    (if (file-accessible-directory-p path)
        (mapc (apply-partially 'add-to-list 'load-path) local-pkgs)
      (make-directory path :parents))))

(defun get-theme-files (path)
  "Get *.el files in a custom path.
ARG define the PATH to the *.el files."
  (let* ((path (expand-file-name path user-emacs-directory))
         (local-pkgs (mapcar 'file-name-directory (directory-files-recursively path ".*\\.el"))))
    (if (file-accessible-directory-p path)
        (mapc (apply-partially 'add-to-list 'custom-theme-load-path) local-pkgs)
      (make-directory path :parents))))

(defun pretty-print-hash (hashtable)
  "Prints the hashtable, each line is key, val"
  (maphash
   (lambda (k v)
     (princ (format "%s: %s" v k))
     (princ "\n"))
   hashtable
   ))

(setq report-table-time-to-load-layers (make-hash-table :test 'equal))

(defun format-loaded-time (beg-time end-time)
  (format "%.2f" (float-time (time-subtract end-time beg-time))))

(defun require-layers (funk-layer-list)
  "Run `require' for each layer of the user pre-defined layer list: FUNK-LAYER-LIST.

  NOTE 2022-01-09: Maybe!
;; at setup-consult.el
(use-package consult
  :ensure t ; <- installs consult if necessary
  :config
  ;; Consult configuration
  )

(provide 'setup-consult)

;; at init.el
(use-package setup-consult  ; <- Configure/load the above file
  :config
  ;; Additional consult configuration

  ;; Configuration to integrate with other packages:
  (use-package embark-consult  ; <- Load after setup-consult, only if installed
    :defer
    :config
    ;; More configuration
    ))"
  ;; (dolist (layer funk-layer-list)
  ;;   (message "=> Loading layer %s (%s)" layer (length funk-layer-list))
  ;;   (require layer))
  (setq report-time-to-load-layers nil)
  (cl-loop for index-layer from 1
           for name-layer in funk-layer-list
           do (setq beg-time (current-time))
           do (message "====================================================================> (%s/%s) Loading layer %s" index-layer (length funk/layers) name-layer)
           do (require name-layer)
           (setq end-time (current-time))
           do (message "[INFO] %s loaded in %s" name-layer (format-loaded-time beg-time end-time))
           do (puthash name-layer (format-loaded-time beg-time end-time) report-table-time-to-load-layers)
           )
  )



(defun funk/remove-unwanted-buffers ()
  (setq unwanted-buffers '("*scratch*"
                           "*ESS*"
                           "*pdf-scroll-log*"
                           "*straight-process*"
                           "*quelpa-build-checkout*"
                           ))
  (dolist (buf unwanted-buffers)
    (if (get-buffer buf)
        (kill-buffer buf))))

;; source: https://emacs.stackexchange.com/a/14676/31478
(defun y-or-n-p-test ()
  (message (if (y-or-n-p "is this true?")
               "yes!"
             "no!")))

(defun auto-yes (old-fun &rest args)
  (cl-letf (((symbol-function 'y-or-n-p) (lambda (prompt) t))
            ((symbol-function 'yes-or-no-p) (lambda (prompt) t)))
    (apply old-fun args)))

(defun auto-no (old-fun &rest args)
  (cl-letf (((symbol-function 'y-or-n-p) (lambda (prompt) nil))
            ((symbol-function 'yes-or-no-p) (lambda (prompt) nil)))
    (apply old-fun args)))

(advice-add #'y-or-n-p-test :around #'auto-yes)
