;;; init-ivy.el --- ivy/counsel/swiper configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; avy
(use-package! avy)

;;; ivy

(use-package! ivy
  :init
  (ivy-mode 1)

  :bind (("C-s" . swiper-all) ; check swiper-all-thing-at-point too!
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         ("C-u" . ivy-avy)
         ("C-o" . ivy-dispatching-done)
         ("C-t" . ivy-mark)
         ("C-<return>" . ivy-immediate-done)
         ("C-f" . ivy-scroll-up-command)
         ("C-b" . ivy-scroll-down-command)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         ("C-u" . ivy-avy)
         ("C-t" . ivy-mark)
         ("C-<return>" . ivy-immediate-done)
         ("C-f" . ivy-scroll-up-command)
         ("C-b" . ivy-scroll-down-command)
         :map ivy-reverse-i-search-map
         ("C-u" . ivy-avy)
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill)
         ("C-t" . ivy-mark)
         ("C-f" . ivy-scroll-up-command)
         ("C-b" . ivy-scroll-down-command))

  :config
  ;; Highlight the selected item
  (defun my-ivy-format-function (cands)
    "Transform CANDS into a string for minibuffer."
    (if (display-graphic-p)
        (ivy-format-function-line cands)
      (ivy-format-function-arrow cands)))
  (setf (alist-get 't ivy-format-functions-alist) #'my-ivy-format-function)

  (setq ivy-use-virtual-buffers t
        ivy-format-function #'ivy-format-function-arrow)

  (custom-set-faces
   '(ivy-org ((t (:height 1.00))))
   '(ivy-current-match ((t (:weight bold))))
   '(ivy-minibuffer-match-face-1 ((t (:weight bold))))
   '(ivy-minibuffer-match-face-2 ((t (:weight bold))))
   '(ivy-minibuffer-match-face-3 ((t (:weight bold))))
   '(ivy-minibuffer-match-face-4 ((t (:weight bold))))
   '(ivy-match-required-face ((t (:weight bold))))
   )

  ;; https://github.com/abo-abo/swiper/issues/2196
  ;; you can set .dir-locals.el to change counsel-file-jump-args
  (setq find-program "fd")
  (setq counsel-file-jump-args (split-string "--type f --hidden --follow --exclude .git ."))
  (setq counsel-dired-jump-args (split-string "--type d --hidden --follow --exclude .git --no-ignore ."))
  (setq ivy-display-style 'fancy)
  (setq ivy-use-selectable-prompt t)
  (setq ivy-count-format "(%d/%d) ")

  ;; irrelevant because the next command
  (setq ivy-height 20)
  ;; set ivy-frame based on a `n' (number) factor
  ;; (setq ivy-height-alist '((t lambda (_caller) (/ (window-height) 2))))
  (setq ivy-use-virtual-buffers nil) ;; this remove recentf from counsel-switch-buffer
  (setq ivy-wrap nil)
  (setq ivy-fixed-height-minibuffer t)
  (setq ivy-re-builders-alist
        '((swiper . ivy--regex-plus)
          (t      . ivy--regex-plus)))

  (ivy-add-actions 'counsel-find-file
                   '(
                     ("I" (lambda (x)
                            (with-ivy-window (insert x)))
                      "insert")

                     ("F" (lambda (x)
                            (with-ivy-window (insert (file-relative-name x))))
                      "insert relative file name")

                     ("B" (lambda (x)
                            (with-ivy-window
                              (insert (file-name-nondirectory (replace-regexp-in-string "/\\'" "" x)))))
                      "insert file name without any directory information")
                     )
                   )

  (ivy-add-actions 'ivy-switch-buffer
                   '(
                     ("l" (lambda (x)
                            (split-window-right)
                            (other-window 1 nil)
                            (switch-to-buffer x))
                      "open in a new window on right")
                     ("j" (lambda (x)
                            (split-window-below)
                            (other-window 1 nil)
                            (switch-to-buffer x))
                      "open in a new window below")
                     ))

  (ivy-add-actions 'counsel-dired-jump
                   '(
                     ("l" (lambda (x)
                            (split-window-right)
                            (other-window 1 nil)
                            (dired-jump nil x))
                      "open in a new window on right")
                     ("j" (lambda (x)
                            (split-window-below)
                            (other-window 1 nil)
                            (dired-jump nil x))
                      "open in a new window below")
                     ))

  (ivy-add-actions 'funk/counsel-file-jump
                   '(
                     ("l" (lambda (x)
                            (split-window-right)
                            (other-window 1 nil)
                            (find-file x))
                      "open in a new window on right")
                     ("j" (lambda (x)
                            (split-window-below)
                            (other-window 1 nil)
                            (find-file x))
                      "open in a new window below")
                     ("I" (lambda (x)
                            (with-ivy-window
                              (insert (abbreviate-file-name (expand-file-name x)))
                              ))
                      "insert relative full-path filename")
                     ("F" (lambda (x)
                            (with-ivy-window
                              (insert (expand-file-name x))
                              ))
                      "insert hardcoded full-path filename")
                     ))

  (ivy-add-actions 'funk/counsel-git-emacs
                   '(
                     ("l" (lambda (x)
                            (split-window-right)
                            (other-window 1 nil)
                            (find-file x))
                      "open in a new window on right")
                     ("j" (lambda (x)
                            (split-window-below)
                            (other-window 1 nil)
                            (find-file x))
                      "open in a new window below")
                     ))

  ;; speed up swiper!
  ;; https://www.reddit.com/r/emacs/comments/cfdv1y/swiper_is_extreamly_slow/euamwwt?utm_source=share&utm_medium=web2x&context=3
  (setq swiper-use-visual-line nil)
  (setq swiper-use-visual-line-p (lambda (a) nil))
  )

;;; ivy-rich
(use-package! ivy-rich
  :after (ivy counsel)
  :init
  (ivy-rich-mode 1))

;;; ivi-prescient
(use-package! ivy-prescient
  :after (ivy counsel)
  :custom
  (ivy-prescient-enable-filtering nil)
  :config
  (prescient-persist-mode 1)
  (ivy-prescient-mode 1))

;;; all-the-icons-ivy-rich
(use-package! all-the-icons-ivy-rich
  :after (ivy counsel)
  :init (all-the-icons-ivy-rich-mode 1)
  :config
  ;; The icon size
  (setq all-the-icons-ivy-rich-icon-size 1.0)
  (setq all-the-icons-ivy-rich-project t)
  ;; Slow Rendering
  ;; If you experience a slow down in performance when rendering multiple icons simultaneously,
  ;; you can try setting the following variable
  (setq inhibit-compacting-font-caches t))

;;; counsel
(use-package! counsel
  :diminish counsel-mode
  :bind (("C-M-j" . 'counsel-switch-buffer)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  :config
  (counsel-mode 1)

  (defun funk/show-kill-ring ()
    (interactive)
    (counsel-yank-pop))

  (defun funk/counsel-file-jump ()
    "Open minibuffer from the project root."
    (interactive)
    (with-eval-after-load 'counsel
      (with-eval-after-load 'projectile
        (with-eval-after-load 'counsel-projectile
          (counsel-file-jump nil (projectile-project-root))))))

  (defun funk/counsel-projectile-from-jump ()
    (interactive)
    (ivy-quit-and-run (counsel-projectile)))
  (defun funk/counsel-dired-jump-from-jump ()
    (interactive)
    (ivy-quit-and-run (funk/counsel-dired-jump)))
  (define-key counsel-file-jump-map (kbd "C-p") 'funk/counsel-projectile-from-jump)
  (define-key counsel-file-jump-map (kbd "C-d") 'funk/counsel-dired-jump-from-jump))

;;; ivy-posframe

;;; counsel-at-point
(use-package! counsel-at-point
  :after (counsel ivy)
  :config
  (global-set-key (kbd "M-n") 'counsel-at-point-rg)
  (global-set-key (kbd "M-o") 'counsel-at-point-imenu))

(provide 'init-ivy)
;;; init-ivy.el ends here
