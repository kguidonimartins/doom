;;; init-keybinding.el --- keybindings configs -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; general
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Remapping-Commands.html
;; (global-unset-key (kbd "C-d"))
(global-unset-key (kbd "C-SPC"))
(global-unset-key (kbd "C-,"))
(global-unset-key (kbd "M-u"))
(global-unset-key (kbd "M-TAB"))

(use-package! general
  :config
  (general-auto-unbind-keys)
  ;; define leader keys
  (general-create-definer funk/primary-leader-key
    :keymaps '(normal insert visual emacs motion operator)
    :prefix ","
    :global-prefix "C-,")

  (general-create-definer funk/secondary-leader-key
    :keymaps '(normal insert visual emacs motion operator)
    :prefix "SPC"
    :non-normal-prefix "C-SPC")

  ;; NOTE: keep a similar list for both leader keys.
  ;; thanks: https://github.com/noctuid/general.el/issues/501
  (defmacro both-leader-keys (&rest args)
    `(progn
       (funk/primary-leader-key ,@args)
       (funk/secondary-leader-key ,@args)))

  (both-leader-keys
   "-"     'funk/split-below-projectile-project-root
   "="     'funk/split-right-projectile-project-root
   "."     'counsel-projectile-switch-to-buffer
   "/"     'funk/counsel-surfraw
   ":"     'counsel-switch-buffer
   "`"     'multi-vterm-project
   "~"     'multi-vterm
   "\""    'eyebrowse-close-window-config
   "0"     'eyebrowse-switch-to-window-config-0
   "1"     'eyebrowse-switch-to-window-config-1
   "2"     'eyebrowse-switch-to-window-config-2
   "3"     'eyebrowse-switch-to-window-config-3
   "4"     'eyebrowse-switch-to-window-config-4
   "5"     'eyebrowse-switch-to-window-config-5
   "6"     'eyebrowse-switch-to-window-config-6
   "7"     'eyebrowse-switch-to-window-config-7
   "8"     'eyebrowse-switch-to-window-config-8
   "9"     'eyebrowse-switch-to-window-config-9
   "C-SPC" 'eyebrowse-switch-to-window-config
   "RET"   'eyebrowse-last-window-config
   "b"      'counsel-bookmark
   "cd"    'funk/simple-insert-date
   "ch"    'funk/comment-header
   "cr"    'funk/create-readme-here
   "ct"    'funk/comment-timestamp-keyword
   "df"    'funk/counsel-git-emacs
   "dm"    'darkroom-mode
   "dp"    'funk/counsel-git-dotfiles-public
   "ed"    'eval-defun
   "fa"    'outline-show-all
   "fb"    'bicycle-cycle
   "fh"    'outline-hide-body
   "fo"    'counsel-outline
   "fs"    'funk/hide-via-selective-display
   "fu"    'funk/show-via-selective-display
   "gA"    'git-gutter+-stage-and-commit-whole-buffer
   "gU"    'git-gutter+-unstage-whole-buffer
   "ga"    'git-gutter+-stage-and-commit
   "gc"    'git-gutter+-commit
   "gd"    'magit-dispatch
   "gf"    'magit-file-dispatch
   "gg"    'magit-status
   "gl"    'magit-list-repositories
   "gn"    'git-gutter+-next-hunk
   "gp"    'git-gutter+-previous-hunk
   "gs"    'git-gutter+-stage-hunks
   "gu"    'git-gutter+-revert-hunks
   "gv"    'git-gutter+-show-hunk
   "hg"    'global-hide-mode-line-mode
   "hm"    'hide-mode-line-mode
   "hh"    'funk/hydra-top-menu/body
   "i"     'funk/indent-region
   "m"     'funk/add-pipe
   "nd"    'narrow-to-defun
   "np"    'narrow-to-page
   "nr"    'narrow-to-region
   "nu"    'widen ; narrow undo!
   "O"     'occur
   "p"     'counsel-projectile-switch-project-open-project
   "rg"    'rg
   "s"     'multi-vterm
   "tl"    'centaur-tabs-counsel-switch-group
   "tn"    'hl-todo-next
   "to"    'hl-todo-occur
   "tp"    'hl-todo-previous
   "tt"    'async-shell-command
   "ww"    'funk/hydra-top-menu/funk-window/body-and-exit
   "x"     'execute-extended-command
   )

  ;; (funk/primary-leader-key
  ;;   )

  ;; (funk/secondary-leader-key
  ;;   )

  )


;;; new prefix: ctrl-a
(define-key global-map (kbd "C-a") (make-sparse-keymap))
(let ((bindings '(
                  ("g" . magit-status)
                  ("h" . evil-window-left)
                  ("j" . evil-window-down)
                  ("k" . evil-window-up)
                  ("l" . evil-window-right)
                  ("x" . delete-window)
                  ("o" . delete-other-windows)
                  ("f" . helm-find-files)
                  ("m" . counsel-switch-buffer)
                  ("b" . switch-to-buffer)
                  ("c" . dired-jump)
                  ("n" . counsel-projectile-switch-to-buffer)
                  ("-" . funk/split-below-projectile-project-root)
                  ("=" . funk/split-right-projectile-project-root)
                  ("s" . projectile-switch-open-project)
                  ("z" . zoom-window-zoom)
                  )))
  (dolist (binding bindings)
    (global-set-key (kbd (concat "C-a " (car binding))) (cdr binding))))

;;; override some global keybinding

  ;; FROM: https://github.com/mpereira/.emacs.d/#mappings
(after!
  (evil evil-collection)

  :config

  ;; (general-define-key
  ;;  :keymaps '(emacs-lisp-mode-map markdown-mode-map ess-mode-map)
  ;;  :states '(normal emacs insert)
  ;;  "C-m" #'newline)

  (general-define-key
   :keymaps '(image-mode-map)
   :states '(normal emacs)
   "," nil
   "." nil)

  (with-eval-after-load 'outline-minor-mode
    (general-define-key
     :keymaps '(outline-mode-map)
     :states '(normal)
     "C-j" nil
     "C-k" nil)
    (general-define-key
     :keymaps '(outline-minor-mode-map)
     :states '(normal)
     "C-j" nil
     "C-k" nil))

  (general-define-key
   :keymaps '(org-agenda-mode-map)
   :states '(normal motion emacs)
   "M-h" nil
   "M-j" nil
   "M-k" nil
   "M-l" nil)

  (general-define-key
   :keymaps '(override) ; check out `general-override-mode-map'.
   ;; Adding `nil' to the states makes these keybindings work on buffers where
   ;; they would usually not work, e.g. the *Messages* buffer or the
   ;; `undo-tree-visualize' buffer.
   :states '(normal visual insert nil)
   "M-,"   #'counsel-M-x
   "M-u"   #'universal-argument
   "M-h"   #'evil-window-left
   "M-j"   #'evil-window-down
   "M-k"   #'evil-window-up
   "M-l"   #'evil-window-right
   "M-TAB" #'evil-window-prev
   "C-y"   #'clipboard-yank
   "M-y"   #'funk/show-kill-ring
   )

  ;; (general-define-key
  ;;  :keymaps '(override)
  ;;  :states '(normal visual)
  ;;  "C-SPC" #'funk/hydra-top-menu/body)

  ;;   (defun easy-underscore (arg)
  ;;     "Convert all inputs of semicolon to an underscore.
  ;; If given ARG, then it will insert an acutal semicolon."
  ;;     (interactive "P")
  ;;     (if arg
  ;;         (insert ";")
  ;;       (insert "_")))

  ;; (global-set-key (kbd ";") 'easy-underscore)

  (defun scroll-buffer-down (&optional arg)
    "Scroll buffer by (optional) ARG paragraphs."
    (interactive "p")
    (forward-paragraph arg)
    (recenter))

  (defun scroll-buffer-up (&optional arg)
    "Scroll buffer by (optional) ARG paragraphs."
    (interactive "p")
    (backward-paragraph arg)
    (recenter))

  (global-set-key "\M-]" 'scroll-buffer-down)
  (global-set-key "\M-[" 'scroll-buffer-up)

  (add-hook 'image-mode-hook (lambda () (use-local-map nil))))

  ;; FROM: https://karthinks.com/software/an-elisp-editing-tip/
  ;; (global-set-key [remap eval-last-sexp] 'pp-eval-last-sexp)

  (global-set-key [remap describe-function] 'counsel-describe-function)
  (global-set-key [remap describe-command] 'helpful-command)
  (global-set-key [remap describe-variable] 'counsel-describe-variable)
  (global-set-key [remap describe-key] 'helpful-key)

  (with-eval-after-load 'major-mode-hydra
    (global-set-key (kbd "M-SPC") 'major-mode-hydra))

  ;; (with-eval-after-load 'magit
  ;;   (global-set-key (kbd "C-x g g") 'magit-status)
  ;;   (global-set-key (kbd "C-x g d") 'magit-dispatch)
  ;;   (global-set-key (kbd "C-x g f") 'magit-file-dispatch)
  ;;   (global-set-key (kbd "C-x g l") 'magit-list-repositories))

  ;; (with-eval-after-load 'git-gutter+
  ;;   (global-set-key (kbd "C-x g n") 'git-gutter+-next-hunk)
  ;;   (global-set-key (kbd "C-x g p") 'git-gutter+-previous-hunk)
  ;;   (global-set-key (kbd "C-x g u") 'git-gutter+-revert-hunks)
  ;;   (global-set-key (kbd "C-x g v") 'git-gutter+-show-hunk)
  ;;   (global-set-key (kbd "C-x g s") 'git-gutter+-stage-hunks)
  ;;   (global-set-key (kbd "C-x g c") 'git-gutter+-commit)
  ;;   (global-set-key (kbd "C-x g a") 'git-gutter+-stage-and-commit)
  ;;   (global-set-key (kbd "C-x g A") 'git-gutter+-stage-and-commit-whole-buffer)
  ;;   (global-set-key (kbd "C-x g U") 'git-gutter+-unstage-whole-buffer))

  ;; (with-eval-after-load 'browse-at-remote
  ;;   (global-set-key (kbd "C-x g b") 'browse-at-remote)
  ;;   )

  ;; (with-eval-after-load 'vc-msg-show
  ;;   (global-set-key (kbd "C-x g o") 'vc-msg-show)
  ;;   )

  (with-eval-after-load 'projectile
    (global-set-key (kbd "C-c p") 'projectile-command-map)
    )

;;;###autoload
  (defun funk/jump-to-mu4e-inbox ()
    (interactive)
    (mu4e~headers-jump-to-maildir "/kguidonimartins/INBOX"))

  (global-set-key (kbd "<escape>")       'keyboard-escape-quit)
  ;; (global-set-key (kbd "<f7>")        'counsel-projectile-switch-project)
  (global-set-key (kbd "C--")            'default-text-scale-decrease)
  (global-set-key (kbd "C-=")            'default-text-scale-increase)
  (global-set-key (kbd "C-)")            'funk/general-font-setup)
  (global-set-key (kbd "C-<tab>")        'funk/switch-to-previous-buffer)
  (global-set-key (kbd "C-c C-m")        'execute-extended-command)
  (global-set-key (kbd "C-c a")          'org-agenda)
  (global-set-key (kbd "C-c b")          'ibuffer)
  (global-set-key (kbd "C-c c")          'org-capture)
  (global-set-key (kbd "C-c e")          'visit-initel)
  (global-set-key (kbd "C-c f")          'jump-to-register)
  (global-set-key (kbd "C-c m")          'funk/jump-to-mu4e-inbox)
  (global-set-key (kbd "C-c r")          'reload-initel)
  (global-set-key (kbd "C-c s")          'funk/simple-scratch-buffer)
  (global-set-key (kbd "C-v")            'vterm-yank)
  (global-set-key (kbd "C-x 2")          'funk/split-below-projectile-project-root)
  (global-set-key (kbd "C-x 3")          'funk/split-right-projectile-project-root)
  (global-set-key (kbd "C-x <C-return>") 'execute-extended-command)
  (global-set-key (kbd "C-x C-b")        'ibuffer)
  (global-set-key (kbd "C-x C-m")        'execute-extended-command)
  (global-set-key (kbd "C-x O")          'funk/only-current-buffer)
  (global-set-key (kbd "C-x m")          nil)
  (global-set-key (kbd "M-/")            'hippie-expand)
  (global-set-key (kbd "M-\\")           'comint-dynamic-complete-filename)

  (with-eval-after-load 'popper
    (global-set-key (kbd "C-`") 'popper-toggle-latest)
    (global-set-key (kbd "M-`") 'popper-cycle)
    (global-set-key (kbd "C-M-`") 'popper-toggle-type))

  (global-set-key (kbd "C-x C-j") 'dired-jump)
  (global-set-key (kbd "C-x C-p") 'projectile-dired)

  (global-set-key (kbd "C-'") 'imenu-list-smart-toggle)

  (with-eval-after-load 'google-this
    (global-set-key (kbd "C-c g") 'google-this-mode-submap))

  (global-set-key (kbd "<f10>") 'funk/toggle-outline-minor-mode)

  (with-eval-after-load 'counsel
    (global-set-key (kbd "C-.") 'counsel-outline))

;;; which-key
(use-package! which-key
  ;; :defer 2
  :init
  (setq which-key-idle-delay 1)
  :config
  (which-key-mode)
  (setq which-key-side-window-max-height 0.5))


(provide 'init-keybinding)
;;; init-keybinding.el ends here
