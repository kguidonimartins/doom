;;; init-terminal.el --- terminal configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; general configurations
(use-package emacs
  ;; :defer 5
  :ensure nil
  :config
  (setq savehist-file (expand-file-name "var/savehist.el" user-emacs-directory))
  (setq history-length t)
  (setq history-delete-duplicates t)
  (setq savehist-additional-variables
        '(command-history
          compile-history
          evil-ex-history
          evil-jumps-history
          extended-command-history
          file-name-history
          helm-M-x-input-history
          helm-grep-history
          ido-file-history
          kill-ring
          log-edit-comment-ring
          magit-read-rev-history
          mark-ring
          minibuffer-history
          read-expression-history
          regexp-search-ring
          savehist-minibuffer-history-variables
          search-ring))
  (savehist-mode +1)
  (setq history-length 100)

  (defun turn-on-comint-history (history-file)
    (setq comint-input-ring-file-name history-file)
    (comint-read-input-ring 'silent))

  (add-hook 'shell-mode-hook
            (lambda ()
              (turn-on-comint-history (getenv "HISTFILE"))))

  (defun funk-shell-mode-hook ()
    (setq comint-input-ring-file-name "~/.local/share/zsh_history")
    (comint-read-input-ring t))

  (add-hook 'shell-mode-hook 'funk-shell-mode-hook))

;;; vterm
(use-package vterm
  :if (not (in-slow-ssh))
  :commands (vterm vterm-other-window)
  :config
  (setq vterm-kill-buffer-on-exit t)
  (setq vterm-max-scrollback 10000)
  (add-hook 'vterm-mode-hook
            (lambda ()
              (set (make-local-variable 'buffer-face-mode-face) 'fixed-pitch)
              (buffer-face-mode t)))
  (add-hook 'vterm-mode-hook 'evil-insert-state))

;;; multi-vterm
(use-package multi-vterm
  ;; :defer 5
  :config
  (add-hook 'vterm-mode-hook
            (lambda ()
              (setq-local evil-insert-state-cursor 'bar)
              (evil-insert-state)))
  (add-hook 'vterm-mode-hook
            (lambda ()
              (read-only-mode -1))))

(provide 'init-terminal)
;;; init-terminal.el ends here
