;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Karlo Guidoni"
      user-mail-address "karloguidoni2@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(defun in-slow-ssh ()
  (interactive)
  (when (not (eq (file-remote-p default-directory) nil)) t))


(use-package! no-littering
  ;; NOTE: If you want to move everything out of the ~/.emacs.d folder
  ;; reliably, set `user-emacs-directory` before loading no-littering!
  :config
  ;; no-littering doesn't set this by default so we must place
  ;; auto save files in the same path as it uses for sessions
  (setq auto-save-file-name-transforms `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))))

(use-package! super-save
  :config
  (setq super-save-auto-save-when-idle t)
  (setq super-save-remote-files nil)
  (setq super-save-idle-duration 30)
  (super-save-mode +1))

;; nice backup setup:
;; https://stackoverflow.com/questions/151945/how-do-i-control-how-emacs-makes-backup-files
(setq funk/backup-dir
      (let ((dir (concat user-emacs-directory
                         "var/backup/")))
        (make-directory dir :parents)
        dir))

(setq funk/backup-dir-per-save
      (let ((dir (concat user-emacs-directory
                         "var/backup/per-save/")))
        (make-directory dir :parents)
        dir))

(setq backup-directory-alist `((".*" . ,funk/backup-dir)))

(setq make-backup-files t
      backup-by-copying t
      version-control t
      kept-new-versions 10
      kept-old-versions 2
      delete-old-versions t
      backup-by-copying t
      auto-save-default t
      auto-save-timeout 20
      auto-save-interval 200)

;; also backup versioned files
(setq vc-make-backup-files t)

;; Default and per-save backups go here:
(setq backup-directory-alist `(("" . ,funk/backup-dir-per-save)))

(defun force-backup-of-buffer ()
  (setq funk/backup-dir-per-session
        (let ((dir (concat user-emacs-directory
                           "var/backup/per-session/")))
          (make-directory dir :parents)
          dir))
  ;; Make a special "per session" backup at the first save of each
  ;; emacs session.
  (when (not buffer-backed-up)
    ;; Override the default parameters for per-session backups.
    (let ((backup-directory-alist `(("" . ,funk/backup-dir-per-session)))
          (kept-new-versions 3))
      (backup-buffer)))
  ;; Make a "per save" backup on each save.  The first save results in
  ;; both a per-session and a per-save backup, to keep the numbering
  ;; of per-save backups consistent.
  (let ((buffer-backed-up nil))
    (backup-buffer)))

(add-hook 'before-save-hook  'force-backup-of-buffer)

(load! "./lisp/init-funk.el")
(load! "./lisp/init-projectile.el")
(load! "./lisp/init-ui.el")
(load! "./lisp/init-fonts.el")
(load! "./lisp/init-buffer-window.el")
(load! "./lisp/init-keybinding.el")
(load! "./lisp/init-evil.el")
(load! "./lisp/init-keychord.el")
(load! "./lisp/init-folding.el")
(load! "./lisp/init-indent.el")
(load! "./lisp/init-ivy.el")
(load! "./lisp/init-online-search.el")
(load! "./lisp/init-rstats.el")
(load! "./lisp/init-snippet.el")
(load! "./lisp/init-file-management.el")
;; (load! "./lisp/init-csv.el")
(load! "./lisp/init-mode-line.el")
(load! "./lisp/init-ssh.el")
(load! "./lisp/init-custom-variables.el")

;; (add-hook 'after-change-major-mode-hook 'funk/remove-unwanted-buffers)

;;; display-buffer-alist (but see `shackle' and `popper')
(after! +all
  (setq display-buffer-alist
        `(
          ;; ("*R Data View:"
          ;;  (display-buffer-pop-up-frame)
          ;;  ;; (slot . 1)
          ;;  ;; (window-width . 0.5)
          ;;  ;; (reusable-frames . nil)
          ;;  )
          ("*help\\[R:"
           (display-buffer-reuse-window display-buffer-at-bottom)
           (slot . 1)
           (window-width . 0.5)
           (reusable-frames . 0)
           )
          ("*Python doc*"
           (display-buffer-reuse-window display-buffer-at-bottom)
           (slot . 1)
           (window-width . 0.5)
           (reusable-frames . 0)
           )
          ("*R dired*"
           (display-buffer-reuse-window display-buffer-at-bottom)
           (slot . 1)
           (window-width . 0.5)
           (reusable-frames . 0)
           )
          ("*R"
           (display-buffer-reuse-window display-buffer-in-side-window)
           (side . left)
           (slot . -1)
           (window-width . 0.35)
           (reusable-frames . nil)
           )
          ("*Python"
           (display-buffer-reuse-window display-buffer-in-side-window)
           (side . left)
           (slot . -1)
           (window-width . 0.35)
           (reusable-frames . nil)
           )
          ("*jupyter-repl"
           (display-buffer-reuse-window display-buffer-in-side-window)
           (side . left)
           (slot . -1)
           (window-width . 0.35)
           (reusable-frames . nil)
           )
          ("*shell*"
           (display-buffer-reuse-window display-buffer-in-side-window)
           (side . left)
           (slot . -1)
           (window-width . 0.35)
           (reusable-frames . nil)
           )
          ("*cider-repl"
           (display-buffer-reuse-window display-buffer-in-side-window)
           (side . left)
           (slot . -1)
           (window-width . 0.35)
           (reusable-frames . nil)
           )
          ;;         ("*eldoc*"
          ;;          (display-buffer-reuse-window display-buffer-at-bottom)
          ;;          (slot . 1)
          ;;          (window-width . 0.5)
          ;;          (reusable-frames . 0))
          ;;         ;; ("*Help"
          ;;         ;;  (display-buffer-reuse-window display-buffer-at-bottom)
          ;;         ;;  (slot . 1)
          ;;         ;;  (window-width . 0.5)
          ;;         ;;  (reusable-frames . 0))
          ;; ("*Help"
          ;;  (display-buffer-reuse-window display-buffer-in-side-window)
          ;;  (side . left)
          ;;  (slot . -1)
          ;;  (window-width . 0.50)
          ;;  (reusable-frames . nil)
          ;;  )
          ("*Help"
           (display-buffer-reuse-window display-buffer-at-bottom)
           (slot . 1)
           (window-width . 0.5)
           (reusable-frames . 0)
           )
          ;; ("*Org Agenda*"
          ;;  (display-buffer-reuse-window display-buffer-at-bottom)
          ;;  (slot . 1)
          ;;  (window-width . 0.5)
          ;;  (reusable-frames . 0)
          ;;  )
          )
        )


  (setq same-window-regexps '("*Org Agenda*"))
  )
